#ifndef __J1939_COMMON_H__
#define __J1939_COMMON_H__

#include <cstdint>
#include <chrono>
#include <random>
#include <functional>

using u8 = uint8_t;    // byte
using u16 = uint16_t;  // word
using u32 = uint32_t;  // dword
using u64 = uint64_t;  // qword

namespace J1939 {
    constexpr auto MAX_PACKET_SIZE = 8;
    constexpr auto MAX_TRANSPORT_PACKETS_DATA_SIZE = 255 * 7; //1785
    class Frame;

    using TimerHandlerFnPtr = std::function<void()>;
    using FrameHandlerFnPtr = std::function<void(const Frame *)>;

    class Timer {
    public:
        virtual ~Timer() = default;
        
        /**
         * \brief Sets a timer to call handler function in duration milliseconds.
         * \param duration when to call the handler
         * \param handler handler to call
         * \return UID of the running timer to be able to cancel it. Returns 0 if the timer has not been set
         */
        virtual u32 setTimer(std::chrono::milliseconds duration, TimerHandlerFnPtr handler) = 0;
        /**
         * \brief Cancels the previously set timer
         * \param uid running timer UID as returned from setTimer
         */
        virtual void cancelTimer(u32 uid) = 0;
    };
   
    class FrameHandler {
        std::vector<FrameHandlerFnPtr> callbacks;
    protected:            
        void executeCallbackHandlers(const Frame *frame){
            for (const auto &callback : callbacks) {
                callback(frame);
            }
        }
    public:
        virtual ~FrameHandler() = default;
        virtual void send(const Frame *) = 0;
        virtual void receive(const Frame *frame)
        {
            executeCallbackHandlers(frame);
        }
        virtual void registerReceiveHandler(FrameHandlerFnPtr &handler)
        {
            callbacks.push_back(handler);
        }
    };


    namespace ID{
    /* Offsets and masks of 29 bit ID parts*/
        constexpr auto SRC_ADDR_OFFSET = 0;
        constexpr auto SRC_ADDR_MASK = 0xFF;

        constexpr auto PGN_OFFSET = 8;
        constexpr auto PGN_MASK_PDUF1 = 0x3FF00;
        constexpr auto PGN_MASK_PDUF2 = 0x3FFFF;
        constexpr auto PGN_MASK = PGN_MASK_PDUF2;    

        constexpr auto PDU_SPECIFIC_MASK = 0xFF;
        constexpr auto PDU_SPECIFIC_OFFSET = 8;
        constexpr auto DST_ADDR_MASK = PDU_SPECIFIC_MASK;
        constexpr auto DST_ADDR_OFFSET = PDU_SPECIFIC_OFFSET;
        constexpr auto GROUP_EXTENSION_MASK = PDU_SPECIFIC_MASK;
        constexpr auto GROUP_EXTENSION_OFFSET = PDU_SPECIFIC_OFFSET;

        constexpr auto PDU_FORMAT_OFFSET = 16;
        constexpr auto PDU_FORMAT_MASK = 0xFF;

        constexpr auto DATA_PAGE_MASK = 1;
        constexpr auto DATA_PAGE_OFFSET = 24;

        constexpr auto EXT_DATA_PAGE_MASK = 1;
        constexpr auto EXT_DATA_PAGE_OFFSET = 25;

        constexpr auto PRIORITY_OFFSET = 26;
        constexpr auto PRIORITY_MASK = 7;
    }
    constexpr auto DEFAULT_PRIORITY = 6;
    constexpr auto PDU_FORMAT_DELIMITER = 240;
    constexpr u8 INVALID_ADDRESS = 0xFE;
    constexpr u8 BROADCAST_ADDRESS = 0xFF;

    /**
    *                       PDU Format          PDU Specific                Communication Mode
    *-------------------------------------------------------------------------------------------
    *   PDU1 Format      |  0    - 239          Destination Address         Peer-to-Peer
    *                    |  0hex - EFhex
    *-------------------------------------------------------------------------------------------
    *   PDU2 Format      |  240 - 255           Group Extension             Broadcasting
    *                    |  F0hex - FFhex
    *-------------------------------------------------------------------------------------------
     */
    enum class PduFormat {
        PDU1,
        PDU2
    };

    namespace TimeoutControl
    {
        using namespace std::chrono;
        /* Scenarios for timeout control are */
        
        /**
         * A  node (regardless whether the node is the receiver or sender of the data message)
         * does not respond within Tr to a data or flow control message
         */
        constexpr auto Tr = 200ms; /*Response time*/
        /**
         * If a receiving node needs (for any reason) to delay the transmission of data it can send
         * a Clear to Send message where the number of packages is set to zero. In cases where
         * the flow must be delayed for a certain time the receiver of a message must repeat the
         * transmission of the Clear to Send message every 0.5 seconds (Th) to maintain an
         * open connection with the sender of the message. As soon as the receiver is ready to
         * receive the message it must send a regular Clear to Send message
         */
        constexpr auto Th = 500ms; /*Holding Time*/
        /**
         * A time of greater than T1 elapsed between two message packets 
         * when more packets were expected.
         */
        constexpr auto T1 = 750ms;
        /**
         * A time greater than T2 elapsed after a Clear to Send message 
         * without receiving data from the sender of the data.
         */
        constexpr auto T2 = 1250ms;
        /**
         * A time greater than T3 elapsed after the last transmitted data 
         * packet without receiving a Clear to Send or End of Message Acknowledgment (ACK) message.
         */
        constexpr auto T3 = 1250ms;
        /**
         * A time greater than T4 elapsed after sending a Clear to Send message to delay data
         * transmission without sending another Clear to Send message
         */
        constexpr auto T4 = 1050ms;

        /**
         * A time to wait until and address is supposed to be claimed and can 
         * be used to communicate.
         */
        constexpr auto TAddrClaim = 250ms;

        /**
         *  Address Claim Bus Collision Management
         *  The transmit delay should be calculated by producing a “pseudorandom” 
         *  delay value between 0 and 255
         */
        constexpr auto TrandMin = 0;
        constexpr auto TrandMax = 255;

        /**
         *  Pseudorandom generator of milliseconds time in the range <TrandMin, TrandMax>
         */
        inline milliseconds RandomTime()
        {
            using namespace std::chrono;
            std::random_device rd;
            std::default_random_engine engine{rd()};
            std::uniform_int_distribution<> distribution{TrandMin, TrandMax};
            return milliseconds{distribution(engine)};
        }
    }
}  // namespace J1939
#endif  // __J1939_COMMON_H__
