#ifndef __DPF_TEST_MONITOR_FRAME_H__
#define __DPF_TEST_MONITOR_FRAME_H__

#include "Frames/J1939Frame.h"
#include "SPN/SPN.h"

namespace J1939 {

    namespace DEUTZ {
        namespace DPFTestMonitor {
            constexpr auto PACKET_SIZE = 8;
            constexpr auto DEFAULT_PRIORITY = 6;
            using namespace SPN;
            
            enum class EolRoutineStatus {
                /* 0 - No test ongoing */
                NO_TEST_ONGOING = 0,
                /* 2 - Check Precondition */
                CHECK_PRECONDITION = 2,
                /* 3 - Error during test */
                ERROR_DURING_TEST = 3,
                /* 4 - Test ongoing */
                TEST_ONGOING = 4,
                /* 6 - Test successful */
                TEST_SUCCESSFUL = 6,
            };
            using EOL_ROUTINE_STATUS = SPN_STATUS<EolRoutineStatus, 1>;
            enum class EolRoutineFeedback {
                /* 0x0 - Test ongoing, No Results */
                NO_RESULTS = 0x0,
                /* 0x1 - Faults before test started see error memory */
                FAULTS_BEFORE_STARTED = 0x1,
                /* 0x2 - Engine not running */
                ENGINE_NOT_RUNNING = 0x2,
                /* 0x4 - Soot load to high, replace DPF */
                HIGH_SOOT_LOAD = 0x4,
                /* 0x9 - Oxy Cat sensors swapped */
                OXYCAT_SENSORS_SWAPPED = 0x9,
                /* 0xA (10 dez.) - fault during test, see error memory */
                FAULTS_DURING_TEST = 0xA,
                /* 0xB (11 dez.) - parking brake, switches, vehicle speed */
                PARKING_BRAKE = 0xB,
                /* 0xC (12 dez.) - temperature upstream OxyCat too high */
                OXYCAT_TEMPERATURE_UPSTREAM_TOO_HIGH = 0xC,
                /* 0x12 (18 dez.) - vehicle left the engine operation point */
                VEHICLE_LEFT_ENGINE_OPERATION_POINT = 0x12,
                /* 0x13 (19 dez.) - Temperature downstream OxyCat is too high */
                OXYCAT_TEMPERATURE_DOWNSTREAM_TOO_HIGH = 0x13,
                /* 0x15 (21 dez.) - Oxy Cat Sensors difference too high */
                OXYCAT_DIFFERENCE_TOO_HIGH = 0x15,
                /* 0x18 (24 dez.) - Air Path: not warmed up or Air Actuators not ready */
                AIR_NOT_WARMED_UP = 0x18,
                /* 0x19 (25 dez.) - Engine coolant not warmed up / Timer since last try / Fault */
                COOLANT_NOT_WARMED_UP = 0x19,
                /* 0x1A (26 dez.) - stationary conditions not met */
                STATIONARY_CONDITIONS_NOT_MET = 0x1A,
                /* 0x1B (27 dez.) - SCR sensors not ready / Dosing Actuators not ready / Tank Frozen */
                SCR_SENSORS_NOT_READY = 0x1B,
                /* 0x1E (30 dez.) - Test successful */
                TEST_SUCCESSFUL = 0x1E,
                /* 0x20 (32 dez.) - NOx sensors swapped */
                NOX_SENSORS_SWAPPED = 0x20
            };
            using EOL_ROUTINE_FEEDBACK = SPN_STATUS<EolRoutineFeedback, 2>;
            enum class RegenerationSuccessfulFlag {
                /* 0b00 - normal operation or regeneration ongoing */
                NORMAL = 0b00,
                /* 0b01 - standstill regeneration was finished successfully. */
                SUCCESSFUL = 0b01,
                /* 0b10*/
                RESERVED2 = 0b10,
                /* 0b11*/
                RESERVED3 = 0b11
            };
            using REGENERATION_SUCCESSFUL_FLAG = SPN_STATUS<RegenerationSuccessfulFlag, 3,1,2>;
            enum class AshLoadExchangeRequest {
                /* 0b00 - no exchange request */
                NO_REQUEST = 0b00,
                /* 0b01 - Filter exchange required */
                EXCHANGE_REQUIRED = 0b01,
                /* 0b10 - Filter exchange required, warning level 1 */
                EXCHANGE_REQUIRED_WARNING_L1 = 0b10,
                /* 0b11 - Filter exchange required, system reaction active */
                EXCHANGE_REQUIRED_SYSTEM_REACTION_ACTIVE = 0b11
            };
            using ASH_LOAD_EXCHANGE_REQUEST = SPN_STATUS<AshLoadExchangeRequest, 3,3,2>;
            enum class OilExchangeRequest {
                /* 0b00 - no oil exchange request */
                NO_REQUEST = 0b00,
                /* 0b01 - Oil Exchange required due to operation time in standstill operation. */
                EXCHANGE_REQUIRED = 0b01,
                /* 0b11 - not used */
                NOT_USED = 0b11
            };
            using OIL_EXCHANGE_REQUEST = SPN_STATUS<OilExchangeRequest, 3,5,2>;
            enum class InstalledEatSystem {
                /*Bitmask*/
                NONE = 0,
                /* 8 Reserved */
                RESERVED_1 = 1 << 8,
                /* 7 Reserved*/
                RESERVED_2 = 1 << 7,
                /* 6 2nd SCR*/
                SCR_SECOND = 1 << 6,
                /* 5 CSF*/
                CSF = 1 << 5,
                /* 4 1st SCR*/
                SCR_FIRST = 1 << 4,
                /* 3 DPF with Burner*/
                DPF_WITH_BURNER = 1 << 3,
                /* 2 CRT*/
                CRT = 1 << 2,
                /* 1 DOC*/
                DOC = 1 << 1,
            };
            InstalledEatSystem operator|(InstalledEatSystem lhs, InstalledEatSystem rhs);
            using INSTALLED_EAT_SYSTEM = SPN_STATUS<InstalledEatSystem, 6>;
            enum class StandstillRegenerationState {
                /* 0b0000 no standstill regeneration active */
                NOT_ACTIVE = 0b000,
                /* 0b0001 Launch Phase */
                LAUNCH_PHASE = 0b0001,
                /* 0b0010 Main phase */
                MAIN_PHASE = 0b0010,
                /* 0b0100 Afterrun */
                AFTERRUN_PHASE = 0b0100,
            };
            using STANDSTILL_REGENERATION_STATE = SPN_STATUS<StandstillRegenerationState, 7,1,4>;
            enum class StandstillRequestReason {
                /* Bitmask */
                NONE = 0,
                /* 8 Requested due to high filter soot load */
                SOOT_LOAD = 1 << 4,
                /* 7 Requested due to long operation time in heat mode 2 */
                LONG_IN_HEAT_MODE = 1 << 3,
                /* 6 Requested due to maintenance standstill necessary */
                MAINTENANCE_STANDSTILL = 1 << 2,
                /* 5 Requested from SCR System */
                SCR = 1 << 1,
            };
            StandstillRequestReason operator|(StandstillRequestReason lhs, StandstillRequestReason rhs);
            using STANDSTILL_REQUEST_REASON = SPN_STATUS<StandstillRequestReason, 7,5,4>;

            /**
            * Parameter Group Name             Proprietary - Deutz DPF Test Monitor
            * Parameter Group Number           65352 (00FF48hex)
            * Definition                       This PGN contains information about the diesel particulate filter regeneration. 
            *                                  Additional proprietary info about DEUTZ Stage V engine.
            * Transmission                     1000 ms
            * Data Length                      8 bytes (CAN DLC = 8)
            * Extended Data Page (R)           0
            * Data Page                        0
            * PDU Format                       255
            * PDU Specific                     72
            * Default Priority                 6
            * Data Description                 BYTE.BIT    SPN     LABEL                
            *                                  1           N/A     EOL Routine Status   
            *                                  2           N/A     EOL Routine Feedback 
            *                                  3.1-3.2     N/A     Regeneration successful flag       
            *                                  3.3-3.4     N/A     Wash Bit, Ash load high
            *                                  3.5-3.6     N/A     Oil Exchange Request
            *                                  3.7-4       N/A     not used (0x3FF)
            *                                  5           N/A     remaining standstill regeneration time - 1 minute / bit, 0 offset
            *                                  6           N/A     Installed EAT system
            *                                  7.1-7.4     N/A     Standstill regeneration state
            *                                  7.5-7.8     N/A     Standstill request reason
            *                                  8           N/A     not used (0xFF)
            */
            class Frame : public J1939::Frame {
                EolRoutineStatus eolStatus;
                EolRoutineFeedback eolFeedback;
                RegenerationSuccessfulFlag regenerationSuccessfulFlag;
                AshLoadExchangeRequest ashLoadExchangeRequest;
                OilExchangeRequest oilExchangeRequest;
                std::chrono::minutes remainintStandstillRegenerationTime;
                InstalledEatSystem installedEatSystem;
                StandstillRegenerationState standstillRegenerationState;
                StandstillRequestReason standstillRequestReason;
            protected:
                bool decodeData(const u8 *buffer, const size_t &length) override;
                bool encodeData(u8 *buffer) const override;

            public:
                Frame();
                virtual ~Frame();
                size_t getDataLength() const override;


                EolRoutineStatus getEolStatus() const;
                EolRoutineFeedback getEolFeedback() const;
                RegenerationSuccessfulFlag getRegenerationSuccessfulFlag() const;
                AshLoadExchangeRequest getAshLoadExchangeRequest() const;
                OilExchangeRequest getOilExchangeRequest() const;
                std::chrono::minutes getRemainintStandstillRegenerationTime() const;
                InstalledEatSystem getInstalledEatSystem() const;
                StandstillRegenerationState getStandstillRegenerationState() const;
                StandstillRequestReason getStandstillRequestReason() const;
            };
        } // namespace DPFTestMonitor
    }     // namespace DEUTZ
}         /* namespace J1939 */

#endif /* __DPF_TEST_MONITOR_FRAME_H__ */
