/**
 * Transport Protocol - Connection Management (TP.CM) frame
 */
#ifndef __TRANSPORT_CM_FRAME_H__
#define __TRANSPORT_CM_FRAME_H__

#include "J1939Frame.h"

namespace J1939 {
    namespace Transport {
        namespace TPCM {
            constexpr auto PACKET_SIZE = 8;
            constexpr auto DEFAULT_PRIORITY = 7;
            enum class ControlType {
                /* DEFAULT unknown type */
                UNKNOWN = 0,
                /**
            * TP.CM_RTS             Connection Mode Request to Send
            *                       1 - Control Byte = 16
            *                       2,3 - Message Size (Number of bytes)
            *                       4 - Total number of packets
            *                       5 - Max. number of packets in response to CTS. No limit when
            *                       filled with FFhex.
            *                       6-8 - Parameter Group Number of the multi-packet message
            *                       (6=LSB, 8=MSB)
            */
                RTS = 16,
                /**
             * TP.CM_CTS             Connection Mode Clear to Send
             *                       1 - Control Byte = 17
             *                       2 - Total number of packets (should not exceed byte 5 in RTS)
             *                       3 - Next packet number
             *                       4,5 - Reserved (should be filled with FFhex)
             *                       6-8 - Parameter Group Number of the multi-packet message
             *                       (6=LSB, 8=MSB)
             */
                CTS = 17,
                /**
             * TP.CM_EndOfMsgACK    End of Message Acknowledgment
             *                      1 - Control Byte = 19
             *                      2,3 - Message Size (Number of bytes)
             *                      4 - Total number of packages
             *                      5 - Reserved (should be filled with FFhex)
             *                      6-8 - Parameter Group Number of the multi-packet message
             *                      (6=LSB, 8=MSB)
             */
                EOM_ACK = 19,
                /**
             * TP.BAM               Broadcast Announce
             *                      1 - Control Byte = 32
             *                      2,3 - Message Size (Number of bytes)
             *                      4 - Total number of packages
             *                      5 - Reserved (should be filled with FFhex)
             *                      6-8 - Parameter Group Number of the multi-packet message
             *                      (6=LSB, 8=MSB)
             */
                BAM = 32,
                /**
             * TP.Conn_Abort        Connection Abort
             *                      1 - Control Byte = 255
             *                      2 - Connection Abort Reason (See following description)
             *                      3-5 - Reserved (should be filled with FFhex)
             *                      6-8 - Parameter Group Number of the multi-packet message
             *                      (6=LSB, 8=MSB)
             */
                ABORT = 255
            };

            enum class AbortReason {
                /* Not an abort reason.*/
                NONE = 0,
                /* Already in one or more connection managed sessions and cannot support another.*/
                NOT_SUPPORTED = 1,
                /* System resources were needed for another task so this connection managed session was terminated.*/
                TERMINATED = 2,
                /* A timeout occurred and this is the connection abort to close the session.*/
                TIMEOUT = 3,
                /* CTS messages received when data transfer is in progress.*/
                MULTI_CTS = 4,
                /* Maximum retransmit request limit reached*/
                RTX_LIMIT = 5,
                /* Unexpected data transfer packet. */
                UNEXPECTED_PACKET = 6,
                /* Bad sequence number (software cannot recover). */
                BAD_SEQ_N = 7,
                /* Duplicate sequence number (software cannot recover) */
                DUPLICATE_SEQ_N = 8,
                /* "Total Message Size" is greater than 1785 bytes */
                TOO_LONG_MSG = 9,
                /* If a Connection Abort reason is identified that is not listed in the table use code 250. */
                UNKNOWN = 250,
                _LAST
            };

            /**
             * Parameter Group Name                 Transport Protocol - Connection Management (TP.CM)
             * Parameter Group Number               60416 (00EC00hex)
             * Definition                           Used for Communication Management flow-control (e.g.
             *                                      Broadcast Announce Message).
             * Transmission Rate                    According to the Parameter Group Number to be transferred
             * Data Length                          8 bytes
             * Extended Data Page (R)               0
             * Data Page                            0
             * PDU Format                           236
             * PDU Specific                         Destination Address (= 255 for broadcast)
             * Default Priority                     7
             * Data Description                     Depending on content of Control Byte - See TPCM::ControlType.
             *                                      Byte 1 - Control Byte = (TPCM::ControlType)
             */
            class Frame : public J1939::Frame {
                ControlType mCtrlType;
                u16 mTotalMsgSize;
                u8 mMaxPackets;
                u8 mPacketsToTx;
                u8 mNextPacket;
                AbortReason mAbortReason;
                u32 mDataPgn;

                bool decodeRTS(const u8 *buffer);
                bool decodeCTS(const u8 *buffer);
                bool decodeEndOfMsgACK(const u8 *buffer);
                bool decodeConnAbort(const u8 *buffer);
                bool decodeBAM(const u8 *buffer);

                bool encodeRTS(u8 *buffer) const;
                bool encodeCTS(u8 *buffer) const;
                bool encodeEndOfMsgACK(u8 *buffer) const;
                bool encodeConnAbort(u8 *buffer) const;
                bool encodeBAM(u8 *buffer) const;

            protected:
                bool decodeData(const u8 *buffer, const size_t &length) override;
                bool encodeData(u8 *buffer) const override;

            public:
                Frame();
                virtual ~Frame();

                size_t getDataLength() const override;

                bool isLengthValid() const;
                bool isValid() const;
                ControlType getCtrlType() const;

                AbortReason getAbortReason() const;

                u32 getDataPgn() const;

                u8 getRTSMaxPackets() const;

                u8 getCTSNextPacket() const;

                u8 getCTSPacketsToTx() const;

                u16 getTotalMsgSize() const;

                u8 getTotalPackets() const;

                void setCtrlType(ControlType ctrlType);

                void setDataPgn(u32 dataPgn);

                bool setAbortReason(AbortReason abortReason);

                bool setRTSMaxPackets(u8 maxPackets);

                bool setCTSNextPacket(u8 nextPacket);

                bool setCTSPacketsToTx(u8 packetsToTx);

                bool setTotalMsgSize(u16 totalMsgSize);

#if defined(DEBUG) && defined(J1939_STRINGS)
                std::string toString() override;
#endif
            };
        } // namespace TPCM
    }     // namespace Transport
}         /* namespace J1939 */

#endif /* __TRANSPORT_CM_FRAME_H__ */
