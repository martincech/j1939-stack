#ifndef __SPN_5xxx_H__
#define __SPN_5xxx_H__

#include "SPN.h"
#include "SPN_3xxx.h"

namespace J1939 {
    namespace SPN {
        enum class SPN_5504_STATES {
            /*00b = Purging not enabled*/
            NOT_ENABLED,
            /*01b = Purging enabled - less urgent*/
            ENABLED,
            /*10b = Purging enabled - urgent*/
            ENABLED_URGENT,
            /*11b = Not available*/
            NOT_AVAILABLE
        };

        using SPN_5504 = SPN_STATUS<SPN_5504_STATES, 8, 1, 2>;

        using SPN_5629_STATES = SPN_3702_STATES;
        using SPN_5629 = SPN_STATUS<SPN_5629_STATES, 8, 3, 2>;
    }

} /* namespace J1939 */

#endif /* __SPN_H__ */
