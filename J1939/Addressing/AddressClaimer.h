#ifndef __ADDRESS_CLAIMER_H__
#define __ADDRESS_CLAIMER_H__

#include "Frames/AddressClaimFrame.h"
#include <functional>
#include <queue>
#include <vector>
#include <chrono>

namespace J1939 {
    namespace AddressClaim {
        /**
        * Address claimer callbacks.
        */
        using AddressClaimerCallbackFn = std::function<void()>;

        /**
         * Address Claimer interface
         */
        class IAddressClaimer {
        public:
            virtual ~IAddressClaimer() = default;
            virtual u8 currentAddress() = 0;
            virtual void registerOnAddressClaimed(const AddressClaimerCallbackFn &callback) = 0;
            virtual void registerOnAddressConflict(const AddressClaimerCallbackFn &callback) = 0;
        };

        /**
         *  Address claimer FSM to claim and address and solve conflicts.
         *  Needs a Timer (to timeout) and FrameHandler(to send/receive frames).
         *  Callbacks can be registered to inform about the claim/conflict statuses
         */
        class AddressClaimer : public IAddressClaimer {
            enum ClaimStatus {
                INIT,
                VALID,
                CONFLICT
            };

            EcuName::EcuName mEcuName;
            std::queue<u8> mPrefferedAddresses;
            u8 mCurrentAddress{};
            u8 clameTrials{};
            ClaimStatus mCurrentStatus;
            std::vector<AddressClaimerCallbackFn> addressClaimedCallbacks;
            std::vector<AddressClaimerCallbackFn> addressConflictCallbacks;

            u32 runningTimerUid{};
            std::chrono::milliseconds runningTimerMs{};
            FrameHandler *frameHandler;
            FrameHandlerFnPtr frameHandleFn;
            Timer *timer;
            TimerHandlerFnPtr timerHandleFn;

            void startTimer(std::chrono::milliseconds);
            void stopTimer();
            void restartRunningTimer();


            void sendAddressClaim(u8 addrToClaim);
            void resetTryClameCount();
            static void callbackFor(std::vector<AddressClaimerCallbackFn> &callbacks);
            void tryToClaimAddress();
            void addressClaimed(u32 addr);
            bool addressValid() const;

            void cannotClaimTimerReset();
            void initTimerReset();


            void handleFrameReceived(const J1939::Frame *frame);
            void handleTimeout();

        public:
            AddressClaimer(
                FrameHandler *fh,
                Timer *t,
                const EcuName::EcuName &name,
                const std::queue<u8> &preferred);

            u8 currentAddress() override;

            void claim();
            void registerOnAddressClaimed(const AddressClaimerCallbackFn &callback) override;
            void registerOnAddressConflict(const AddressClaimerCallbackFn &callback) override;
        };
    } // namespace AddressClaim

} /* namespace J1939 */

#endif /* __ADDRESS_CLAIMER_H__ */
