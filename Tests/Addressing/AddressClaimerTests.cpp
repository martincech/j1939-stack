#include <gmock/gmock.h>
#include <chrono>
#include <queue>
#include <functional>

#include "Addressing/AddressClaimer.h"
#include "Frames/RequestFrame.h"
#include "Factory.h"
#include "MockFrameHandler.h"
#include "MockTimer.h"
#include "MockCallback.h"

using namespace J1939;
using namespace Tests;
using namespace AddressClaim;
using testing::_;
using testing::InvokeArgument;
using testing::Eq;
using testing::Matcher;
using testing::MatcherCast;
using testing::SafeMatcherCast;
using testing::InvokeWithoutArgs;
using testing::Invoke;
using testing::Return;
using testing::NiceMock;
using testing::SaveArg;

namespace AddressClaimerTests {

    struct AddressClaimerTest : testing::Test {
        NiceMock<MockFrameHandler> mockHandler;
        NiceMock<MockTimer> mockTimer;
        MockCallback callbacks;

        EcuName::EcuName name = EcuName::EcuName(25898, 1256, 5, 16, 241, 90, 15, 2, false);
        EcuName::EcuName higherPrioEcuName = EcuName::EcuName(2, 1256, 5, 16, 241, 90, 15, 2, false);
        EcuName::EcuName lowerPrioEcuName = EcuName::EcuName(30000, 1256, 5, 16, 241, 90, 15, 2, false);
        //u8 ecuNameraw[] = {0x2A, 0x65, 0x00, 0x9D, 0x85, 0xF1, 0xB4, 0x2F};
        std::queue<u8> addressesQ;

        u8 addresses[3] = {0x35, 0x85, 0xA2};

        FrameHandlerFnPtr p;
        std::function<void()> canResponseHighPrioEcu;
        std::function<void()> canResponseLowPrioEcu;

        AddressClaimerTest()
            : p(nullptr) {
            addressesQ.push(addresses[0]);
            addressesQ.push(addresses[1]);
            addressesQ.push(addresses[2]);
            Factory::registerStackFrames();
            canResponseHighPrioEcu = [&]()
            {
                const auto addr = addressesQ.front();
                auto resp = *static_cast<AddressClaim::Frame*>(Factory::getFrame(PGN::ADDRESS_CLAIM));
                resp.setDstAddr(BROADCAST_ADDRESS);
                resp.setSrcAddr(addr);
                resp.setEcuName(higherPrioEcuName);
                addressesQ.pop(); // move to next address
                addressesQ.push(addr);
                p(&resp);
            };
            canResponseLowPrioEcu = [&]()
            {
                auto resp = *static_cast<AddressClaim::Frame*>(Factory::getFrame(PGN::ADDRESS_CLAIM));
                resp.setDstAddr(BROADCAST_ADDRESS);
                resp.setSrcAddr(addresses[0]);
                resp.setEcuName(lowerPrioEcuName);
                p(&resp);
            };
        }

        ~AddressClaimerTest() {
            Factory::unregisterAllFrames();
        }
    };

    class ValidAddressClaimFrameMatcherInterface : public testing::MatcherInterface<const J1939::Frame*> {
    public:
        explicit ValidAddressClaimFrameMatcherInterface(u8 expectedSrc, u8 expectedDst)
            : expectedSrc(expectedSrc)
            , expectedDst(expectedDst) {
        }


        bool MatchAndExplain(const J1939::Frame *x, testing::MatchResultListener *) const override {
            return x->getSrcAddr() == expectedSrc && x->getDstAddr() == expectedDst;
        }

        void DescribeTo(std::ostream *os) const override {
            *os << "src == " << expectedSrc << " and dst == " << expectedDst;
        }

        void DescribeNegationTo(std::ostream *os) const override {
            *os << "src != " << expectedSrc << " or dst != " << expectedDst;
        }

    private:
        u8 expectedSrc;
        u8 expectedDst;
    };

    inline Matcher<const J1939::Frame*> ValidAddressClaimFrameMatcher(u8 expectedSrc, u8 expectedDst) {
        return MakeMatcher(new ValidAddressClaimFrameMatcherInterface(expectedSrc, expectedDst));
    }

    TEST_F(AddressClaimerTest, FrameHandlerRegistered) {
        EXPECT_CALL(mockHandler, registerReceiveHandler(_));
        auto uutClaimer = AddressClaimer(&mockHandler, &mockTimer, name, addressesQ);
        uutClaimer.claim();

    }

    TEST_F(AddressClaimerTest, PgnRequestSend) {
        using namespace std::chrono;
        EXPECT_CALL(mockHandler, send(ValidAddressClaimFrameMatcher(addresses[0], BROADCAST_ADDRESS)))
            .Times(1);
        auto uutClaimer = AddressClaimer(&mockHandler, &mockTimer, name, addressesQ);
        uutClaimer.claim();
    }

    TEST_F(AddressClaimerTest, FirstAddressClaimed) {
        using namespace std::chrono;
        //invoke the registered time handler
        EXPECT_CALL(mockTimer, setTimer(_,_))
            .WillOnce(DoAll(InvokeArgument<1>(), Return(4))); //fist call - timeout

        // match parameter 
        EXPECT_CALL(mockHandler, send(ValidAddressClaimFrameMatcher(addresses[0], BROADCAST_ADDRESS)))
            .Times(1);

        // TEST
        auto uutClaimer = AddressClaimer(&mockHandler, &mockTimer, name, addressesQ);
        uutClaimer.claim();
        ASSERT_EQ(uutClaimer.currentAddress(), addresses[0]); //address claimed
    }

    TEST_F(AddressClaimerTest, SecondAddressClaimedWhenLowerEcuRespond) {
        /* Handler for packet reception*/
        EXPECT_CALL(mockHandler, registerReceiveHandler(_))
            .Times(1)
            .WillOnce(testing::SaveArg<0>(&p));
        /* Packet send expectations with response from bus */
        EXPECT_CALL(mockHandler, send(ValidAddressClaimFrameMatcher(addresses[0], BROADCAST_ADDRESS)))
            .WillOnce(InvokeWithoutArgs(canResponseHighPrioEcu));
        EXPECT_CALL(mockHandler, send(ValidAddressClaimFrameMatcher(addresses[1], BROADCAST_ADDRESS)))
            .Times(1);


        /* Timer setup */
        EXPECT_CALL(mockTimer, setTimer(_,_))
            .WillOnce(DoAll(InvokeArgument<1>(), Return(4)))
            .WillRepeatedly(Return(4));

        // TEST
        auto uutClaimer = AddressClaimer(&mockHandler, &mockTimer, name, addressesQ);
        uutClaimer.claim();
        ASSERT_EQ(uutClaimer.currentAddress(), addresses[1]); //address claimed
    }

    TEST_F(AddressClaimerTest, AllAddressesSeizedWithHighPriorityEcus) {
        using namespace std::chrono;
        EXPECT_CALL(mockHandler, registerReceiveHandler(_))
            .Times(1)
            .WillRepeatedly(testing::SaveArg<0>(&p));

        // expect messages to be send in exact order
        {
            EXPECT_CALL(mockHandler, send(ValidAddressClaimFrameMatcher(addresses[0], BROADCAST_ADDRESS)))
                .WillOnce(InvokeWithoutArgs(canResponseHighPrioEcu))     //1. call           - 1. address
                .WillOnce(InvokeWithoutArgs(canResponseHighPrioEcu));     //5. call           - 1. address
            EXPECT_CALL(mockHandler, send(ValidAddressClaimFrameMatcher(addresses[1], BROADCAST_ADDRESS)))
                .WillOnce(InvokeWithoutArgs(canResponseHighPrioEcu))      //2. call           - 2. address
                .WillOnce(InvokeWithoutArgs(canResponseHighPrioEcu));     //6. call           - 2. address
            EXPECT_CALL(mockHandler, send(ValidAddressClaimFrameMatcher(addresses[2], BROADCAST_ADDRESS)))
                .WillOnce(InvokeWithoutArgs(canResponseHighPrioEcu))      //3. call           - 3. address
                .WillOnce(InvokeWithoutArgs(canResponseHighPrioEcu));     //7. call           - 3. address
            EXPECT_CALL(mockHandler, send(ValidAddressClaimFrameMatcher(INVALID_ADDRESS,BROADCAST_ADDRESS)))
                .WillOnce(Return()).WillOnce(Return());       //4. and 8. call           - Invalid address

        }
        // receive message after registering the timer handler
        EXPECT_CALL(mockTimer, setTimer(_,_))       // 1 timeout - after cant claim (then stop)
            .WillOnce(DoAll(InvokeArgument<1>(), Return(4)))
            .WillRepeatedly(Return(4));              
        
        // TEST
        auto uutClaimer = AddressClaimer(&mockHandler, &mockTimer, name, addressesQ);
        uutClaimer.claim();
        ASSERT_EQ(uutClaimer.currentAddress(), INVALID_ADDRESS); //address claimed
    }

    TEST_F(AddressClaimerTest, LowPriorityEcusOnLineAddressSeized) {
        using namespace std::chrono;
        EXPECT_CALL(mockHandler, registerReceiveHandler(_))
            .Times(1)
            .WillRepeatedly(testing::SaveArg<0>(&p));

        // expect messages to be send in exact order
        {
            EXPECT_CALL(mockHandler, send(ValidAddressClaimFrameMatcher(addresses[0], BROADCAST_ADDRESS)))
                .WillOnce(InvokeWithoutArgs(canResponseLowPrioEcu)) // 3 ecus respond
                .WillOnce(InvokeWithoutArgs(canResponseLowPrioEcu))
                .WillOnce(InvokeWithoutArgs(canResponseLowPrioEcu))
                .WillRepeatedly(Return());

        }
        // receive message after registering the timer handler
        EXPECT_CALL(mockTimer, setTimer(_,_))       
            .WillRepeatedly(Return(4));              

        // TEST
        auto uutClaimer = AddressClaimer(&mockHandler, &mockTimer, name, addressesQ);
        uutClaimer.claim();
        ASSERT_EQ(uutClaimer.currentAddress(), addresses[0]); //address claimed
    }

    TEST_F(AddressClaimerTest, HighAndLowPrioEcusOnLineAddressSeized) {
        using namespace std::chrono;
        EXPECT_CALL(mockHandler, registerReceiveHandler(_))
            .Times(1)
            .WillRepeatedly(testing::SaveArg<0>(&p));

        // expect messages to be send in exact order
        {
            EXPECT_CALL(mockHandler, send(_))
                .WillOnce(InvokeWithoutArgs(canResponseHighPrioEcu)) // 3 ecus respond
                .WillOnce(InvokeWithoutArgs(canResponseHighPrioEcu))
                .WillOnce(InvokeWithoutArgs(canResponseLowPrioEcu))
                .WillRepeatedly(Return());

        }
        // receive message after registering the timer handler
        EXPECT_CALL(mockTimer, setTimer(_,_))
            .WillRepeatedly(Return(4));              

        // TEST
        auto uutClaimer = AddressClaimer(&mockHandler, &mockTimer, name, addressesQ);
        uutClaimer.claim();
        ASSERT_EQ(uutClaimer.currentAddress(), addresses[2]); //address claimed
    }

    TEST_F(AddressClaimerTest, CallbacksCalled) {
        using namespace std::chrono;
        EXPECT_CALL(mockHandler, registerReceiveHandler(_))
            .Times(1)
            .WillRepeatedly(testing::SaveArg<0>(&p));

        // expect once claimed
        EXPECT_CALL(callbacks, ClaimedCallback())
            .WillOnce(Return());
        
        // expect 3 times conflict
        EXPECT_CALL(callbacks, ConflictCallback())
            .Times(3);

        // expect messages to be send in exact order
        EXPECT_CALL(mockHandler, send(_))
            .WillOnce(InvokeWithoutArgs(canResponseHighPrioEcu)) // 3 ecus respond
            .WillOnce(InvokeWithoutArgs(canResponseHighPrioEcu))
            .WillOnce(InvokeWithoutArgs(canResponseLowPrioEcu))
            .WillRepeatedly(Return());

        // receive message after registering the timer handler
        EXPECT_CALL(mockTimer, setTimer(_,_))       
            .WillOnce(Return(4))
            .WillOnce(Return(4))
            .WillOnce(DoAll(InvokeArgument<1>(), Return(4)));

        // TEST
        
        auto uutClaimer = AddressClaimer(&mockHandler, &mockTimer, name, addressesQ);
        uutClaimer.registerOnAddressClaimed(MockCallback::addressClaimedMockFn);
        uutClaimer.registerOnAddressConflict(MockCallback::addressConflictMockFn);
        uutClaimer.claim();
        ASSERT_EQ(uutClaimer.currentAddress(), addresses[2]); //address claimed
    }


    TEST_F(AddressClaimerTest, TimersCancelled) {
        using namespace std::chrono;
        EXPECT_CALL(mockHandler, registerReceiveHandler(_))
            .Times(1)
            .WillRepeatedly(testing::SaveArg<0>(&p));

        // expect messages to be send in exact order
        EXPECT_CALL(mockHandler, send(_))
            .WillOnce(InvokeWithoutArgs(canResponseHighPrioEcu)) // 3 ecus respond
            .WillOnce(InvokeWithoutArgs(canResponseHighPrioEcu))
            .WillOnce(InvokeWithoutArgs(canResponseLowPrioEcu))
            .WillRepeatedly(Return());

        // receive message after registering the timer handler
        EXPECT_CALL(mockTimer, setTimer(_,_))
            .WillOnce(Return(4))
            .WillOnce(Return(4))
            .WillOnce(DoAll(InvokeArgument<1>(), Return(4)));

        // receive message after registering the timer handler
        EXPECT_CALL(mockTimer, cancelTimer(_))       
            .Times(2)    //timer is set 3 times, 2 times canceled         
            .WillRepeatedly(Return());
            

        // TEST        
        auto uutClaimer = AddressClaimer(&mockHandler, &mockTimer, name, addressesQ);
        uutClaimer.claim();
        ASSERT_EQ(uutClaimer.currentAddress(), addresses[2]); //address claimed
    }
} // namespace AddressClaimerTests
