#ifndef __J1939_FRAME_H__
#define __J1939_FRAME_H__

#include "J1939Common.h"
#include "PGN.h"
#if defined(DEBUG) && defined(J1939_STRINGS)
#include <string>
#endif

namespace J1939 {

    /**
     *  Generic abstract frame containing the ID fields (no data).
     *  static parsing methods of ID available
     */
    class Frame {

        u32 mPgn;
        u8 mPriority{};
        u8 mSrcAddr;
        u8 mDstAddr{};
#if defined(DEBUG) && defined(J1939_STRINGS)
        std::string mName;
#endif

    public:
        Frame(PGN pgn);
        Frame(PGN pgn, u8 priority);
        Frame(u32 pgn);
        Frame(u32 pgn, u8 priority);
#if defined(DEBUG) && defined(J1939_STRINGS)
        Frame(PGN pgn, const char *name);
        Frame(PGN pgn, u8 priority, const char *name);
        Frame(u32 pgn, const char *name);
        Frame(u32 pgn, u8 priority, const char *name);
#endif
        virtual ~Frame();

        /* parse parts of ID */
        static u32 getPGN(u32 id);
        static u8 getSrcAddr(u32 id);
        static PduFormat getPDUFormatGroup(u32 id);
        static u8 getPDUFormat(u32 id);
        static u8 getPDUSpecific(u32 id);
        static u8 getDstAddr(u32 id);
        static u8 getGroupExtension(u32 id);
        static u8 getPriority(u32 id);
        static u8 getExtDataPage(u32 id);
        static u8 getDataPage(u32 id);
        static u32 createId(u32 pgn, u32 src, u8 priority = DEFAULT_PRIORITY, u32 dst = BROADCAST_ADDRESS);

        u8 getPriority() const;

        bool setPriority(u8 priority);

        u8 getExtDataPage() const;

        u8 getDataPage() const;

        u8 getPDUFormat() const;

        u8 getPDUSpecific() const;

        u8 getSrcAddr() const;

        void setSrcAddr(u8 src);

        PduFormat getPDUFormatGroup() const;

        u8 getDstAddr() const;
        /* Available only for J1939PduFormat::PDU_FORMAT_1 */
        bool setDstAddr(u8 dst);

        u32 getIdentifier() const;

        u32 getPGN() const;

        /* Decode frame */
        bool decode(u32 identifier, const u8 *buffer, size_t length);
        /* Encode frame */
        bool encode(u32 &identifier, u8 *buffer, size_t &length) const;

    protected:
        /**
        * Decodes the given data
        */
        virtual bool decodeData(const u8 *buffer, const size_t &length) = 0;

        /**
        * Encodes the data field in the given buffer
        * Length is used as input to check the length of the buffer and then set to
        * the number of encoded bytes (which is always less or equal than the given
        * length)
        */
        virtual bool encodeData(u8 *buffer) const = 0;

    public:
        /**
        * Method to know how long our buffer should be to encode properly this
        * message
        */
        virtual size_t getDataLength() const = 0;

        bool isMultiPacketFrame() const;

#if defined(DEBUG) && defined(J1939_STRINGS)
        /**
        * Method to get the frame name
        */
        const std::string &getName() const;
        virtual std::string toString();
#endif
    };

} /* namespace J1939 */

#endif /* __J1939_FRAME_H__ */
