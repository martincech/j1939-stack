#include "GenericMultiFrame.h"
#include <cstring>

#if defined(DEBUG) && defined(J1939_STRINGS)
#include <ostream>
#include <sstream>
#include <string>
constexpr auto MULTIFRAME_NAME = "Generic Multipcket Frame";
#endif


namespace J1939 {

    GenericMultiFrame::GenericMultiFrame(PGN pgn)
        : GenericMultiFrame(static_cast<u32>(pgn)) {
    }

    GenericMultiFrame::GenericMultiFrame(PGN pgn, u8 priority)
        : GenericMultiFrame(static_cast<u32>(pgn), priority) {
    }

    GenericMultiFrame::GenericMultiFrame(u32 pgn)
        : GenericMultiFrame(pgn, DEFAULT_PRIORITY) {
    }

    GenericMultiFrame::GenericMultiFrame(u32 pgn, u8 priority)
        : Frame(pgn, priority
#if defined(DEBUG) && defined(J1939_STRINGS)
                ,
                MULTIFRAME_NAME
#endif
               )
    {
    }

    GenericMultiFrame::~GenericMultiFrame() = default;

    bool GenericMultiFrame::decodeData(const u8* buffer, const size_t& length) {
        data = std::vector<u8>(buffer, buffer + length);
        return true;
    }

    bool GenericMultiFrame::encodeData(u8* buffer) const {
        memcpy(buffer, data.data(), data.size());
        return true;
    }

    size_t GenericMultiFrame::getDataLength() const {
        return data.size();
    }

    const u8* GenericMultiFrame::getRawData() const {
        return data.data();
    }
}
