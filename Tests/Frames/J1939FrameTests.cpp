
#include "Frames/J1939Frame.h"
#include "TestFrame.h"
#include <gtest/gtest.h>
#include <ostream>

using namespace J1939;

namespace J1939FrameTests {

    struct J1939FrameTest : testing::Test {
        TestFrame *frame;

        J1939FrameTest(u32 pgn) {
            frame = new TestFrame(pgn);
        }

        J1939FrameTest(u32 pgn, u8 prio) {
            frame = new TestFrame(pgn, prio);
        }

        ~J1939FrameTest() {
            delete frame;
        }
    };

    struct PDU1FrameTest : J1939FrameTest {

        PDU1FrameTest()
            : J1939FrameTest(0x00EF00) {
        }
    };

    struct PDU2FrameTest : J1939FrameTest {

        PDU2FrameTest()
            : J1939FrameTest(0x00FFFF) {
        }
    };

    struct InitialExpected {
        u32 initial;
        u32 expected;

        friend std::ostream &operator<<(std::ostream &os, const InitialExpected &obj) {
            return os
                   << "initial: " << obj.initial
                   << " expected: " << obj.expected;
        }
    };

    /* PGN tests */
    struct PgnTest : J1939FrameTest, testing::WithParamInterface<InitialExpected> {
        PgnTest()
            : J1939FrameTest(GetParam().initial) {
        }
    };

    TEST_P(PgnTest, PGN) {
        auto pt = GetParam();
        ASSERT_EQ(frame->getPGN(), pt.expected);
    }

    INSTANTIATE_TEST_CASE_P(PDUS, PgnTest,
        testing::Values(
            InitialExpected{0x00AAAA, 0x00AA00},
            InitialExpected{0x00EFFF, 0x00EF00},
            InitialExpected{0x00F0FF, 0x00F0FF},
            InitialExpected{0x00FFFF, 0x00FFFF},
            InitialExpected{0x00FEFF, 0x00FEFF}
        ));

    INSTANTIATE_TEST_CASE_P(RDP, PgnTest,
        testing::Values(
            InitialExpected{0xFFAAAA, 0x03AA00},
            InitialExpected{0x01AAAA, 0x01AA00},
            InitialExpected{0x02AAAA, 0x02AA00},
            InitialExpected{0x04AAAA, 0x00AA00},
            InitialExpected{0xFFFFFF, 0x03FFFF},
            InitialExpected{0x01FFFF, 0x01FFFF},
            InitialExpected{0x02FFFF, 0x02FFFF},
            InitialExpected{0x04FFFF, 0x00FFFF}
        ));

    struct AddrTest : J1939FrameTest, testing::WithParamInterface<InitialExpected> {
        AddrTest()
            : J1939FrameTest(GetParam().initial) {
        }

    };

    using SrcAddrTest = AddrTest;
    using DstAddrTest = AddrTest;

    TEST_F(PDU1FrameTest, SetDstAddr) {
        auto dst = 5;
        frame->setDstAddr(dst);
        ASSERT_EQ(frame->getDstAddr(), dst);
    }

    TEST_F(PDU1FrameTest, SetSrcAddr) {
        auto src = 5;
        frame->setSrcAddr(src);
        ASSERT_EQ(frame->getSrcAddr(), src);
    }

    TEST_F(PDU2FrameTest, SetDstAddr) {
        auto dst = 5;
        frame->setDstAddr(dst);
        ASSERT_EQ(frame->getDstAddr(), BROADCAST_ADDRESS);
    }

    TEST_F(PDU2FrameTest, SetSrcAddr) {
        auto src = 5;
        frame->setSrcAddr(src);
        ASSERT_EQ(frame->getSrcAddr(), src);
    }

    TEST_P(DstAddrTest, DST) {
        auto dst = GetParam();
        ASSERT_EQ(frame->getDstAddr(), dst.expected);
    }


    INSTANTIATE_TEST_CASE_P(DST, DstAddrTest,
        testing::Values(
            InitialExpected{0x00AA55, 0x55},
            InitialExpected{0x00FF55, BROADCAST_ADDRESS}
        ));

    TEST_P(SrcAddrTest, SRC) {
        auto src = GetParam();
        ASSERT_EQ(frame->getSrcAddr(), src.expected);
    }

    INSTANTIATE_TEST_CASE_P(SRC, SrcAddrTest,
        testing::Values(
            InitialExpected{0x00AA55, INVALID_ADDRESS},
            InitialExpected{0x00FF55, INVALID_ADDRESS}
        ));


    struct PrioTest : J1939FrameTest, testing::WithParamInterface<InitialExpected> {
        PrioTest(u32 pgn)
            : J1939FrameTest(pgn, GetParam().initial) {
        }
    };

    struct PDU1PrioTest : PrioTest {
        PDU1PrioTest()
            : PrioTest(0x00EF00) {
        }
    };

    struct PDU2PrioTest : PrioTest {
        PDU2PrioTest()
            : PrioTest(0x00FF00) {
        }
    };

    TEST_F(PDU1FrameTest, DefaultPrio) {
        ASSERT_EQ(frame->getPriority(), DEFAULT_PRIORITY);
    }

    TEST_F(PDU2FrameTest, DefaultPrio) {
        ASSERT_EQ(frame->getPriority(), DEFAULT_PRIORITY);
    }

    TEST_F(PDU1FrameTest, SetPrio) {
        auto prio = 7;
        frame->setPriority(prio);
        ASSERT_EQ(frame->getPriority(), prio);
    }

    TEST_F(PDU2FrameTest, SetPrio) {
        auto prio = 7;
        frame->setPriority(prio);
        ASSERT_EQ(frame->getPriority(), prio);
    }

    TEST_F(PDU1FrameTest, SetInvalidPrio) {
        auto prio = 20;
        ASSERT_FALSE(frame->setPriority(prio));
        ASSERT_EQ(frame->getPriority(), 7);
    }

    TEST_F(PDU2FrameTest, SetInvalidPrio) {
        auto prio = 20;
        ASSERT_FALSE(frame->setPriority(prio));
        ASSERT_EQ(frame->getPriority(), 7);
    }

    TEST_P(PDU1PrioTest, Prio) {
        auto src = GetParam();
        ASSERT_EQ(frame->getPriority(), src.expected);
    }

    TEST_P(PDU2PrioTest, Prio) {
        auto src = GetParam();
        ASSERT_EQ(frame->getPriority(), src.expected);
    }

    INSTANTIATE_TEST_CASE_P(PDU1Prio, PDU1PrioTest,
        testing::Values(
            InitialExpected{0, 0},
            InitialExpected{1, 1},
            InitialExpected{2, 2},
            InitialExpected{3, 3},
            InitialExpected{4, 4},
            InitialExpected{5, 5},
            InitialExpected{6, 6},
            InitialExpected{7, 7},
            InitialExpected{8, 7},
            InitialExpected{255, 7}
        ));

    INSTANTIATE_TEST_CASE_P(PDU2Prio, PDU2PrioTest,
        testing::Values(
            InitialExpected{0, 0},
            InitialExpected{1, 1},
            InitialExpected{2, 2},
            InitialExpected{3, 3},
            InitialExpected{4, 4},
            InitialExpected{5, 5},
            InitialExpected{6, 6},
            InitialExpected{7, 7},
            InitialExpected{8, 7},
            InitialExpected{255, 7}
        ));
} // namespace J1939FrameTests
