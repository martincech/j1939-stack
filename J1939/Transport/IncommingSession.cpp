#include "MultipacketSession.h"
#include "Factory.h"
#include <chrono>

namespace J1939 {
    namespace Transport {
        /**********************************************************************
        * Incomming session
        **********************************************************************/
        IncommingSession::IncommingSession(ISessionOwner *owner, const TPCM::Frame &cmFrame)
            : MultipacketSession(owner, cmFrame) {
        }

        void IncommingSession::waitForNextDtPacket(std::chrono::milliseconds ms) {
            startTimer(ms);
        }

        void IncommingSession::storeFrame(const TPDT::Frame &dtFrame) {
            mDtFrames[dtFrame.getSq()] = dtFrame;
        }

        void IncommingSession::newConnectionFrame(const TPCM::Frame &frame) {
            if (frame.getCtrlType() == TPCM::ControlType::ABORT) {
                aborted = true;
                finishSession();
            }
        }

        bool IncommingSession::isComplete() const {
            return mDtFrames.size() == mCmFrame.getTotalPackets();
        }

        bool IncommingSession::isFrameStored(const TPDT::Frame &dtFrame) {
            return mDtFrames.find(dtFrame.getSq()) != mDtFrames.end();
        }

        u32 IncommingSession::dstAddr() const {
            return mCmFrame.getSrcAddr();
        }

        /**********************************************************************
        * Incomming P2P session
        **********************************************************************/
        IncommingP2PSession::IncommingP2PSession(ISessionOwner *owner, const TPCM::Frame &cmFrame)
            : IncommingSession(owner, cmFrame)
            , ctsNext(1)
            , ctsCount(0) {
            requestNextChunkOfPackets();
        }

        void IncommingP2PSession::newDataFrame(const TPDT::Frame &dtFrame) {
            if (isSeqNOutOfOrder(dtFrame)) {
                requestNextChunkOfPackets();
                return;
            }
            storeFrame(dtFrame);
            if (isComplete()) {
                sendEomFrame();
                finishSession();
                return;
            }
            ctsNext++;
            ctsCount--;
            if (ctsCount == 0) {
                requestNextChunkOfPackets();
                return;
            }
            waitForNextDtPacket(TimeoutControl::T1);
        }

        void IncommingP2PSession::newConnectionFrame(const TPCM::Frame &frame) {
            IncommingSession::newConnectionFrame(frame);
            // TODO process other possible frames
        }

        void IncommingP2PSession::sendCtsFrame() {
            const auto sendFrame = makeSendFrame(TPCM::ControlType::CTS);
            sendFrame->setCTSNextPacket(ctsNext);
            sendFrame->setCTSPacketsToTx(ctsCount);
            owner->send(sendFrame);
            waitForNextDtPacket(TimeoutControl::T2);
        }

        bool IncommingP2PSession::isSeqNOutOfOrder(const TPDT::Frame &dtFrame) const {
            return dtFrame.getSq() != ctsNext;
        }

        void IncommingP2PSession::requestNextChunkOfPackets() {
            const auto restFrames = mCmFrame.getTotalPackets() - mDtFrames.size();
            const auto maxFrames = mCmFrame.getRTSMaxPackets();
            ctsCount = restFrames < maxFrames ? restFrames : maxFrames;
            sendCtsFrame();
        }

        void IncommingP2PSession::sendEomFrame() const {
            const auto sendFrame = makeSendFrame(TPCM::ControlType::EOM_ACK);
            sendFrame->setTotalMsgSize(mCmFrame.getTotalMsgSize());
            owner->send(sendFrame);
        }

        void IncommingP2PSession::sessionTimeout() {
            sendAbortFrame(TPCM::AbortReason::TIMEOUT);
            IncommingSession::sessionTimeout();
        }

        /**********************************************************************
        * Incomming BAM session
        **********************************************************************/
        IncommingBAMSession::IncommingBAMSession(ISessionOwner *owner, const TPCM::Frame &cmFrame)
            : IncommingSession(owner, cmFrame) {
            waitForNextDtPacket(TimeoutControl::T1);
        }

        void IncommingBAMSession::newDataFrame(const TPDT::Frame &dtFrame) {
            storeFrame(dtFrame);
            if (isComplete()) {
                finishSession();
                return;
            }
            waitForNextDtPacket(TimeoutControl::T1);
        }
    } // namespace Transport
}     // namespace J1939
