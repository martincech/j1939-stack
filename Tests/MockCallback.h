#pragma once
#include "Addressing/AddressClaimer.h"
#include "J1939Common.h"
#include <gmock/gmock.h>

namespace J1939 {
    namespace Tests {
        

        void ClaimedCallback();

        void ConflictCallback();

        void FrameReceivedCallback(const Frame *frame);

        struct MockCallback {

            static AddressClaim::AddressClaimerCallbackFn addressClaimedMockFn;
            static AddressClaim::AddressClaimerCallbackFn addressConflictMockFn;
            static FrameHandlerFnPtr frameReceivedMockFn;

            MOCK_CONST_METHOD0(ClaimedCallback, void());
            MOCK_CONST_METHOD0(ConflictCallback, void());
            MOCK_CONST_METHOD1(FrameReceivedCallback, void(const Frame *));

            MockCallback() {
                addressClaimedMockFn = [this]() { return ClaimedCallback(); };
                addressConflictMockFn = [this]() { ConflictCallback(); };
                frameReceivedMockFn = [this](const Frame *f) { FrameReceivedCallback(f); };
            }

            ~MockCallback() {
                addressClaimedMockFn = {};
                addressConflictMockFn = {};
                frameReceivedMockFn = {};
            }
        };
    } // namespace Tests
}     // namespace J1939
