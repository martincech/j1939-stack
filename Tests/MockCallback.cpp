#include "MockCallback.h"

using namespace J1939;
using namespace Tests;
AddressClaim::AddressClaimerCallbackFn MockCallback::addressClaimedMockFn;
AddressClaim::AddressClaimerCallbackFn MockCallback::addressConflictMockFn;
FrameHandlerFnPtr MockCallback::frameReceivedMockFn;


void Tests::ClaimedCallback() {
    MockCallback::addressClaimedMockFn();
}

void Tests::ConflictCallback() {
    MockCallback::addressConflictMockFn();
}

void Tests::FrameReceivedCallback(const Frame *frame) {
    MockCallback::frameReceivedMockFn(frame);
}
