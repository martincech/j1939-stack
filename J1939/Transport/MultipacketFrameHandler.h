#ifndef __MULTIPACKET_SEQUENCER_H__
#define __MULTIPACKET_SEQUENCER_H__

#include "Addressing/AddressClaimer.h"

#include "Frames/TPCMFrame.h"
#include "Frames/TPDTFrame.h"
#include "J1939Common.h"

#include <map>
#include <vector>


namespace J1939 {
    namespace Transport {
        class MultipacketSession;
        class IncommingSession;

        class ISessionOwner : protected Timer {
            friend class MultipacketSession;
            friend class IncommingSession;

            u32 setTimer(std::chrono::milliseconds duration, TimerHandlerFnPtr handler) override =0;
            void cancelTimer(u32 uid) override =0;
            virtual u8 currentAddress() = 0;

        public:
            virtual void send(const Frame *frame) = 0;
            virtual void receive(const Frame *) = 0;
        };

        /**
         * Handless Transport protocol frames of both types:
         *  - Multi-Packet Broadcast 
         *  - Multi-Packet Peer-to-Peer
         * 
         *  Sending restriction:
         *  --------------------
         *    Node on the network can originate only one Peer-to-Peer specific connection transfer with a given destination at a time.
         *    Node on the network can originate only one multipacket BAM (i.e. global destination) transfer at a given time. 
         *         
         *  Receive restriction:
         *  --------------------
         *  However, responders (i.e. receiving devices) must recognize that multiple multipacket messages can be received, 
         *  interspersed with one another, from different originators (i.e. source addresses).
         *  
         *  A node must also be able to support one RTS/CTS session and one BAM session concurrently 
         *  from the same source address
         *  If multiple RTS messages are received from the same SA for the same PGN, then the most recent RTS message shall be
         *  acted on and the previous RTS messages will be abandoned. No TP.Conn_Abort message shall be sent for the abandoned
         *  RTS messages in this specific case.
         */
        class MultipacketFrameHandler : public FrameHandler, ISessionOwner {
            FrameHandler *frameHandler;
            Timer *timer;
            AddressClaim::IAddressClaimer *claimer;            
            FrameHandlerFnPtr frameHandleFn;
            
            std::map<u16, IncommingSession*> incommingSession;
            void startSession(const TPCM::Frame *frame);
            bool sessionRegistered(const TPCM::Frame *frame);
            void endSession(const IncommingSession *session);

            void process(const TPCM::Frame *frame);
            void process(const TPDT::Frame *frame);

            u32 setTimer(std::chrono::milliseconds duration, TimerHandlerFnPtr handler) override;

            void cancelTimer(u32 uid) override;

            u8 currentAddress() override;
        public:
            /** 
             * Need a low level frame handler to work with and a timer to close connections 
             * Optionally an address claimer needs to be injected if P2P packets should be available
             * (otherwise only global packets received)
             */
            MultipacketFrameHandler(FrameHandler *fh,
                                    Timer *t,
                                    AddressClaim::IAddressClaimer *claimer = nullptr);
            ~MultipacketFrameHandler();

            void send(const Frame *) override;
            void receive(const Frame *) override;
        };
    }
}
#endif

