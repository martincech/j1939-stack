#include "AddressClaimer.h"
#include "Factory.h"
#include "Frames/PGN.h"
#include "Frames/RequestFrame.h"

namespace J1939 {
    namespace AddressClaim {
        using namespace std::chrono;

        AddressClaimer::AddressClaimer(FrameHandler *fh,
                                       Timer *t,
                                       const EcuName::EcuName &name,
                                       const std::queue<u8> &preferred)
            : mEcuName(name)
            , mPrefferedAddresses(preferred)
            , mCurrentAddress(INVALID_ADDRESS)
            , clameTrials(preferred.size())
            , mCurrentStatus(INIT)
            , frameHandler(fh)
            , timer(t) {
            frameHandleFn = [this](const J1939::Frame* frame){
                handleFrameReceived(frame);
            };
            timerHandleFn = [this](){
                handleTimeout();
            };
            frameHandler->registerReceiveHandler(frameHandleFn);            
        }

        void AddressClaimer::claim() {
            if(mCurrentStatus == INIT){
                handleTimeout();
            }
        }

        bool AddressClaimer::addressValid() const {
            return mCurrentStatus == VALID;
        }

        void AddressClaimer::cannotClaimTimerReset() {
            using namespace std::chrono;
            if (mCurrentStatus != INIT) {
                return;
            }
            startTimer(TimeoutControl::RandomTime());
        }

        void AddressClaimer::initTimerReset() {
            using namespace std::chrono;
            if (mCurrentStatus != CONFLICT) {
                return;
            }
            startTimer(TimeoutControl::TAddrClaim);
        }


        void AddressClaimer::handleFrameReceived(const J1939::Frame *frame) {
            switch (frame->getPGN()) {
                case static_cast<u32>(PGN::REQUEST_PGN): {
                    if (addressValid()) {
                        break;
                    }
                    const auto reqFrame = static_cast<const Request::Frame *>(frame);

                    if ((reqFrame->getRequestPGN() == static_cast<u32>(PGN::ADDRESS_CLAIM)) &&
                        ((reqFrame->getDstAddr() == BROADCAST_ADDRESS) || (reqFrame->getDstAddr() == mCurrentAddress))) {

                        //Send claim address
                        sendAddressClaim(mCurrentAddress);
                    }
                    break;
                }

                case static_cast<u32>(PGN::ADDRESS_CLAIM): {
                    const auto rcvFrame = static_cast<const Frame *>(frame);
                    if (rcvFrame->getSrcAddr() != INVALID_ADDRESS && rcvFrame->getSrcAddr() == mCurrentAddress) {
                        //Contending frame!!
                        if (mEcuName < rcvFrame->getEcuName()) {
                            //WIN
                            restartRunningTimer();
                            addressClaimed(mCurrentAddress);
                            break;
                        }
                        //Lose
                        tryToClaimAddress();
                    }

                }
                break;

                default:
                    break;
            }
        }

        void AddressClaimer::addressClaimed(const u32 addr) {
            mCurrentAddress = addr;
            sendAddressClaim(addr);
        }

        void AddressClaimer::stopTimer() {
            if (runningTimerUid != 0) {
                timer->cancelTimer(runningTimerUid);
            }
            runningTimerUid = 0;
        }

        void AddressClaimer::startTimer(const milliseconds ms) {
            stopTimer();
            runningTimerUid = timer->setTimer(ms, timerHandleFn);
            runningTimerMs = ms;
        }

        void AddressClaimer::restartRunningTimer() {
            if (runningTimerUid != 0) {
                startTimer(runningTimerMs);
            }
        }


        void AddressClaimer::sendAddressClaim(const u8 addrToClaim) {
            auto sendFrame = static_cast<Frame *>(Factory::getFrame(PGN::ADDRESS_CLAIM));
            if (sendFrame == nullptr) {
                return;
            }
            sendFrame->setEcuName(mEcuName);
            sendFrame->setSrcAddr(addrToClaim);
            sendFrame->setDstAddr(BROADCAST_ADDRESS);
            frameHandler->send(sendFrame);
        }

        void AddressClaimer::resetTryClameCount() {
            clameTrials = mPrefferedAddresses.size();
        }
        
        void AddressClaimer::callbackFor(std::vector<AddressClaimerCallbackFn> &callbacks) {
            for (const auto &callback : callbacks) {
                callback();
            }
        }
        typedef void (AddressClaimer::*TimerResetFn)();

        void AddressClaimer::tryToClaimAddress() {            
            TimerResetFn resetTimer;
            if (clameTrials) {
                mCurrentAddress = mPrefferedAddresses.front();
                //Introduce again the address at the end of the queue
                mPrefferedAddresses.pop();
                mPrefferedAddresses.push(mCurrentAddress);
                clameTrials--;
                mCurrentStatus = CONFLICT;
                resetTimer = &AddressClaimer::initTimerReset;
            } else {
                mCurrentAddress = INVALID_ADDRESS;
                mCurrentStatus = INIT;
                resetTryClameCount();
                resetTimer = &AddressClaimer::cannotClaimTimerReset;
            }
            callbackFor(addressConflictCallbacks);
            sendAddressClaim(mCurrentAddress);
            std::invoke(resetTimer, this);
        }

        void AddressClaimer::handleTimeout() {
            stopTimer();
            switch (mCurrentStatus) {
                case INIT:
                    tryToClaimAddress();
                    break;
                case CONFLICT:
                    mCurrentStatus = VALID;
                    callbackFor(addressClaimedCallbacks);
                    break;
                default:
                    break;
            }
        }

        u8 AddressClaimer::currentAddress() {
            return mCurrentAddress;
        }

        void AddressClaimer::registerOnAddressClaimed(const AddressClaimerCallbackFn &callback) {
            addressClaimedCallbacks.push_back(callback);
        }

        void AddressClaimer::registerOnAddressConflict(const AddressClaimerCallbackFn &callback) {
            addressConflictCallbacks.push_back(callback);
        }
    } // namespace AddressClaim
}     /* namespace J1939 */
