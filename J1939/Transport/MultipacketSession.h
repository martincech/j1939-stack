#ifndef __MULTIPACKET_SESSION_H__
#define __MULTIPACKET_SESSION_H__

#include "MultipacketFrameHandler.h"
#include <map>

namespace J1939 {
    namespace Transport {

        /**********************************************************************
        * I/O multipacket transport session
        **********************************************************************/
        class MultipacketSession {
            TimerHandlerFnPtr sessionTimeoutHandler;
            u32 runningTimer;
        protected:
            ISessionOwner *owner;

            /* Frames - header and data */
            TPCM::Frame mCmFrame;
            std::map<u8, TPDT::Frame> mDtFrames;
            bool timedOut = false;
            bool aborted = false;

            void startTimer(std::chrono::milliseconds ms);
            void stopTimer();
            virtual void sessionTimeout();
            void finishSession();


            TPCM::Frame *makeSendFrame(TPCM::ControlType) const;
            void sendAbortFrame(TPCM::AbortReason reason) const;

            MultipacketSession(ISessionOwner *owner, const TPCM::Frame &cmFrame);

            virtual u32 dstAddr() const = 0;

        public:
            virtual ~MultipacketSession() = default;
            bool isPartOfSession(const TPDT::Frame &frame) const;

            virtual bool isComplete() const;
            bool isTimedOut() const;
            bool isAborted() const;
            bool isActive() const;

            virtual void newConnectionFrame(const TPCM::Frame &) = 0;

            u16 id() const;
            static u16 id(const TPCM::Frame &cmFrame);
        };

        /**********************************************************************
        * Incomming session
        **********************************************************************/
        class IncommingSession : public MultipacketSession {
        protected:
            void waitForNextDtPacket(std::chrono::milliseconds ms);
            bool isFrameStored(const TPDT::Frame &dtFrame);
            void storeFrame(const TPDT::Frame &dtFrame);
            u32 dstAddr() const override;
        public:

            IncommingSession(ISessionOwner *owner, const TPCM::Frame &cmFrame);

            virtual void newDataFrame(const TPDT::Frame &dtFrame) = 0;
            void newConnectionFrame(const TPCM::Frame &) override;
            bool isComplete() const override;
        };

        /**********************************************************************
        * Incomming P2P session
        **********************************************************************/
        class IncommingP2PSession : public IncommingSession {
            u32 ctsNext;
            u32 ctsCount;
            bool isSeqNOutOfOrder(const TPDT::Frame &dtFrame) const;
            void requestNextChunkOfPackets();
            void sendCtsFrame();
            void sendEomFrame() const;

        protected:
            void sessionTimeout() override;
        public:
            IncommingP2PSession(ISessionOwner *owner, const TPCM::Frame &cmFrame);

            void newDataFrame(const TPDT::Frame &dtFrame) override;
            void newConnectionFrame(const TPCM::Frame &frame) override;
        };

        /**********************************************************************
        * Incomming BAM session
        **********************************************************************/
        class IncommingBAMSession : public IncommingSession {
        public:
            IncommingBAMSession(ISessionOwner *owner, const TPCM::Frame &cmFrame);

            void newDataFrame(const TPDT::Frame &dtFrame) override;
        };


    }
}
#endif

