#include "DPFTestMonitor.h"

namespace J1939 {

    namespace DEUTZ {
        namespace DPFTestMonitor {
            InstalledEatSystem operator|(InstalledEatSystem lhs, InstalledEatSystem rhs) {
                return static_cast<InstalledEatSystem>(static_cast<char>(lhs) | static_cast<char>(rhs));
            }

            StandstillRequestReason operator|(StandstillRequestReason lhs, StandstillRequestReason rhs) {
                return static_cast<StandstillRequestReason>(static_cast<char>(lhs) | static_cast<char>(rhs));
            }

            bool Frame::decodeData(const u8 *buffer, const size_t &length) {
                if (length < getDataLength()) {
                    return false;
                }

                eolStatus = EOL_ROUTINE_STATUS::decode(buffer);
                eolFeedback = EOL_ROUTINE_FEEDBACK::decode(buffer);
                regenerationSuccessfulFlag = REGENERATION_SUCCESSFUL_FLAG::decode(buffer);
                ashLoadExchangeRequest = ASH_LOAD_EXCHANGE_REQUEST::decode(buffer);
                oilExchangeRequest = OIL_EXCHANGE_REQUEST::decode(buffer);
                remainintStandstillRegenerationTime = std::chrono::minutes(buffer[4]);
                installedEatSystem = INSTALLED_EAT_SYSTEM::decode(buffer);
                standstillRegenerationState = STANDSTILL_REGENERATION_STATE::decode(buffer);
                standstillRequestReason = STANDSTILL_REQUEST_REASON::decode(buffer);
                return true;
            }

            bool Frame::encodeData(u8 *buffer) const {
                EOL_ROUTINE_STATUS::encode(buffer, eolStatus);
                EOL_ROUTINE_FEEDBACK::encode(buffer, eolFeedback);
                REGENERATION_SUCCESSFUL_FLAG::encode(buffer, regenerationSuccessfulFlag);
                ASH_LOAD_EXCHANGE_REQUEST::encode(buffer, ashLoadExchangeRequest);
                OIL_EXCHANGE_REQUEST::encode(buffer, oilExchangeRequest);
                buffer[4] = std::chrono::duration_cast<std::chrono::minutes>(remainintStandstillRegenerationTime).count();
                INSTALLED_EAT_SYSTEM::encode(buffer, installedEatSystem);
                STANDSTILL_REGENERATION_STATE::encode(buffer, standstillRegenerationState);
                STANDSTILL_REQUEST_REASON::encode(buffer, standstillRequestReason);
                return true;
            }

            Frame::Frame()
                : J1939::Frame(PGN::DPFTestMonitor, DEFAULT_PRIORITY)
                , eolStatus(EolRoutineStatus::NO_TEST_ONGOING)
                , eolFeedback(EolRoutineFeedback::NO_RESULTS)
                , regenerationSuccessfulFlag(RegenerationSuccessfulFlag::NORMAL)
                , ashLoadExchangeRequest(AshLoadExchangeRequest::NO_REQUEST)
                , oilExchangeRequest(OilExchangeRequest::NO_REQUEST)
                , remainintStandstillRegenerationTime(0)
                , installedEatSystem(InstalledEatSystem::NONE)
                , standstillRegenerationState(StandstillRegenerationState::NOT_ACTIVE)
                , standstillRequestReason(StandstillRequestReason::NONE) {
            }

            Frame::~Frame() = default;

            size_t Frame::getDataLength() const {
                return PACKET_SIZE;
            }

            EolRoutineStatus Frame::getEolStatus() const {
                return eolStatus;
            }

            EolRoutineFeedback Frame::getEolFeedback() const {
                return eolFeedback;
            }

            RegenerationSuccessfulFlag Frame::getRegenerationSuccessfulFlag() const {
                return regenerationSuccessfulFlag;
            }

            AshLoadExchangeRequest Frame::getAshLoadExchangeRequest() const {
                return ashLoadExchangeRequest;
            }

            OilExchangeRequest Frame::getOilExchangeRequest() const {
                return oilExchangeRequest;
            }

            std::chrono::minutes Frame::getRemainintStandstillRegenerationTime() const {
                return remainintStandstillRegenerationTime;
            }

            InstalledEatSystem Frame::getInstalledEatSystem() const {
                return installedEatSystem;
            }

            StandstillRegenerationState Frame::getStandstillRegenerationState() const {
                return standstillRegenerationState;
            }

            StandstillRequestReason Frame::getStandstillRequestReason() const {
                return standstillRequestReason;
            }

        }
    }
}
