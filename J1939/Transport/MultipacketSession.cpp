#include "MultipacketSession.h"
#include "Factory.h"
#include "Frames/GenericMultiFrame.h"
#include <chrono>

namespace J1939 {
    namespace Transport {
        MultipacketSession::MultipacketSession(ISessionOwner *owner, const TPCM::Frame &cmFrame)
            : runningTimer(0)
            , owner(owner)
            , mCmFrame(cmFrame) {
            sessionTimeoutHandler = [this]()
            {
                sessionTimeout();
            };
        }

        bool MultipacketSession::isPartOfSession(const TPDT::Frame &frame) const {
            return frame.getSq() <= mCmFrame.getTotalPackets() &&
                   frame.getSrcAddr() == mCmFrame.getSrcAddr() &&
                   frame.getDstAddr() == mCmFrame.getDstAddr();
        }

        bool MultipacketSession::isComplete() const {
            return true;
        }

        void MultipacketSession::stopTimer() {
            if (runningTimer) {
                owner->cancelTimer(runningTimer);
            }
            runningTimer = 0;
        }

        void MultipacketSession::finishSession() {
            stopTimer();
            if (!isComplete()) {
                return;
            }

            size_t offset = 0;

            const auto length = mCmFrame.getTotalMsgSize();
            std::vector<u8> data;

            for (auto iter = mDtFrames.begin(); iter != mDtFrames.end(); ++iter) {
                auto &dtFrame = iter->second;
                const auto dataLen = length - offset < TPDT::DATA_SIZE ? length - offset : TPDT::DATA_SIZE;
                data.insert(data.end(), dtFrame.getData(), dtFrame.getData() + dataLen);
                offset += dataLen;
                if (offset >= length) {
                    break;
                }
            }
            const auto id = Frame::createId(mCmFrame.getDataPgn(), mCmFrame.getSrcAddr(), mCmFrame.getPriority(), mCmFrame.getDstAddr());
            const auto frame = Factory::getFrame(id, data.data(), data.size());
            if (frame == nullptr) {
                auto multiframe = GenericMultiFrame(mCmFrame.getDataPgn(), mCmFrame.getPriority());
                multiframe.decode(id, data.data(), data.size());
                owner->receive(&multiframe);
            } else {
                owner->receive(frame);
            }
        }

        TPCM::Frame *MultipacketSession::makeSendFrame(TPCM::ControlType type) const {
            auto sendFrame = static_cast<TPCM::Frame *>(Factory::getFrame(PGN::TPCM));
            if (sendFrame != nullptr) {
                sendFrame->setCtrlType(type);
                sendFrame->setSrcAddr(owner->currentAddress());
                sendFrame->setDstAddr(dstAddr());
            }
            return sendFrame;
        }

        void MultipacketSession::sendAbortFrame(TPCM::AbortReason reason) const {
            const auto sendFrame = makeSendFrame(TPCM::ControlType::ABORT);
            sendFrame->setAbortReason(reason);
            owner->send(sendFrame);
        }

        void MultipacketSession::startTimer(const std::chrono::milliseconds ms) {
            stopTimer();
            runningTimer = owner->setTimer(ms, sessionTimeoutHandler);
        }

        void MultipacketSession::sessionTimeout() {
            timedOut = true;
            finishSession();
        }

        bool MultipacketSession::isTimedOut() const { return timedOut; }

        bool MultipacketSession::isAborted() const { return aborted; }

        bool MultipacketSession::isActive() const { return !isComplete() && !isTimedOut() && !isAborted(); }

        u16 MultipacketSession::id() const {
            return id(mCmFrame);
        }

        u16 MultipacketSession::id(const TPCM::Frame &cmFrame) {
            if (cmFrame.getCtrlType() == TPCM::ControlType::RTS ||
                cmFrame.getCtrlType() == TPCM::ControlType::BAM) {
                return cmFrame.getSrcAddr() << 8 | cmFrame.getDstAddr();
            }
            return cmFrame.getDstAddr() << 8 | cmFrame.getSrcAddr();
        }
    } // namespace Transport
}     // namespace J1939
