#ifndef __GENERIC_MULTI_FRAME_H__
#define __GENERIC_MULTI_FRAME_H__

#include "J1939Frame.h"
#include <vector>

namespace J1939 {
    class GenericMultiFrame : public Frame {
    protected:
        std::vector<u8> data;
        bool decodeData(const u8 *buffer, const size_t &length) override;
        bool encodeData(u8 *buffer) const override;

    public:
        GenericMultiFrame(PGN pgn);
        GenericMultiFrame(PGN pgn, u8 priority);
        GenericMultiFrame(u32 pgn);
        GenericMultiFrame(u32 pgn, u8 priority);

        ~GenericMultiFrame();

        size_t getDataLength() const override;
        const u8 *getRawData() const;
    };
} // namespace J1939

#endif
