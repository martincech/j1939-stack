#ifndef __STOUT_FRAME_H__
#define __STOUT_FRAME_H__

#include "Frames/J1939Frame.h"
#include "SPN/SPN.h"

namespace J1939 {

    namespace DEUTZ {
        namespace STOUT {
            constexpr auto PACKET_SIZE = 8;
            constexpr auto DEFAULT_PRIORITY = 6;
            using namespace SPN;

            enum class ScrWarning {
                /* 0b000 = Off*/
                OFF = 0b000,
                /* 0b001 = On - solid*/
                ON = 0b001,
                /* 0b010 - Flashing with 0.5 Hz */
                ON_0_5_HZ = 0b010,
                /* 0b011 - Flashing with 1 Hz */
                ON_1_HZ = 0b011,
                /* 0b100 - Flashing with 2 Hz */
                ON_2_HZ = 0b100,
                /* 0b111 - Not Available */
                NOT_AVAILABLE = 0b111
            };

            using SCR_WARNING = SPN_STATUS<ScrWarning, 3, 1, 3>;

            enum class EngineRunningLamp {
                /* 0b00 = Off*/
                OFF = 0b00,
                /* 0b01 = On*/
                ON = 0b01,
                /* 0b11 - Not Available */
                NOT_AVAILABLE = 0b11
            };

            using ENGINE_RUNNING_LAMP = SPN_STATUS<EngineRunningLamp, 7, 1, 2>;

            enum class StarterOutput {
                /* 0b00 = Off*/
                OFF = 0b00,
                /* 0b01 = On*/
                ON = 0b01,
                /* 0b10 - Error */
                ERROR = 0b10
            };

            using STARTER_OUTPUT = SPN_STATUS<StarterOutput, 7, 5, 2>;

            using CoolantTemperatureWarning = EngineRunningLamp;
            using COOLANT_TEMPERATURE_WARNING = SPN_STATUS<CoolantTemperatureWarning, 8, 1, 2>;

            using OilPressureWarning = EngineRunningLamp;
            using OIL_PRESSURE_WARNING = SPN_STATUS<CoolantTemperatureWarning, 8, 3, 2>;

            using PreheatLamp = EngineRunningLamp;
            using PREHEAT_LAMP = SPN_STATUS<CoolantTemperatureWarning, 8, 5, 2>;


            /**
            * Parameter Group Name             Proprietary - Deutz  State of Outputs 
            * Parameter Group Number           65291 (00FF0Bhex)
            * Definition                       This PGN contains information about various lamps and outputs 
            *                                  about DEUTZ Stage V engine.
            * Transmission                     1000 ms
            * Data Length                      8 bytes (CAN DLC = 8)
            * Extended Data Page (R)           0
            * Data Page                        0
            * PDU Format                       255
            * PDU Specific                     11
            * Default Priority                 6
            * Data Description                 BYTE.BIT    SPN     LABEL                
            *                                  1           N/A     not used (0x00)
            *                                  2           N/A     not used (0xFF)
            *                                  3.1-3.3     N/A     SCR warning/EAT inducement symbol     
            *                                  3.4-4       N/A     not used (0x1FFF)
            *                                  5           N/A     Torque Output (1%/bit, 0 offset)
            *                                  6           N/A     Fan Output (1%/bit, 0 offset)
            *                                  7.1-7.2     N/A     ECU On Lamp or / Engine Running Lamp*
            *                                  7.3-7.4     N/A     not used (0b00)
            *                                  7.5-7.6     N/A     Starter Output
            *                                  7.7-7.8     N/A     not used (0b11)
            *                                  8.1-8.2     N/A     Engine Coolant Temperature /
            *                                                      Level Warning Lamp / 
            *                                                      Charge Air Temperature Warning Lamp 
            *                                  8.3-8.4     N/A     Oil Pressure Warning Lamp / 
            *                                                      Engine Low Temperature Lamp
            *                                  8.5-8.6     N/A     Preheat Lamp     
            *                                  8.7-8.8     N/A     not used (0b00)          
            */

            class Frame : public J1939::Frame {
                ScrWarning scrWarning;
                uint8_t torqueOutput;
                uint8_t fanOutput;
                EngineRunningLamp engineRunningLamp;
                StarterOutput starterOutput;
                CoolantTemperatureWarning coolantTemperatureWarning;
                OilPressureWarning oilPressureWarning;
                PreheatLamp preheatLamp;
            protected:
                bool decodeData(const u8 *buffer, const size_t &length) override;
                bool encodeData(u8 *buffer) const override;

            public:
                Frame();
                virtual ~Frame();
                size_t getDataLength() const override;


                ScrWarning getScrWarning() const;
                uint8_t getTorqueOutput() const;
                uint8_t getFanOutput() const;
                EngineRunningLamp getEngineRunningLamp() const;
                StarterOutput getStarterOutput() const;
                CoolantTemperatureWarning getCoolantTemperatureWarning() const;
                OilPressureWarning getOilPressureWarning() const;
                PreheatLamp getPreheatLamp() const;
            };
        } // namespace STOUT
    }     // namespace DEUTZ
}         /* namespace J1939 */

#endif /* __STOUT_FRAME_H__ */
