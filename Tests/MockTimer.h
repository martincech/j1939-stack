#pragma once
#include "J1939Common.h"
#include <gmock/gmock.h>

namespace J1939 {
    namespace Tests {
        class MockTimer : public Timer {
        public:
            MOCK_METHOD2(setTimer, u32(std::chrono::milliseconds duration, J1939::TimerHandlerFnPtr handler));
            MOCK_METHOD1(cancelTimer, void(u32 uid));
        };
    } // namespace Tests
}     // namespace J1939
