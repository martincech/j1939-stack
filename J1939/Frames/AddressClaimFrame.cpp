#include "AddressClaimFrame.h"
#include "PGN.h"

#if defined(DEBUG) && defined(J1939_STRINGS)
#include <ostream>
#include <sstream>
#include <string>
constexpr auto MULTIFRAME_NAME = "Address Claim";
#endif

namespace J1939 {
    namespace AddressClaim {
        Frame::Frame()
            : Frame(EcuName::EcuName()) {
        }

        Frame::Frame(const EcuName::EcuName &name)
            : J1939::Frame(PGN::ADDRESS_CLAIM,
                           DEFAULT_PRIORITY
#if defined(DEBUG) && defined(J1939_STRINGS)
                           ,
                           MULTIFRAME_NAME
#endif
                          )
            , mEcuName(name) {
        }

        Frame::~Frame() = default;

        size_t Frame::getDataLength() const { return PACKET_SIZE; }

        void Frame::setEcuName(EcuName::EcuName &ecuName) {
            mEcuName = ecuName;
        }

        const EcuName::EcuName &Frame::getEcuName() const { return mEcuName; }

        namespace EcuName {
            EcuName::EcuName() = default;

            EcuName::EcuName(const u32 idNumber, const u16 manufacturerCode, const u8 ecuInstance, const u8 functionInstance, const u8 function, const u8 vehicleSystem, const u8 vehicSystemInstance, const u8 industryGroup, const bool arbitraryAddressCapable)
                : mIdNumber(idNumber)
                , mManufacturerCode(manufacturerCode)
                , mEcuInstance(ecuInstance)
                , mFunctionInstance(functionInstance)
                , mFunction(function)
                , mVehicleSystem(vehicleSystem)
                , mVehicleSystemInstance(vehicSystemInstance)
                , mIndustryGroup(industryGroup)
                , mArbitraryAddressCapable(arbitraryAddressCapable) {
            }

            EcuName::~EcuName() = default;

            u64 EcuName::getValue() const {

                return (u64)(mArbitraryAddressCapable ? ARBITRARY_ADDR_CAPABLE_MASK : 0) << ARBITRARY_ADDR_CAPABLE_OFFSET |
                       (u64)(mIndustryGroup & INDUSTRY_GROUP_MASK) << INDUSTRY_GROUP_OFFSET |
                       (u64)(mVehicleSystemInstance & VEHICLE_SYSTEM_INTERFACE_MASK) << VEHICLE_SYSTEM_INTERFACE_OFFSET |
                       (u64)(mVehicleSystem & VEHICLE_SYSTEM_MASK) << VEHICLE_SYSTEM_OFFSET |
                       (u64)(mFunction & FUNCTION_MASK) << FUNCTION_OFFSET |
                       (u64)(mFunctionInstance & FUNCTION_INSTANCE_MASK) << FUNCTION_INSTANCE_OFFSET |
                       (u64)(mEcuInstance & ECU_INSTANCE_MASK) << ECU_INSTANCE_OFFSET |
                       (u64)(mManufacturerCode & MANUFACTURER_CODE_MASK) << MANUFACTURER_CODE_OFFSET |
                       (u64)(mIdNumber & IDENTITY_NUMBER_MASK) << IDENTITY_NUMBER_OFFSET;
            }

            bool EcuName::operator<(const EcuName &other) const {

                return getValue() < other.getValue();
            }

            bool EcuName::operator>(const EcuName &other) const {
                return getValue() > other.getValue();
            }

            bool EcuName::isArbitraryAddressCapable() const {
                return mArbitraryAddressCapable;
            }

            void EcuName::setArbitraryAddressCapable(bool arbitraryAddressCapable) {
                mArbitraryAddressCapable = arbitraryAddressCapable;
            }

            u8 EcuName::getEcuInstance() const {
                return mEcuInstance;
            }

            void EcuName::setEcuInstance(const u8 ecuInstance) {
                mEcuInstance = ecuInstance;
            }

            u8 EcuName::getFunction() const {
                return mFunction;
            }

            void EcuName::setFunction(const u8 function) {
                mFunction = function;
            }

            u8 EcuName::getFunctionInstance() const {
                return mFunctionInstance;
            }

            void EcuName::setFunctionInstance(const u8 functionInstance) {
                mFunctionInstance = functionInstance;
            }

            u32 EcuName::getIdNumber() const {
                return mIdNumber;
            }

            void EcuName::setIdNumber(const u32 idNumber) {
                mIdNumber = idNumber;
            }

            u8 EcuName::getIndustryGroup() const {
                return mIndustryGroup;
            }

            void EcuName::setIndustryGroup(const u8 industryGroup) {
                mIndustryGroup = industryGroup;
            }

            u16 EcuName::getManufacturerCode() const {
                return mManufacturerCode;
            }

            void EcuName::setManufacturerCode(const u16 manufacturerCode) {
                mManufacturerCode = manufacturerCode;
            }

            u8 EcuName::getVehicleSystem() const {
                return mVehicleSystem;
            }

            void EcuName::setVehicleSystem(const u8 vehicleSystem) {
                mVehicleSystem = vehicleSystem;
            }

            u8 EcuName::getVehicleSystemInstance() const {
                return mVehicleSystemInstance;
            }

            void EcuName::setVehicleSystemInstance(u8 vehicleSystemInstance) {
                mVehicleSystemInstance = vehicleSystemInstance;
            }
        } // namespace EcuName
        bool Frame::decodeData(const u8 *buffer, const size_t &length) {
            if (length != getDataLength()) {
                return false;
            }
            mEcuName = EcuName::EcuName();
            mEcuName.setIdNumber(buffer[0] | buffer[1] << 8 | (buffer[2] & 0x1F) << 16);
            mEcuName.setManufacturerCode((buffer[2] & 0xE0) >> 5 | buffer[3] << 3);
            mEcuName.setEcuInstance(buffer[4] & 0x07);
            mEcuName.setFunctionInstance((buffer[4] & 0xF8) >> 3);
            mEcuName.setFunction(buffer[5]);
            mEcuName.setVehicleSystem(buffer[6] >> 1);
            mEcuName.setVehicleSystemInstance(buffer[7] & 0x0F);
            mEcuName.setIndustryGroup(buffer[7] >> 4 & 0x07);
            mEcuName.setArbitraryAddressCapable(buffer[7] >> 7);
            return true;
        }

        bool Frame::encodeData(u8 *buffer) const {
            buffer[0] = mEcuName.getIdNumber() & 0xFF;
            buffer[1] = mEcuName.getIdNumber() >> 8 & 0xFF;
            buffer[2] = (mEcuName.getIdNumber() >> 16 & 0x1F) |
                        (mEcuName.getManufacturerCode() << 5 & 0xFF);
            buffer[3] = mEcuName.getManufacturerCode() >> 3 & 0xFF;
            buffer[4] = (mEcuName.getEcuInstance() & EcuName::ECU_INSTANCE_MASK) |
                        (mEcuName.getFunctionInstance() << 3 & 0xFF);
            buffer[5] = mEcuName.getFunction();
            buffer[6] = mEcuName.getVehicleSystem() << 1;
            buffer[7] = (mEcuName.getVehicleSystemInstance() & EcuName::VEHICLE_SYSTEM_INTERFACE_MASK) |
                        (mEcuName.getIndustryGroup() & EcuName::INDUSTRY_GROUP_MASK) << 4 |
                        (mEcuName.isArbitraryAddressCapable() ? 1 : 0) << 7;
            return true;
        }
#if defined(DEBUG) && defined(J1939_STRINGS)
        std::string Frame::toString() {

            const auto retVal = Frame::toString();
            std::stringstream sstr;

            sstr << "Id Number: " << mEcuName.getIdNumber() << std::endl;
            sstr << "Manufacturer Code: " << static_cast<u32>(mEcuName.getManufacturerCode()) << std::endl;
            sstr << "ECU Intance: " << static_cast<u32>(mEcuName.getEcuInstance()) << std::endl;
            sstr << "Function Instance: " << static_cast<u32>(mEcuName.getFunctionInstance()) << std::endl;
            sstr << "Function: " << static_cast<u32>(mEcuName.getFunction()) << std::endl;
            sstr << "Vehicle System: " << static_cast<u32>(mEcuName.getVehicleSystem()) << std::endl;
            sstr << "Vehicle System Instance: " << static_cast<u32>(mEcuName.getVehicleSystemInstance()) << std::endl;
            sstr << "Industry Group: " << static_cast<u32>(mEcuName.getIndustryGroup()) << std::endl;
            sstr << "Address Capable: " << (mEcuName.isArbitraryAddressCapable() ? "yes" : "no") << std::endl;

            return retVal + sstr.str();
        }
#endif
    } // namespace AddressClaim
}     /* namespace J1939 */
