#include "MultipacketFrameHandler.h"
#include "MultipacketSession.h"

namespace J1939 {
    namespace Transport {
        MultipacketFrameHandler::MultipacketFrameHandler(FrameHandler *fh, Timer *t, AddressClaim::IAddressClaimer *claimer)
            : frameHandler(fh)
            , timer(t)
            , claimer(claimer) {
            frameHandleFn = [this](const Frame *frame)
            {
                receive(frame);
            };
            frameHandler->registerReceiveHandler(frameHandleFn);
            // TODO register address claimed and conflict for claimer
        }

        MultipacketFrameHandler::~MultipacketFrameHandler() = default;

        void MultipacketFrameHandler::send(const Frame *frame) {
            if(claimer == nullptr)
            {
                return;
            }
            if (frame->getDataLength() > MAX_PACKET_SIZE) {
                //TODO send multipacket message

            } else {
                //just proxy
                frameHandler->send(frame);
            }
        }

        void MultipacketFrameHandler::receive(const Frame *frame) {
            if (frame == nullptr ||
                (frame->getDstAddr() != BROADCAST_ADDRESS &&
                 (claimer != nullptr && frame->getDstAddr() != claimer->currentAddress()))) {
                // not for me
                return;
            }

            const auto pgn = frame->getPGN();
            switch (pgn) {
                case static_cast<u32>(PGN::TPCM): {
                    const auto rcvFrame = static_cast<const TPCM::Frame *>(frame);
                    process(rcvFrame);
                }
                break;
                case static_cast<u32>(PGN::TPDT): {
                    const auto rcvFrame = static_cast<const TPDT::Frame *>(frame);
                    process(rcvFrame);
                    break;
                }
                default:
                    FrameHandler::receive(frame);
                    break;
            }
        }

        void MultipacketFrameHandler::process(const TPCM::Frame *frame) {
            if (!frame->isValid()) {
                return;
            }

            switch (frame->getCtrlType()) {
                case TPCM::ControlType::ABORT:
                case TPCM::ControlType::EOM_ACK:
                case TPCM::ControlType::CTS:
                    if (sessionRegistered(frame)) {
                        auto sessionPair = incommingSession.find(MultipacketSession::id(*frame));
                        sessionPair->second->newConnectionFrame(*frame);
                    }
                    break;
                case TPCM::ControlType::RTS:
                case TPCM::ControlType::BAM:
                    if (!sessionRegistered(frame)) {
                        startSession(frame);
                    }
                    break;
                default:
                    break;
            }
        }

        void MultipacketFrameHandler::process(const TPDT::Frame *frame) {
            IncommingSession *session = nullptr;
            if (!frame->isValid()) {
                return;
            }
            for (auto &sessionPair : incommingSession) {
                if (sessionPair.second->isPartOfSession(*frame)) {
                    session = sessionPair.second;
                    break;
                }
            }
            if (session == nullptr) {
                return;
            }

            session->newDataFrame(*frame);
            if (!session->isActive()) {
                endSession(session);
            }
        }

        void MultipacketFrameHandler::startSession(const TPCM::Frame *frame) {
            if (frame->getCtrlType() == TPCM::ControlType::RTS) {
                incommingSession[MultipacketSession::id(*frame)] =
                    new IncommingP2PSession(this, *frame);
            } else if (frame->getCtrlType() == TPCM::ControlType::BAM) {
                incommingSession[MultipacketSession::id(*frame)] =
                    new IncommingBAMSession(this, *frame);
            }
        }

        bool MultipacketFrameHandler::sessionRegistered(const TPCM::Frame *frame) {
            return incommingSession.find(MultipacketSession::id(*frame)) != incommingSession.end();
        }

        void MultipacketFrameHandler::endSession(const IncommingSession *session) {
            const auto id = session->id();
            delete incommingSession[id];
            incommingSession.erase(id);
        }

        u32 MultipacketFrameHandler::setTimer(std::chrono::milliseconds duration, TimerHandlerFnPtr handler) {
            return timer->setTimer(duration, handler);
        }

        void MultipacketFrameHandler::cancelTimer(u32 uid) {
            timer->cancelTimer(uid);
        }

        u8 MultipacketFrameHandler::currentAddress() {
            if(claimer == nullptr){return INVALID_ADDRESS;}
            return claimer->currentAddress();
        }
    } // namespace Transport
}     // namespace J1939
