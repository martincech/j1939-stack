#ifndef __FACTORY_H__
#define __FACTORY_H__

#include <map>
#include <set>
#include "Frames/J1939Frame.h"

namespace J1939 {

    /**
     * Factory to create frames. 
     * New frames should be created using this factory. 
     * It's main purpose is to get the concrete Frame type and decode the incoming data.
     * Frame has to be registered into factory first. Even the stack implemented frames (call registerStackFrames)
     * Dynamic registration/unregistration available.
     */
    class Factory {
        static std::map<u32, Frame*> mFrames;
        Factory();
        virtual ~Factory();
    public:                
        static Frame *getFrame(u32 id, const u8 *data, size_t length);
        static Frame *getFrame(u32 pgn);
        static Frame *getFrame(PGN pgn);      
        static std::set<u32> getAllRegisteredPGNs();

        static void registerStackFrames();        
        static bool registerFrame(Frame *);
        static void unregisterAllFrames();
        static void unRegisterFrame(u32 pgn);
#if defined(DEBUG) && defined(J1939_STRINGS)
        static Frame *getFrame(const std::string &name);
#endif

    };

} /* namespace J1939 */

#endif /* __FACTORY_H__ */
