#ifndef __TRANSPORT_DT_FRAME_H__
#define __TRANSPORT_DT_FRAME_H__

#include "J1939Frame.h"

namespace J1939 {
    namespace Transport {
        namespace TPDT {
            constexpr auto DEFAULT_PRIORITY = 7;
            constexpr auto DATA_SIZE = 7;
            constexpr auto SEQ_SIZE = 1;
            constexpr auto PACKET_SIZE = DATA_SIZE + SEQ_SIZE;

            /**
            * Parameter Group Name             Transport Protocol - Data Transfer (TP.DT)
            * Parameter Group Number           60160 (00EB00hex)
            * Definition                       Data Transfer of Multi-Packet Messages
            * Transmission Rate                According to the Parameter Group Number to be transferred
            * Data Length                      8 bytes
            * Extended Data Page (R)           0
            * Data Page                        0
            * PDU Format                       235
            * PDU Specific                     Destination Address
            * Default Priority                 7
            * Data Description
            * Byte                             1 - Sequence Number (1 to 255)
            *                                  2-8 - Data
            */
            class Frame : public J1939::Frame {
                u8 mSQNum;
                u8 mData[DATA_SIZE]{};

            protected:
                bool decodeData(const u8 *buffer, const size_t &length) override;
                bool encodeData(u8 *buffer) const override;

            public:
                Frame();
                Frame(u8 sq, u8 *data, size_t length);
                virtual ~Frame();

                bool isValid() const;
                size_t getDataLength() const override;
                const u8 *getData() const;
                void setData(u8 *data, u8 length);

                u8 getSq() const;
                void setSq(u8 sq);

#if defined(DEBUG) && defined(J1939_STRINGS)
                std::string toString() override;
#endif
            };
        } // namespace TPDT
    }     // namespace Transport
}         /* namespace J1939 */

#endif /* __TRANSPORT_DT_FRAME_H__ */
