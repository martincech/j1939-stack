#ifndef __PGN__H_
#define __PGN__H_

namespace J1939 {
    /**
     * Parameter Group Number (PGN) as specified in SAE J1939/71
     * Parameter Group Numbers point to vehicle signals and parameters which are defined as
     * Parameter Groups. Parameter Groups in turn contain information on parameter assignments
     * within the 8 byte CAN data field of each message as well as repetition rate and priority
     * The CAN extended identifier is split into a priority field
     * a Parameter Group Number (PGN) to identify the content of the data field, and the source address.
     * A PGN identifies a message's function and associated data.
     *
     * DP   PGN Range(hex)      Number of PGNs              SAE or                  Communication
     *                                              Manufacturer Assigned
     * ------------------------------------------------------------------------------------------
     * 0    000000 - 00EE00     239                         SAE                     PDU1 = Peer-to-Peer
     * 0    00EF00              1                           MF                      PDU1 = Peer-to-Peer (Proprietary A)
     * 0    00F000 - 00FEFF     3840                        SAE                     PDU2 = Broadcast
     * 0    00FF00 - 00FFFF     256                         MF                      PDU2 = Broadcast    (Proprietary B)    
     * 1    010000 - 01EE00     239                         SAE                     PDU1 = Peer-to-Peer
     * 1    01EF00              1                           MF                      PDU1 = Peer-to-Peer (Proprietary A2)
     * 1    01F000 - 01FEFF     3840                        SAE                     PDU2 = Broadcast
     * 1    01FF00 - 01FFFF     256                         MF                      PDU2 = Broadcast    (Proprietary B2)
     */
    enum class PGN {
        /* Request 2*/
        REQUEST_2 = 0x00C900,
        /* Request 2 transfer */
        REQUEST_2_TRANSFER = 0x00CA00,
        /* Cab Message 1 */
        CM1 = 0x00E000,
        //TODO
        /* Acknowledgement */
        ACK = 0x00E800,
        /* Request for PGN*/
        REQUEST_PGN = 0x00EA00,
        /* Transport Protocol - Data Transfer (TP.DT) */
        TPDT = 0x00EB00,
        /* Transport Protocol - Connection Management (TP.CM) */
        TPCM = 0x00EC00,
        /* Address claim request */
        ADDRESS_CLAIM = 0x00EE00,
        /* Proprietary A (DP 0) */
        PROPRIETARY_A = 0x00EF00,
        /* Proprietary A2 (DP 1) */
        PROPRIETARY_A2 = 0x01EF00,
        /* Proprietary B (DP 0) */
        PROPRIETARY_B_FIRST = 0x00FF00,
        /* DEUTZ State of outputs packet */
        STOUT = 0x00FF0B,
        /* DEUTZ DPF Test monitor packet */
        DPFTestMonitor = 0x00FF48,
        PROPRIETARY_B_LAST = 0x00FFFF,
        /* Proprietary B (DP 1) */
        PROPRIETARY_B2_FIRST = 0x01FF00,
        PROPRIETARY_B2_LAST = 0x01FFFF,
        /* Aftertreatment 2 Service 1 */
        AT2S1 = 0x00FD7A,
        //TODO
        /* Aftertreatment 1 Service 1 */
        AT1S1 = 0x00FD7B,
        //TODO
        /* Diesel Particulate Filter Control 1 */
        DPFC1 = 0x00FD7C,
        /* Diesel Particulate Filter Control 2 */
        DPFC2 = 0x00FCE0,
        //TODO
    };
} // namespace J1939
#endif /* __PGN__H_ */
