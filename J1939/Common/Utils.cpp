#include "Utils.h"
#include <chrono>

namespace Utils {

    TimeStamp TimeStamp::operator-(const TimeStamp &other) const {

        TimeStamp retVal(*this);

        if (other.mMicroSec > mMicroSec) {
            if (other.mSeconds >= mSeconds) {
                return TimeStamp();
            }
            retVal.mMicroSec = mMicroSec + 1000000;
            retVal.mSeconds = mSeconds - 1;
        }

        if (other.mSeconds > retVal.mSeconds) {
            return TimeStamp();
        }

        retVal.mSeconds = retVal.mSeconds - other.mSeconds;
        retVal.mMicroSec = retVal.mMicroSec - other.mMicroSec;

        return retVal;
    }

    TimeStamp TimeStamp::operator+(const TimeStamp &other) const {

        TimeStamp retVal(*this);

        retVal.mSeconds = retVal.mSeconds + other.mSeconds;
        retVal.mMicroSec = retVal.mMicroSec + other.mMicroSec;

        if (retVal.mMicroSec > 1000000) {
            retVal.mMicroSec -= 1000000;
            ++retVal.mSeconds;
        }

        return retVal;
    }

    bool TimeStamp::operator>(const TimeStamp &other) const {

        if (mSeconds > other.mSeconds)
            return true;

        if (mSeconds == other.mSeconds && mMicroSec > other.mMicroSec)
            return true;

        return false;
    }

    bool TimeStamp::operator<(const TimeStamp &other) const {

        if (mSeconds < other.mSeconds)
            return true;

        if (mSeconds == other.mSeconds && mMicroSec < other.mMicroSec)
            return true;

        return false;
    }

    bool TimeStamp::operator<=(const TimeStamp &other) const {
        return !(*this > other);
    }

    bool TimeStamp::operator>=(const TimeStamp &other) const {
        return !(*this < other);
    }

    TimeStamp TimeStamp::operator-(u32 millis) const {

        TimeStamp aux(millis / 1000, (millis % 1000) * 1000);

        return (*this - aux);
    }

    TimeStamp TimeStamp::operator+(u32 millis) const {

        const TimeStamp aux(millis / 1000, (millis % 1000) * 1000);

        return (*this + aux);
    }

    TimeStamp TimeStamp::now() {

        auto now = std::chrono::steady_clock::now();
        const auto duration = now.time_since_epoch();
        auto micro = std::chrono::duration_cast<std::chrono::microseconds>(duration);

        return TimeStamp(micro.count() / 1000000, micro.count() % 1000000);
    }

} // namespace Utils
