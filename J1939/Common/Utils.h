#ifndef __UTILS_H__
#define __UTILS_H__

#define J1939_MIN(a, b) ((a) < (b) ? (a) : (b))
#define J1939_MAX(a, b) ((a) > (b) ? (a) : (b))
#include "J1939Common.h"

namespace Utils {

    class TimeStamp {
      private:
        u32 mSeconds;
        u32 mMicroSec;

      public:
        TimeStamp() : mSeconds(0), mMicroSec(0) {}
        TimeStamp(u32 seconds, u32 microSec) : mSeconds(seconds), mMicroSec(microSec) {}
        ~TimeStamp() {}
        u32 getMicroSec() const { return mMicroSec; }
        void setMicroSec(u32 microSec) { mMicroSec = microSec; }
        u32 getSeconds() const { return mSeconds; }
        void setSeconds(u32 seconds) { mSeconds = seconds; }

        TimeStamp operator-(const TimeStamp &other) const;
        TimeStamp operator+(const TimeStamp &other) const;
        TimeStamp operator-(u32 millis) const;
        TimeStamp operator+(u32 millis) const;
        bool operator==(const TimeStamp &other) const { return (mSeconds == other.mSeconds && mMicroSec == other.mMicroSec); };
        bool operator>(const TimeStamp &other) const;
        bool operator<(const TimeStamp &other) const;
        bool operator<=(const TimeStamp &other) const;
        bool operator>=(const TimeStamp &other) const;

        static TimeStamp now();
    };

} // namespace Utils

#endif /* __UTILS_H__ */
