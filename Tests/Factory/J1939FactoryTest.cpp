#include "Factory.h"
#include "Frames/AddressClaimFrame.h"
#include "Frames/PGN.h"
#include "TestFrame.h"
#include <gtest/gtest.h>
#include <ostream>
#include <vector>

using namespace J1939;

namespace J1939FactoryTests {
    class J1939FactoryTest : public testing::Test {

    public:
        TestFrame frame1 = TestFrame(0xDE00); // PDUF1
        TestFrame frame2 = TestFrame(0xAF00); // PDUF1
        TestFrame frame3 = TestFrame(0xFEEF); // PDUF2
        TestFrame frame4 = TestFrame(0xDB00); // PDUF1
        TestFrame frame5 = TestFrame(0xF0FF); // PDUF2

        J1939FactoryTest() = default;

        void SetUp() override {
            Factory::registerFrame(&frame1);
            Factory::registerFrame(&frame2);
            Factory::registerFrame(&frame3);

            Factory::registerFrame(&frame4);
            Factory::registerFrame(&frame5);
            Factory::unRegisterFrame(frame4.getPGN());
            Factory::unRegisterFrame(frame5.getPGN());
        }

        void TearDown() override {
            Factory::unregisterAllFrames();
        }
    };

    struct FrameTestParams {
        u32 id;
        u32 pgn;
        u8 srcAddr;
        u8 dstAddr;
        std::vector<u8> rawData;

        friend std::ostream &operator<<(std::ostream &os, const FrameTestParams &obj) {
            return os
                   << "id: " << obj.id
                   << " pgn: " << obj.pgn
                   << " srcAddr: " << obj.srcAddr
                   << " dstAddr: " << obj.dstAddr;
        }
    };

    struct J1939FactoryParametrizedTest : J1939FactoryTest, testing::WithParamInterface<FrameTestParams> {
    };

    using RegisteredFramesTest = J1939FactoryParametrizedTest;
    TEST_P(RegisteredFramesTest, RegisteredFrame) {
        auto param = GetParam();
        auto frame = Factory::getFrame(param.id, param.rawData.data(), param.rawData.size());

        ASSERT_EQ(frame->getPGN(), param.pgn);
        ASSERT_EQ(frame->getSrcAddr(), param.srcAddr);
        ASSERT_EQ(frame->getDstAddr(), param.dstAddr);

        auto testFrame = static_cast<TestFrame *>(frame);

        ASSERT_EQ(memcmp(param.rawData.data(), testFrame->getRaw().c_str(), param.rawData.size()), 0);
    }

    INSTANTIATE_TEST_CASE_P(RegisteredFrames, RegisteredFramesTest,
        testing::Values(
            FrameTestParams{
                0x00DEAA50, //id
                0x00DE00,   //pgn
                0x00000050, // src
                0x0000AA,   // dst
                std::vector<u8>{0xAB, 0xCD, 0xEF, 0x01, 0x23, 0x45, 0x67, 0x89}
            },
            FrameTestParams{
                0x00AFCC60, //id
                0x00AF00,   //pgn
                0x00000060, // src
                0x0000CC,   // dst
                std::vector<u8>{0x01, 0x23, 0x45, 0x67, 0x89, 0x23, 0x45, 0x67, 0x89, 0x67, 0x89, 0x67, 0x89}
            },
            FrameTestParams{
                0x00FEEF40,        //id
                0x00FEEF,          //pgn
                0x00000040,        // src
                BROADCAST_ADDRESS, // dst
                std::vector<u8>{0xBA, 0xDC, 0xFE, 0x10, 0x32, 0x54, 0x76, 0x98, 0x67, 0xAA, 0xBB, 0xEE, 0xDD}
            }
        )
    );

    using UnregisteredFramesTest = J1939FactoryParametrizedTest;
    TEST_P(UnregisteredFramesTest, UnregisteredFrame) {
        auto param = GetParam();
        auto frame = Factory::getFrame(param.id, param.rawData.data(), param.rawData.size());
        ASSERT_EQ(frame, nullptr);
        frame = Factory::getFrame(param.pgn);
        ASSERT_EQ(frame, nullptr);
        auto pgns = Factory::getAllRegisteredPGNs();
        ASSERT_EQ(pgns.end(), pgns.find(param.pgn));
    }


    INSTANTIATE_TEST_CASE_P(UnegisteredFrames, UnregisteredFramesTest,
        testing::Values(
            FrameTestParams{ //random
                0x00ABCDEF,  //id
                0x00AB00,    //pgn
                0x000000EF,  // src
                0x0000CD,    // dst
                std::vector<u8>{}
            },
            FrameTestParams{ //frame4
                0x00DB0011,  //id
                0x00DB00,    //pgn
                0x00000011,  // src
                0x000000,    // dst
                std::vector<u8>{0xDE, 0xAD, 0xBE, 0xEF}
            },
            FrameTestParams{       //frame5
                0x00F0FFEF,        //id
                0x00F0FF,          //pgn
                0x000000EF,        // src
                BROADCAST_ADDRESS, // dst
                std::vector<u8>{0xDE, 0xAD, 0xBE, 0xEF}
            }
        )
    );

    TEST_F(J1939FactoryTest, AddressClaimRegistered) {
        const auto src = 0xBB;
        const auto id = static_cast<u32>(PGN::ADDRESS_CLAIM) << ID::PGN_OFFSET | src;
        const u8 raw[] = {0x2A, 0x65, 0x00, 0x9D, 0x85, 0xF1, 0xB4, 0x2F};

        /* Not yet registerd*/
        auto frame = Factory::getFrame(id, raw, sizeof raw);
        ASSERT_EQ(frame, nullptr);

        /* Register and test parsing */
        Factory::registerStackFrames();
        frame = Factory::getFrame(id, raw, sizeof raw);
        ASSERT_NE(frame, nullptr);
        ASSERT_EQ(frame->getPGN(), static_cast<u32>(PGN::ADDRESS_CLAIM));
        ASSERT_EQ(frame->getSrcAddr(), src);
        ASSERT_EQ(frame->getDstAddr(), 0);

        const auto testFrame = static_cast<AddressClaim::Frame *>(frame);
        const auto &name = testFrame->getEcuName();
        ASSERT_EQ(name.getIdNumber(), 25898);
        ASSERT_EQ(name.getManufacturerCode(), 1256);
        ASSERT_EQ(name.getEcuInstance(), 5);
        ASSERT_EQ(name.getFunctionInstance(), 16);
        ASSERT_EQ(name.getFunction(), 241);
        ASSERT_EQ(name.getVehicleSystem(), 90);
        ASSERT_EQ(name.getVehicleSystemInstance(), 15);
        ASSERT_EQ(name.getIndustryGroup(), 2);
        ASSERT_EQ(name.getEcuInstance(), 5);
        ASSERT_EQ(name.isArbitraryAddressCapable(), false);
    }


    struct StackFrameTest : testing::Test, testing::WithParamInterface<u32> {
    };

    TEST_P(StackFrameTest, existingFrames) {
        auto PGN = GetParam();
        auto pgns = Factory::getAllRegisteredPGNs();
        ASSERT_EQ(pgns.end(), pgns.find(PGN));
        Factory::registerStackFrames();
        pgns = Factory::getAllRegisteredPGNs();
        ASSERT_NE(pgns.end(), pgns.find(PGN));
        Factory::unregisterAllFrames();
    }

    INSTANTIATE_TEST_CASE_P(StackFramesRegistered, StackFrameTest,
        testing::Values(
            PGN::ADDRESS_CLAIM,
            PGN::REQUEST_PGN,
            PGN::TPCM,
            PGN::TPDT
        )
    );

} // namespace J1939FactoryTests
