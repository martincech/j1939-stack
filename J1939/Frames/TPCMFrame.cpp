
#include "TPCMFrame.h"
#include "PGN.h"
#include "TPDTFrame.h"
#include <cstring>
#if defined(DEBUG) && defined(J1939_STRINGS)
#include <ostream>
#include <sstream>
#include <string>
constexpr auto TPCM_NAME = "Transport Connection Management";
#endif

namespace J1939 {
    namespace Transport {
        namespace TPCM {
            Frame::Frame()
                : J1939::Frame(PGN::TPCM,
                               DEFAULT_PRIORITY
#if defined(DEBUG) && defined(J1939_STRINGS)
                               ,
                               TPCM_NAME
#endif
                              )
                , mCtrlType(ControlType::UNKNOWN)
                , mTotalMsgSize(0)
                , mMaxPackets(0xFF)
                , mPacketsToTx(0)
                , mNextPacket(0)
                , mAbortReason(AbortReason::NONE)
                , mDataPgn(0) {
            }

            Frame::~Frame() = default;

            bool Frame::decodeData(const u8 *buffer, const size_t &length) {
                if (length != getDataLength()) {
                    return false;
                }
                bool ret;
                mCtrlType = static_cast<ControlType>(buffer[0]);
                switch (mCtrlType) {
                    case ControlType::RTS:
                        ret = decodeRTS(buffer + 1);
                        break;
                    case ControlType::CTS:
                        ret = decodeCTS(buffer + 1);
                        break;
                    case ControlType::EOM_ACK:
                        ret = decodeEndOfMsgACK(buffer + 1);
                        break;
                    case ControlType::ABORT:
                        ret = decodeConnAbort(buffer + 1);
                        break;
                    case ControlType::BAM:
                        ret = decodeBAM(buffer + 1);
                        break;
                    default:
                        ret = false;
                        break;
                }
                mDataPgn = buffer[5] | (buffer[6] << 8) | (buffer[7] << 16);
                return ret;
            }

            bool Frame::encodeData(u8 *buffer) const {
                memset(buffer, 0xFF, PACKET_SIZE);
                buffer[0] = static_cast<u8>(mCtrlType);
                bool ret;
                switch (mCtrlType) {
                    case ControlType::RTS:
                        ret = encodeRTS(buffer + 1);
                        break;
                    case ControlType::CTS:
                        ret = encodeCTS(buffer + 1);
                        break;
                    case ControlType::EOM_ACK:
                        ret = encodeEndOfMsgACK(buffer + 1);
                        break;
                    case ControlType::ABORT:
                        ret = encodeConnAbort(buffer + 1);
                        break;
                    case ControlType::BAM:
                        ret = encodeBAM(buffer + 1);
                        break;
                    default:
                        return false;
                }

                buffer[5] = mDataPgn & 0xFF;
                buffer[6] = (mDataPgn >> 8) & 0xFF;
                buffer[7] = (mDataPgn >> 16) & 0xFF;
                return ret;
            }

            size_t Frame::getDataLength() const { return PACKET_SIZE; }

            bool Frame::isLengthValid() const {
                return getTotalMsgSize() > MAX_PACKET_SIZE && getTotalMsgSize() <= MAX_TRANSPORT_PACKETS_DATA_SIZE;
            }

            bool Frame::isValid() const {
                if(mCtrlType == ControlType::BAM)
                {
                    return getDstAddr() == BROADCAST_ADDRESS &&
                        isLengthValid() &&
                        getTotalPackets() > 1;
                }
                if(mCtrlType == ControlType::RTS)
                {
                    return isLengthValid() && getRTSMaxPackets() > 1; 
                }

                if(mCtrlType == ControlType::CTS)
                {
                    return getCTSNextPacket() > 1; 
                }

                return true;
            }


            ControlType Frame::getCtrlType() const {
                return mCtrlType;
            }

            AbortReason Frame::getAbortReason() const {
                return mAbortReason;
            }

            u32 Frame::getDataPgn() const {
                return mDataPgn;
            }

            u8 Frame::getRTSMaxPackets() const {
                return mMaxPackets;
            }

            u8 Frame::getCTSNextPacket() const {
                return mNextPacket;
            }

            u8 Frame::getCTSPacketsToTx() const {
                return mPacketsToTx;
            }

            u16 Frame::getTotalMsgSize() const {
                return mTotalMsgSize;
            }

            u8 Frame::getTotalPackets() const {
                return (mTotalMsgSize / TPDT::DATA_SIZE) + (mTotalMsgSize % TPDT::DATA_SIZE ? 1 : 0);
            }

            void Frame::setCtrlType(ControlType ctrlType) {
                mCtrlType = ctrlType;
            }

            void Frame::setDataPgn(u32 dataPgn) {
                mDataPgn = dataPgn;
            }

            bool Frame::setAbortReason(AbortReason abortReason) {
                if (mCtrlType != ControlType::ABORT)
                    return false;
                mAbortReason = abortReason;
                return true;
            }

            bool Frame::setRTSMaxPackets(u8 maxPackets) {
                if (mCtrlType != ControlType::RTS || maxPackets < 2)
                    return false;
                mMaxPackets = maxPackets;
                return true;
            }

            bool Frame::setCTSNextPacket(u8 nextPacket) {
                if (mCtrlType != ControlType::CTS || nextPacket < 1)
                    return false;
                mNextPacket = nextPacket;
                return true;
            }

            bool Frame::setCTSPacketsToTx(u8 packetsToTx) {
                if (mCtrlType != ControlType::CTS)
                    return false;
                mPacketsToTx = packetsToTx;
                return true;
            }

            bool Frame::setTotalMsgSize(u16 totalMsgSize) {
                if (mCtrlType != ControlType::RTS && mCtrlType != ControlType::BAM && mCtrlType != ControlType::EOM_ACK) {
                    return false;
                }
                if (totalMsgSize > MAX_TRANSPORT_PACKETS_DATA_SIZE) {
                    mTotalMsgSize = MAX_TRANSPORT_PACKETS_DATA_SIZE;
                } else {
                    mTotalMsgSize = totalMsgSize;
                }
                return true;
            }

            bool Frame::decodeRTS(const u8 *buffer) {

                mTotalMsgSize = buffer[0] | (buffer[1] << 8);
                mMaxPackets = buffer[3];
                return getTotalPackets() == buffer[2];
            }

            bool Frame::decodeCTS(const u8 *buffer) {

                mPacketsToTx = buffer[0];
                mNextPacket = buffer[1];
                return true;
            }

            bool Frame::decodeEndOfMsgACK(const u8 *buffer) {
                mTotalMsgSize = buffer[0] | (buffer[1] << 8);
                return getTotalPackets() == buffer[2];
            }

            bool Frame::decodeConnAbort(const u8 *buffer) {
                if (buffer[0] == static_cast<u8>(AbortReason::NONE) || buffer[0] >= static_cast<u8>(AbortReason::_LAST)) {
                    return false;
                }
                mAbortReason = static_cast<AbortReason>(buffer[0]);
                return true;
            }

            bool Frame::decodeBAM(const u8 *buffer) {
                mTotalMsgSize = buffer[0] | (buffer[1] << 8);
                return getTotalPackets() == buffer[2];
            }

            bool Frame::encodeRTS(u8 *buffer) const {

                if (mTotalMsgSize == 0 || mMaxPackets == 0)
                    return false;
                buffer[0] = mTotalMsgSize & 0xFF;
                buffer[1] = (mTotalMsgSize >> 8) & 0xFF;
                buffer[2] = getTotalPackets();
                buffer[3] = mMaxPackets;
                return true;
            }

            bool Frame::encodeCTS(u8 *buffer) const {

                buffer[0] = mPacketsToTx;
                buffer[1] = mNextPacket;
                return true;
            }

            bool Frame::encodeEndOfMsgACK(u8 *buffer) const {

                if (mTotalMsgSize == 0)
                    return false;
                buffer[0] = mTotalMsgSize & 0xFF;
                buffer[1] = (mTotalMsgSize >> 8) & 0xFF;
                buffer[2] = getTotalPackets();
                return true;
            }

            bool Frame::encodeConnAbort(u8 *buffer) const {
                if (mAbortReason == AbortReason::NONE || mAbortReason >= AbortReason::_LAST)
                    return false;
                buffer[0] = static_cast<u8>(mAbortReason);
                return true;
            }

            bool Frame::encodeBAM(u8 *buffer) const {
                if (mTotalMsgSize == 0)
                    return false;
                buffer[0] = mTotalMsgSize & 0xFF;
                buffer[1] = (mTotalMsgSize >> 8) & 0xFF;
                buffer[2] = getTotalPackets();
                return true;
            }

#if defined(DEBUG) && defined(J1939_STRINGS)
            std::string Frame::toString() {
                const auto retVal = Frame::toString();
                std::stringstream sstr;
                //TODO tostring of TPCM frame
                return retVal + sstr.str();
            }
#endif
        } // namespace TPCM
    }     // namespace Transport
}         /* namespace J1939 */
