
#include "TPDTFrame.h"
#include "PGN.h"
#include "Utils.h"
#include <cstring>
#if defined(DEBUG) && defined(J1939_STRINGS)
#include <ostream>
#include <sstream>
#include <string>
constexpr auto TPDT_NAME = "Transport Data";
#endif

namespace J1939 {
    namespace Transport {
        namespace TPDT {
            Frame::Frame()
                : J1939::Frame(PGN::TPDT,
                               DEFAULT_PRIORITY
#if defined(DEBUG) && defined(J1939_STRINGS)
                               ,
                               TPDT_NAME
#endif
                              )
                , mSQNum(0) {
                memset(mData, 0xFF, DATA_SIZE);
            }

            Frame::Frame(u8 sq, u8 *data, size_t length)
                : Frame() {
                mSQNum = sq;
                memcpy(mData, data, J1939_MIN(length, TPDT::DATA_SIZE));
            }

            Frame::~Frame() = default;

            bool Frame::isValid() const {
                return getSq() > 0;
            }

            size_t Frame::getDataLength() const { return PACKET_SIZE; }

            const u8 *Frame::getData() const { return mData; }

            void Frame::setData(u8 *data, const u8 length) {
                memcpy(mData, data, J1939_MIN(length, TPDT::DATA_SIZE));
                if (length < DATA_SIZE) {
                    memset(mData + length, 0xFF, DATA_SIZE - length);
                }
            }

            u8 Frame::getSq() const { return mSQNum; }

            void Frame::setSq(u8 sq)
            {
                mSQNum = sq;
            }

            bool Frame::decodeData(const u8 *buffer, const size_t &length) {

                if (length < getDataLength()) {
                    return false;
                }
                mSQNum = *buffer++;
                memcpy(mData, buffer, DATA_SIZE);
                return true;
            }

            bool Frame::encodeData(u8 *buffer) const {
                *buffer++ = mSQNum;
                memcpy(buffer, mData, DATA_SIZE);
                return true;
            }

#if defined(DEBUG) && defined(J1939_STRINGS)
            std::string Frame::toString() {
                const auto retVal = Frame::toString();
                std::stringstream sstr;
                sstr << "Sequence num: " << mSQNum << std::endl;
                return retVal + sstr.str();
            }
#endif
        } // namespace TPDT
    }     // namespace Transport
}         /* namespace J1939 */
