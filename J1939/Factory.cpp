#include "Factory.h"
#include "Frames/AddressClaimFrame.h"
#include "Frames/RequestFrame.h"
#include "Frames/TPCMFrame.h"
#include "Frames/TPDTFrame.h"
#include "Frames/DPFC1Frame.h"
#include "Frames/Proprietary/Deutz/DPFTestMonitor.h"
#include "Frames/Proprietary/Deutz/STOUT.h"

namespace J1939 {
    std::map<u32, Frame*> Factory::mFrames;

    Factory::Factory() {
    }

    Factory::~Factory() = default;

    Frame *Factory::getFrame(u32 id, const u8 *data, size_t length) {
        const auto pgn = Frame::getPGN(id);
        auto iter = mFrames.find(pgn);

        if (iter == mFrames.end()) {
            return nullptr;
        }
        iter->second->decode(id, data, length);
        return iter->second;
    }

    Frame *Factory::getFrame(u32 pgn) {
        if (mFrames.find(pgn) != mFrames.end()) {
            return mFrames[pgn];
            
        }
        return nullptr;
    }

    Frame* Factory::getFrame(PGN pgn) {
        return getFrame(static_cast<u32>(pgn));
    }

    bool Factory::registerFrame(Frame *frame) {

        if (mFrames.find(frame->getPGN()) == mFrames.end()) {
            mFrames[frame->getPGN()] = frame;
            return true;
        }
        return false;
    }

    void Factory::unregisterAllFrames() {
        mFrames.clear();
    }

    void Factory::unRegisterFrame(u32 pgn) {
        const auto iter = mFrames.find(pgn);
        if (iter != mFrames.end()) {
            mFrames.erase(iter);
        }
    }

    void Factory::registerStackFrames() {
        /*{
            static AddressClaim::Frame frame;
            registerFrame(&frame);
        }

        {
            static Request::Frame frame;
            registerFrame(&frame);
        }
        {
            static Transport::TPCM::Frame frame;
            registerFrame(&frame);
        }

        {
            static Transport::TPDT::Frame frame;
            registerFrame(&frame);
        }*/

        {
            static DPFC1::Frame frame;
            registerFrame(&frame);
        }

        {
            static DEUTZ::DPFTestMonitor::Frame frame;
            registerFrame(&frame);
        }

        {
            static DEUTZ::STOUT::Frame frame;
            registerFrame(&frame);
        }
    }

    std::set<u32> Factory::getAllRegisteredPGNs() {

        std::set<u32> pgns;

        for (auto iter = mFrames.begin(); iter != mFrames.end(); ++iter) {
            pgns.insert(iter->first);
        }

        return pgns;
    }

#if defined(DEBUG) && defined(J1939_STRINGS)
    Frame *Factory::getFrame(const std::string &name) {
        for (auto iter = mFrames.begin(); iter != mFrames.end(); ++iter) {
            if (iter->second != nullptr && iter->second->getName() == name) {
                return iter->second;
            }
        }

        return nullptr;
    }
#endif

} /* namespace J1939 */
