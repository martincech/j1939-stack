#pragma once

#include "J1939Common.h"
#include <gmock/gmock.h>

namespace J1939 {
    namespace Tests {
        class MockFrameHandler : public FrameHandler {
        public:
            MOCK_METHOD1(send, void(const J1939::Frame *));
            MOCK_METHOD1(receive, void(const J1939::Frame *));
            MOCK_METHOD1(registerReceiveHandler, void(FrameHandlerFnPtr&));
        };
    } // namespace Tests
}     // namespace J1939
