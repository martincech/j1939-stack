#ifndef __SPN_10xxx_H__
#define __SPN_10xxx_H__

#include "SPN_3xxx.h"

namespace J1939 {
    namespace SPN {
        using SPN_10153_STATES = SPN_3702_STATES;           
        using SPN_10153 = SPN_STATUS<SPN_10153_STATES, 8, 7, 2>;
    }
} /* namespace J1939 */

#endif /* __SPN_H__ */
