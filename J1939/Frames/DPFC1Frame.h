#ifndef __DPFC1_FRAME_H__
#define __DPFC1_FRAME_H__

#include "J1939Frame.h"
#include "SPN/SPN_3xxx.h"
#include "SPN/SPN_4xxx.h"
#include "SPN/SPN_5xxx.h"
#include "SPN/SPN_8xxx.h"
#include "SPN/SPN_10xxx.h"

namespace J1939 {

    namespace DPFC1 {
        constexpr auto PACKET_SIZE = 8;
        constexpr auto DEFAULT_PRIORITY = 6;
        using namespace SPN;

        using LampCommand = SPN_3697_STATES;
        using PassiveRegenerationStatus = SPN_3699_STATES;
        using ActiveRegenerationAvailability = SPN_8857_STATES;
        using ActiveRegenerationStatus = SPN_3700_STATES;
        using ActiveRegenerationNeedLevel = SPN_3701_STATES;
        using ActiveRegenerationInitiationConfiguration = SPN_3718_STATES;
        using ActiveRegenerationConditionsNotMet = SPN_3750_STATES;
        using ExhaustHighTemperatureLampCommand = SPN_3698_STATES;
        using ActiveRegenerationForcedStatus = SPN_4175_STATES;
        using HydrocarbonDoserPurgingEnabled = SPN_5504_STATES;

        enum class ActiveRegenerationInhibitedReason {
            NOT_INHIBITED,
            INHIBITED_DUE_UNKNOWN_REASON,
            INHIBITED_DUE_INHIBIT_SWITCH,
            INHIBITED_DUE_CLUTCH_DISENGAGED,
            INHIBITED_DUE_SERVICE_BRAKE_ACTIVE,
            INHIBITED_DUE_PTO_ACTIVE,
            INHIBITED_DUE_ACCELERATOR_PEDAL_OFF_IDLE,
            INHIBITED_DUE_OUT_OF_NEUTRAL,
            INHIBITED_DUE_VEHICLE_SPEED_ABOVE_ALLOWED_SPEED,
            INHIBITED_DUE_PARKING_BRAKE_NOT_SET,
            INHIBITED_DUE_LOW_EXHAUST_TEMPERATURE,
            INHIBITED_DUE_SYSTEM_FAULT_ACTIVE,
            INHIBITED_DUE_SYSTEM_TIMEOUT,
            INHIBITED_DUE_TEMPORARY_SYSTEM_LOCKOUT,
            INHIBITED_DUE_PERMANENT_SYSTEM_LOCKOUT,
            INHIBITED_DUE_ENGINE_NOT_WARMED_UP,
            INHIBITED_DUE_VEHICLE_SPEED_BELOW_ALLOWED_SPEED,
            INHIBITED_DUE_LOW_EXHAUST_GAS_PRESSURE,
            INHIBITED_DUE_TO_TRESHER
        };


        /**
         * Parameter Group Name             Diesel Particulate Filter Control 1 
         * Parameter Group Number           64892 (00FD7Chex)
         * Definition                       This PGN contains information about the diesel particulate filter regeneration control.
         *                                  If there are aftertreatment systems on two banks, 
         *                                  this PGN represents the composite information from both banks.
         *                                  NOTE: This message will be transmitted by the engine or 
         *                                  aftertreatment system controller.
         * Transmission                     1000 ms
         * Data Length                      8 bytes (CAN DLC = 8)
         * Extended Data Page (R)           0
         * Data Page                        0
         * PDU Format                       253
         * PDU Specific                     124
         * Default Priority                 6
         * Data Description                 BYTE.BIT    SPN     LABEL
         *                                  1.1         3697    Diesel Particulate Filter Lamp Command
         *                                  1.4         8857    Diesel Particulate Filter Active Regeneration Availability Status
         *                                  2.1         3699    Aftertreatment Diesel Particulate Filter Passive Regeneration Status
         *                                  2.3         3700    Aftertreatment Diesel Particulate Filter Active Regeneration Status
         *                                  2.5         3701    Aftertreatment Diesel Particulate Filter Status
         *                                  3.1         3702    Diesel Particulate Filter Active Regeneration Inhibited Status
         *                                  3.3         3703    Diesel Particulate Filter Active Regeneration Inhibited Due to Inhibit Switch
         *                                  3.5         3704    Diesel Particulate Filter Active Regeneration Inhibited Due to Clutch Disengaged
         *                                  3.7         3705    Diesel Particulate Filter Active Regeneration Inhibited Due to Service Brake Active
         *                                  4.1         3706    Diesel Particulate Filter Active Regeneration Inhibited Due to PTO Active
         *                                  4.3         3707    Diesel Particulate Filter Active Regeneration Inhibited Due to Accelerator Pedal Off Idle
         *                                  4.5         3708    Diesel Particulate Filter Active Regeneration Inhibited Due to Out of Neutral
         *                                  4.7         3709    Diesel Particulate Filter Active Regeneration Inhibited Due to Vehicle Speed Above Allowed Speed
         *                                  5.1         3710    Diesel Particulate Filter Active Regeneration Inhibited Due to Parking Brake Not Set
         *                                  5.3         3711    Diesel Particulate Filter Active Regeneration Inhibited Due to Low Exhaust Temperature
         *                                  5.5         3712    Diesel Particulate Filter Active Regeneration Inhibited Due to System Fault Active
         *                                  5.7         3713    Diesel Particulate Filter Active Regeneration Inhibited Due to System Timeout
         *                                  6.1         3714    Diesel Particulate Filter Active Regeneration Inhibited Due to Temporary System Lockout
         *                                  6.3         3715    Diesel Particulate Filter Active Regeneration Inhibited Due to Permanent System Lockout
         *                                  6.5         3716    Diesel Particulate Filter Active Regeneration Inhibited Due to Engine Not Warmed Up
         *                                  6.7         3717    Diesel Particulate Filter Active Regeneration Inhibited Due to Vehicle Speed Below Allowed Speed
         *                                  7.1         3718    Diesel Particulate Filter Automatic Active Regeneration Initiation Configuration
         *                                  7.3         3698    Exhaust System High Temperature Lamp Command
         *                                  7.6         4175    Diesel Particulate Filter Active Regeneration Forced Status
         *                                  8.1         5504    Hydrocarbon Doser Purging Enable
         *                                  8.3         5629    Diesel Particulate Filter Active Regeneration Inhibited Due to Low Exhaust Pressure
         *                                  8.5         3750    Aftertreatment 1 Diesel Particulate Filter Conditions Not Met for Active Regeneration
         *                                  8.7         10153   Diesel Particulate Filter Active Regeneration Inhibited Due to Thresher

         */
        class Frame : public J1939::Frame {
            LampCommand lamp;
            PassiveRegenerationStatus pasiveStatus;
            ActiveRegenerationStatus activeStatus;
            ActiveRegenerationNeedLevel activeLevel;
            ActiveRegenerationInhibitedReason activeInhibitReason;
            ActiveRegenerationAvailability activeAvailable;
            ActiveRegenerationForcedStatus activeRegenerationForce;
            ActiveRegenerationConditionsNotMet activeConditionsNotMet;
            ActiveRegenerationInitiationConfiguration activeInitationConfiguration;
            HydrocarbonDoserPurgingEnabled hydrocarboPurgingEnable;
            ExhaustHighTemperatureLampCommand exhaustHighTempLampCommand;
        protected:
            bool decodeData(const u8 *buffer, const size_t &length) override;
            bool encodeData(u8 *buffer) const override;

        public:
            Frame();
            virtual ~Frame();

            size_t getDataLength() const override;

            LampCommand getLamp() const;
            ActiveRegenerationAvailability getActiveAvailable() const;
            PassiveRegenerationStatus getPasiveStatus() const;
            ActiveRegenerationStatus getActiveStatus() const;
            ActiveRegenerationNeedLevel getActiveLevel() const;
            bool activeRegenerationIsInhibited() const;
            ActiveRegenerationInhibitedReason getActiveInhibitReason() const;
            ActiveRegenerationForcedStatus getActiveRegenerationForce() const;
            ActiveRegenerationConditionsNotMet getActiveConditionsNotMet() const;
            ActiveRegenerationInitiationConfiguration getActiveInitationConfiguration() const;
            HydrocarbonDoserPurgingEnabled getHydrocarboPurgingEnable() const;
            ExhaustHighTemperatureLampCommand getExhaustHighTempLampCommand() const;
#if defined(DEBUG) && defined(J1939_STRINGS)
            std::string toString() override;
#endif
        };
    } // namespace DPF
}     /* namespace J1939 */

#endif /* __DPFC1_FRAME_H__ */
