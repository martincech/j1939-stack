#include "DPFC1Frame.h"
#include "J1939Common.h"
#include "PGN.h"

#if defined(DEBUG) && defined(J1939_STRINGS)
#include <ostream>
#include <sstream>
#include <string>
constexpr auto DPFC1_FRAME_NAME = "DPFC1";
#endif

namespace J1939 {
    namespace DPFC1 {

        Frame::Frame()
            : J1939::Frame(PGN::DPFC1,
                           DEFAULT_PRIORITY
#if defined(DEBUG) && defined(J1939_STRINGS)
                           ,
                           DPFC1_FRAME_NAME
#endif
                          )
            , lamp(LampCommand::OFF)
            , activeStatus(ActiveRegenerationStatus::NOT_ACTIVE)
            , activeLevel(ActiveRegenerationNeedLevel::NOT_NEEDED)
            , activeInhibitReason(ActiveRegenerationInhibitedReason::NOT_INHIBITED) {
        }

        Frame::~Frame() = default;

        size_t Frame::getDataLength() const { return PACKET_SIZE; }

        LampCommand Frame::getLamp() const {
            return lamp;
        }

        PassiveRegenerationStatus Frame::getPasiveStatus() const {
            return pasiveStatus;
        }

        ActiveRegenerationStatus Frame::getActiveStatus() const {
            return activeStatus;
        }

        ActiveRegenerationNeedLevel Frame::getActiveLevel() const {
            return activeLevel;
        }

        bool Frame::activeRegenerationIsInhibited() const {
            return activeInhibitReason != ActiveRegenerationInhibitedReason::NOT_INHIBITED;
        }

        ActiveRegenerationInhibitedReason Frame::getActiveInhibitReason() const {
            return activeInhibitReason;
        }

        ActiveRegenerationAvailability Frame::getActiveAvailable() const {
            return activeAvailable;
        }

        ActiveRegenerationForcedStatus Frame::getActiveRegenerationForce() const {
            return activeRegenerationForce;
        }

        ActiveRegenerationConditionsNotMet Frame::getActiveConditionsNotMet() const {
            return activeConditionsNotMet;
        }

        ActiveRegenerationInitiationConfiguration Frame::getActiveInitationConfiguration() const {
            return activeInitationConfiguration;
        }

        HydrocarbonDoserPurgingEnabled Frame::getHydrocarboPurgingEnable() const {
            return hydrocarboPurgingEnable;
        }

        ExhaustHighTemperatureLampCommand Frame::getExhaustHighTempLampCommand() const {
            return exhaustHighTempLampCommand;
        }


        template <typename SPN, typename SPN_STATES, ActiveRegenerationInhibitedReason R>
        static void decodeIfInhibited(ActiveRegenerationInhibitedReason &reason, const u8 *buffer) {
            if (SPN::decode(buffer) == SPN_STATES::INHIBITED) {
                reason = R;
            }
        }

        template <typename SPN, typename SPN_STATES, ActiveRegenerationInhibitedReason R>
        static void encodeIfInhibited(const ActiveRegenerationInhibitedReason &reason, u8 *buffer) {
            if(reason == R)
            {
                SPN::encode(buffer, SPN_STATES::INHIBITED);
            }else
            {
                SPN::encode(buffer, SPN_STATES::NOT_INHIBITED);
            }
        }

#define DecodeInhibitReason(N, I) decodeIfInhibited<SPN_##N, SPN_##N##_STATES, I>(activeInhibitReason, buffer)

#define EncodeInhibitReason(N, I) encodeIfInhibited<SPN_##N, SPN_##N##_STATES, I>(activeInhibitReason, buffer)

        bool Frame::decodeData(const u8 *buffer, const size_t &length) {
            if (length < getDataLength()) {
                return false;
            }
            lamp = SPN_3697::decode(buffer);
            activeAvailable = SPN_8857::decode(buffer);
            pasiveStatus = SPN_3699::decode(buffer);
            activeStatus = SPN_3700::decode(buffer);
            activeLevel = SPN_3701::decode(buffer);
            exhaustHighTempLampCommand = SPN_3698::decode(buffer);
            activeInhibitReason = ActiveRegenerationInhibitedReason::NOT_INHIBITED;
            if (SPN_3702::decode(buffer) == SPN_3702_STATES::INHIBITED) {
                activeInhibitReason = ActiveRegenerationInhibitedReason::INHIBITED_DUE_UNKNOWN_REASON;
                // inhibitReason=   SPN    ENUM
                DecodeInhibitReason(3703, ActiveRegenerationInhibitedReason::INHIBITED_DUE_INHIBIT_SWITCH);
                DecodeInhibitReason(3704, ActiveRegenerationInhibitedReason::INHIBITED_DUE_CLUTCH_DISENGAGED);
                DecodeInhibitReason(3705, ActiveRegenerationInhibitedReason::INHIBITED_DUE_SERVICE_BRAKE_ACTIVE);
                DecodeInhibitReason(3706, ActiveRegenerationInhibitedReason::INHIBITED_DUE_PTO_ACTIVE);
                DecodeInhibitReason(3707, ActiveRegenerationInhibitedReason::INHIBITED_DUE_ACCELERATOR_PEDAL_OFF_IDLE);
                DecodeInhibitReason(3708, ActiveRegenerationInhibitedReason::INHIBITED_DUE_OUT_OF_NEUTRAL);
                DecodeInhibitReason(3709, ActiveRegenerationInhibitedReason::INHIBITED_DUE_VEHICLE_SPEED_ABOVE_ALLOWED_SPEED);
                DecodeInhibitReason(3710, ActiveRegenerationInhibitedReason::INHIBITED_DUE_PARKING_BRAKE_NOT_SET);
                DecodeInhibitReason(3711, ActiveRegenerationInhibitedReason::INHIBITED_DUE_LOW_EXHAUST_TEMPERATURE);
                DecodeInhibitReason(3712, ActiveRegenerationInhibitedReason::INHIBITED_DUE_SYSTEM_FAULT_ACTIVE);
                DecodeInhibitReason(3713, ActiveRegenerationInhibitedReason::INHIBITED_DUE_SYSTEM_TIMEOUT);
                DecodeInhibitReason(3714, ActiveRegenerationInhibitedReason::INHIBITED_DUE_TEMPORARY_SYSTEM_LOCKOUT);
                DecodeInhibitReason(3715, ActiveRegenerationInhibitedReason::INHIBITED_DUE_PERMANENT_SYSTEM_LOCKOUT);
                DecodeInhibitReason(3716, ActiveRegenerationInhibitedReason::INHIBITED_DUE_ENGINE_NOT_WARMED_UP);
                DecodeInhibitReason(3717, ActiveRegenerationInhibitedReason::INHIBITED_DUE_VEHICLE_SPEED_BELOW_ALLOWED_SPEED);
                DecodeInhibitReason(5629, ActiveRegenerationInhibitedReason::INHIBITED_DUE_LOW_EXHAUST_GAS_PRESSURE);
                DecodeInhibitReason(10153, ActiveRegenerationInhibitedReason::INHIBITED_DUE_TO_TRESHER);
            }

            activeInitationConfiguration = SPN_3718::decode(buffer);
            activeRegenerationForce = SPN_4175::decode(buffer);
            hydrocarboPurgingEnable = SPN_5504::decode(buffer);
            activeConditionsNotMet = SPN_3750::decode(buffer);

            return true;
        }

        bool Frame::encodeData(u8 *buffer) const {
            SPN_3697::encode(buffer, lamp);
            SPN_8857::encode(buffer, activeAvailable);
            SPN_3699::encode(buffer, pasiveStatus);
            SPN_3700::encode(buffer, activeStatus);
            SPN_3701::encode(buffer, activeLevel);
            if(activeInhibitReason == ActiveRegenerationInhibitedReason::NOT_INHIBITED)
            {
                SPN_3702::encode(buffer, SPN_3702_STATES::NOT_INHIBITED);
            }else
            {
                SPN_3702::encode(buffer, SPN_3702_STATES::INHIBITED);
            }            
            EncodeInhibitReason(3703, ActiveRegenerationInhibitedReason::INHIBITED_DUE_INHIBIT_SWITCH);
            EncodeInhibitReason(3704, ActiveRegenerationInhibitedReason::INHIBITED_DUE_CLUTCH_DISENGAGED);
            EncodeInhibitReason(3705, ActiveRegenerationInhibitedReason::INHIBITED_DUE_SERVICE_BRAKE_ACTIVE);
            EncodeInhibitReason(3706, ActiveRegenerationInhibitedReason::INHIBITED_DUE_PTO_ACTIVE);
            EncodeInhibitReason(3707, ActiveRegenerationInhibitedReason::INHIBITED_DUE_ACCELERATOR_PEDAL_OFF_IDLE);
            EncodeInhibitReason(3708, ActiveRegenerationInhibitedReason::INHIBITED_DUE_OUT_OF_NEUTRAL);
            EncodeInhibitReason(3709, ActiveRegenerationInhibitedReason::INHIBITED_DUE_VEHICLE_SPEED_ABOVE_ALLOWED_SPEED);
            EncodeInhibitReason(3710, ActiveRegenerationInhibitedReason::INHIBITED_DUE_PARKING_BRAKE_NOT_SET);
            EncodeInhibitReason(3711, ActiveRegenerationInhibitedReason::INHIBITED_DUE_LOW_EXHAUST_TEMPERATURE);
            EncodeInhibitReason(3712, ActiveRegenerationInhibitedReason::INHIBITED_DUE_SYSTEM_FAULT_ACTIVE);
            EncodeInhibitReason(3713, ActiveRegenerationInhibitedReason::INHIBITED_DUE_SYSTEM_TIMEOUT);
            EncodeInhibitReason(3714, ActiveRegenerationInhibitedReason::INHIBITED_DUE_TEMPORARY_SYSTEM_LOCKOUT);
            EncodeInhibitReason(3715, ActiveRegenerationInhibitedReason::INHIBITED_DUE_PERMANENT_SYSTEM_LOCKOUT);
            EncodeInhibitReason(3716, ActiveRegenerationInhibitedReason::INHIBITED_DUE_ENGINE_NOT_WARMED_UP);
            EncodeInhibitReason(3717, ActiveRegenerationInhibitedReason::INHIBITED_DUE_VEHICLE_SPEED_BELOW_ALLOWED_SPEED);
            EncodeInhibitReason(5629, ActiveRegenerationInhibitedReason::INHIBITED_DUE_LOW_EXHAUST_GAS_PRESSURE);
            EncodeInhibitReason(10153, ActiveRegenerationInhibitedReason::INHIBITED_DUE_TO_TRESHER);
            SPN_3718::encode(buffer, activeInitationConfiguration);
            SPN_3698::encode(buffer, exhaustHighTempLampCommand);
            SPN_4175::encode(buffer, activeRegenerationForce);
            SPN_5504::encode(buffer, hydrocarboPurgingEnable);
            SPN_3750::encode(buffer, activeConditionsNotMet);
            return true;
        }

#if defined(DEBUG) && defined(J1939_STRINGS)
        std::string Frame::toString() {
            return "";
        }
#endif
    }
} /* namespace J1939 */
