#include "STOUT.h"
#include <cstring>

namespace J1939 {

    namespace DEUTZ {
        namespace STOUT {
            bool Frame::decodeData(const u8 *buffer, const size_t &length) {
                if (length < getDataLength()) {
                    return false;
                }
                scrWarning = SCR_WARNING::decode(buffer);
                torqueOutput = buffer[4];
                fanOutput = buffer[5];
                engineRunningLamp = ENGINE_RUNNING_LAMP::decode(buffer);
                starterOutput = STARTER_OUTPUT::decode(buffer);
                coolantTemperatureWarning = COOLANT_TEMPERATURE_WARNING::decode(buffer);
                oilPressureWarning = OIL_PRESSURE_WARNING::decode(buffer);
                preheatLamp = PREHEAT_LAMP::decode(buffer);
                return true;
            }

            bool Frame::encodeData(u8 *buffer) const {
                memset(buffer, 0xFF, getDataLength());
                buffer[0] = 0;
                SCR_WARNING::encode(buffer, scrWarning);
                buffer[4] = torqueOutput;
                buffer[5] = fanOutput;
                ENGINE_RUNNING_LAMP::encode(buffer, engineRunningLamp);
                STARTER_OUTPUT::encode(buffer, starterOutput);
                COOLANT_TEMPERATURE_WARNING::encode(buffer, coolantTemperatureWarning);
                OIL_PRESSURE_WARNING::encode(buffer, oilPressureWarning);
                PREHEAT_LAMP::encode(buffer, preheatLamp);
                return true;
            }

            Frame::Frame()
                : J1939::Frame(PGN::STOUT, DEFAULT_PRIORITY)
                , scrWarning(ScrWarning::OFF)
                , torqueOutput(0)
                , fanOutput(0)
                , engineRunningLamp(EngineRunningLamp::OFF)
                , starterOutput(StarterOutput::OFF)
                , coolantTemperatureWarning(CoolantTemperatureWarning::OFF)
                , oilPressureWarning(OilPressureWarning::OFF)
                , preheatLamp(PreheatLamp::OFF) {
            }

            Frame::~Frame() = default;

            size_t Frame::getDataLength() const {
                return PACKET_SIZE;
            }

            ScrWarning Frame::getScrWarning() const {
                return scrWarning;
            }

            uint8_t Frame::getTorqueOutput() const {
                return torqueOutput;
            }

            uint8_t Frame::getFanOutput() const {
                return fanOutput;
            }

            EngineRunningLamp Frame::getEngineRunningLamp() const {
                return engineRunningLamp;
            }

            StarterOutput Frame::getStarterOutput() const {
                return starterOutput;
            }

            CoolantTemperatureWarning Frame::getCoolantTemperatureWarning() const {
                return coolantTemperatureWarning;
            }

            OilPressureWarning Frame::getOilPressureWarning() const {
                return oilPressureWarning;
            }

            PreheatLamp Frame::getPreheatLamp() const {
                return preheatLamp;
            }
        } // namespace STOUT
    }     // namespace DEUTZ
}         // namespace J1939
