#ifndef __SPN_4xxx_H__
#define __SPN_4xxx_H__
#include "SPN.h"

namespace J1939 {
    namespace SPN {
        enum class SPN_4175_STATES {
            /*000b = Not Active*/
            NOT_ACTIVE,
            /*001b = Active - Forced by Switch (See SPN 3696)*/
            ACTIVE_BY_SWITCH,
            /*010b = Active - Forced by Service Tool */
            ACTIVE_BY_SERVICE_TOOL,
            /*011b = Reserved for SAE Assignment*/
            RESERVED1,
            /*100b = Reserved for SAE Assignment*/
            RESERVED2,
            /*101b = Reserved for SAE Assignment*/
            RESERVED3,
            /*110b = Reserved for SAE Assignment*/
            RESERVED4,
            /*111b = not available*/
            NOT_AVAILABLE
        };

        using SPN_4175 = SPN_STATUS<SPN_4175_STATES, 7, 6, 3>;
    }

} /* namespace J1939 */

#endif /* __SPN_H__ */
