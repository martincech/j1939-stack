#ifndef TESTFRAME_H_
#define TESTFRAME_H_

#include "Frames/J1939Frame.h"
#include <string>

namespace J1939 {

    class TestFrame : public Frame {
        std::basic_string<u8> mRaw;

    protected:
        bool decodeData(const u8 *buffer, const size_t &length) override;

        bool encodeData(u8 *buffer) const override;

    public:
        TestFrame(u32 pgn);
        TestFrame(u32 pgn, u8 prio);
        TestFrame(u32 pgn, const char *name);
        TestFrame(u32 pgn, u8 prio, const char *name);
        virtual ~TestFrame();

        std::basic_string<u8> getRaw() const;
        void setRaw(const std::basic_string<u8> &raw);
        size_t getDataLength() const override;
        
    };

} /* namespace J1939 */

#endif /* TESTFRAME_H_ */
