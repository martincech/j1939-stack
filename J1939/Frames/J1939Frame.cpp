#include "J1939Frame.h"
#include "J1939Common.h"
#include <cstring>
#if defined(DEBUG) && defined(J1939_STRINGS)
#include <ostream>
#include <sstream>
constexpr auto UNKNOWN_FRAME = "Unknown";
#endif

namespace J1939 {

    Frame::Frame(PGN pgn)
        : Frame(static_cast<u32>(pgn)) {
    }

    Frame::Frame(PGN pgn, u8 priority)
        : Frame(static_cast<u32>(pgn), priority) {
    }

    Frame::Frame(u32 pgn)
        : Frame(pgn, DEFAULT_PRIORITY) {
    }

    Frame::Frame(u32 pgn, u8 priority)
        : mPgn(getPGN(pgn << ID::PGN_OFFSET))
        , mSrcAddr(INVALID_ADDRESS) {
        setDstAddr(getDstAddr(pgn << ID::PGN_OFFSET));
        setPriority(priority);
    }
#if defined(DEBUG) && defined(J1939_STRINGS)
    Frame::Frame(PGN pgn, const char *name)
        :Frame(static_cast<u32>(pgn), name)
    {
    }

    Frame::Frame(PGN pgn, u8 priority, const char *name)
        :Frame(static_cast<u32>(pgn), priority, name)
    {
    }

    Frame::Frame(u32 pgn, const char *name)
        : Frame(pgn, DEFAULT_PRIORITY, name) {
    }

    Frame::Frame(u32 pgn, u8 priority, const char *name)
        : Frame(pgn, priority) {
        mName = std::string(name);
    }
#endif

    Frame::~Frame() = default;

    u32 Frame::getPGN(u32 id) {
        auto pgn = (id >> ID::PGN_OFFSET) & ID::PGN_MASK_PDUF2;
        if (getPDUFormatGroup(id) == PduFormat::PDU1) {
            pgn &= ID::PGN_MASK_PDUF1;
        }
        return pgn;
    }

    u8 Frame::getSrcAddr(u32 id) {
        return (id >> ID::SRC_ADDR_OFFSET) & ID::SRC_ADDR_MASK;
    }

    PduFormat Frame::getPDUFormatGroup(u32 id) {
        return getPDUFormat(id) < PDU_FORMAT_DELIMITER ? PduFormat::PDU1 : PduFormat::PDU2;
    }

    u8 Frame::getPDUFormat(u32 id) {
        return (id >> ID::PDU_FORMAT_OFFSET) & ID::PDU_FORMAT_MASK;
    }

    u8 Frame::getPDUSpecific(u32 id) {
        return (id >> ID::PDU_SPECIFIC_OFFSET) & ID::PDU_SPECIFIC_MASK;
    }

    u8 Frame::getDstAddr(u32 id) {
        return (id >> ID::DST_ADDR_OFFSET) & ID::DST_ADDR_MASK;
    }

    u8 Frame::getGroupExtension(u32 id) {
        return (id >> ID::GROUP_EXTENSION_OFFSET) & ID::GROUP_EXTENSION_MASK;
    }

    u8 Frame::getPriority(u32 id) {
        return (id >> ID::PRIORITY_OFFSET) & ID::PRIORITY_MASK;
    }

    u8 Frame::getExtDataPage(u32 id) {
        return (id >> ID::EXT_DATA_PAGE_OFFSET) & ID::EXT_DATA_PAGE_MASK;
    }

    u8 Frame::getDataPage(u32 id) {
        return (id >> ID::DATA_PAGE_OFFSET) & ID::DATA_PAGE_MASK;
    }

    u32 Frame::createId(u32 pgn, u32 src, u8 priority, u32 dst) {

        u32 id = ((pgn & ID::PGN_MASK) << ID::PGN_OFFSET) |
            ((src & ID::SRC_ADDR_MASK) << ID::SRC_ADDR_OFFSET) |
            ((priority & ID::PRIORITY_MASK) << ID::PRIORITY_OFFSET);
        if (getPDUFormatGroup((pgn & ID::PGN_MASK_PDUF1) >> ID::PGN_OFFSET) == PduFormat::PDU1) {
            id |= ((dst & ID::DST_ADDR_MASK) << ID::DST_ADDR_OFFSET);
        }
        return id;
            
    }

    u8 Frame::getExtDataPage() const {
        return getExtDataPage(mPgn << ID::PGN_OFFSET);
    }

    u8 Frame::getDataPage() const {
        return getDataPage(mPgn << ID::PGN_OFFSET);
    }

    u8 Frame::getPDUFormat() const {
        return getPDUFormat(mPgn << ID::PGN_OFFSET);
    }

    u8 Frame::getPDUSpecific() const {
        return getPDUSpecific(mPgn << ID::PGN_OFFSET);
    }

    PduFormat Frame::getPDUFormatGroup() const {
        return getPDUFormatGroup(mPgn << ID::PGN_OFFSET);
    }

    u8 Frame::getPriority() const { return mPriority; }

    bool Frame::setPriority(u8 priority) {
        if (priority > ID::PRIORITY_MASK) {
            mPriority = ID::PRIORITY_MASK;
        } else {
            mPriority = priority;
        }
        return mPriority == priority;
    }

    u8 Frame::getSrcAddr() const { return mSrcAddr; }

    void Frame::setSrcAddr(const u8 src) { mSrcAddr = src; }


    u8 Frame::getDstAddr() const { return mDstAddr; }

    bool Frame::setDstAddr(const u8 dst) {
        if (getPDUFormatGroup() == PduFormat::PDU1) {
            mDstAddr = dst;
        } else {
            mDstAddr = BROADCAST_ADDRESS;
        }
        return dst == mDstAddr;
    }

    u32 Frame::getPGN() const { return mPgn; }

    

    bool Frame::decode(u32 identifier, const u8 *buffer, const size_t length) {

        if (mPgn != getPGN(identifier)) {
            return false;
        }
        setDstAddr(getDstAddr(identifier));
        setSrcAddr(getSrcAddr(identifier));
        setPriority(getPriority(identifier));

        return decodeData(buffer, length);
    }

    bool Frame::encode(u32 &identifier, u8 *buffer, size_t &length) const {
        identifier = getIdentifier();
        if (length < getDataLength()) {
            return false;
        }
        memset(buffer, 0xFF, getDataLength());
        const auto ret = encodeData(buffer);
        length = getDataLength();
        return ret;
    }

    u32 Frame::getIdentifier() const {
        return ((mPriority & ID::PRIORITY_MASK) << ID::PRIORITY_OFFSET) |
               ((mPgn & ID::PGN_MASK) << ID::PGN_OFFSET) |
               ((mSrcAddr & ID::SRC_ADDR_MASK) << ID::SRC_ADDR_OFFSET) |
               (getPDUFormatGroup() == PduFormat::PDU1 ? ((mDstAddr & ID::DST_ADDR_MASK) << ID::DST_ADDR_OFFSET) : 0);
    }

    bool Frame::isMultiPacketFrame() const {
        return getDataLength() > MAX_PACKET_SIZE;
    }

#if defined(DEBUG) && defined(J1939_STRINGS)

    const std::string &Frame::getName() const { return mName; }

    std::string Frame::toString() {

        std::stringstream sstr;

        sstr << "Name" << "\t" << "PGN" << "\t" << "Source Address" << "\t" << "PDU format" << "\t";

        if (getPDUFormatGroup() == PduFormat::PDU1) {
            sstr << "Dest Address" << "\t";
        }

        sstr << "Priority" << "\t";

        sstr << std::endl;

        sstr << mName << "\t" << std::uppercase << std::hex << mPgn << "\t" << static_cast<u32>(mSrcAddr) << "\t" << "\t" << (getPDUFormatGroup() == PduFormat::PDU1 ? "1" : "2") << "\t" << "\t";

        if (getPDUFormatGroup() == PduFormat::PDU1) {
            sstr << static_cast<u32>(mDstAddr) << "\t";
        }

        sstr << static_cast<u32>(mPriority) << "\t";

        sstr << std::endl;

        return sstr.str();
    }
#endif
} /* namespace J1939 */
