#ifndef __SPN_3xxx_H__
#define __SPN_3xxx_H__

#include "SPN.h"

namespace J1939 {
    namespace SPN {
        enum class SPN_3697_STATES {
            /*000b = Off*/
            OFF,
            /*001b = On - solid*/
            ON,
            /*010b = reserved for SAE assignment */
            RESERVED1,
            /*011b = reserved for SAE assignment*/
            RESERVED2,
            /*100b = On - fast blink (3 HZ)*/
            ON_FAST_BLINK,
            /*101b = On - slow blink (0.5 HZ)*/
            ON_SLOW_BLINK,
            /*110b = reserved for SAE assignment*/
            RESERVED3,
            /*111b = not available*/
            NOT_AVAILABLE
        };

        using SPN_3697 = SPN_STATUS<SPN_3697_STATES, 1, 1, 3>;

        enum class SPN_3698_STATES {
            /*000b = Off*/
            OFF,
            /*001b = On - solid*/
            ON,
            /*010b = reserved for SAE assignment*/
            RESERVED1,
            /*011b = reserved for SAE assignment*/
            RESERVED2,
            /*100b = reserved for SAE assignment*/
            RESERVED3,
            /*101b = reserved for SAE assignment*/
            RESERVED4,
            /*110b = reserved for SAE assignment*/
            RESERVED5,
            /*111b = not available*/
            NOT_AVAILABLE
        };

        using SPN_3698 = SPN_STATUS<SPN_3698_STATES, 7, 3, 3>;

        enum class SPN_3699_STATES {
            /*00b = not active*/
            NOT_ACTIVE,
            /*01b = active*/
            ACTIVE,
            /*10b = reserved for SAE assignment*/
            RESERVED,
            /*11b = not available*/
            NOT_AVAILABLE
        };

        using SPN_3699 = SPN_STATUS<SPN_3699_STATES, 2, 1, 2>;

        enum class SPN_3700_STATES {
            /*00b = not active*/
            NOT_ACTIVE,
            /*01b = active: only if standstill regeneration is ongoing*/
            ACTIVE,
            /*10b = regeneration needed - automatically initiated active regeneration imminent*/
            NEEDED,
            /*11b = not available*/
            NOT_AVAILABLE
        };

        using SPN_3700 = SPN_STATUS<SPN_3700_STATES, 2, 3, 2>;

        enum class SPN_3701_STATES {
            /*000b = Regeneration not needed*/
            NOT_NEEDED,
            /*001b = Regeneration needed - lowest level*/
            NEEDED_LOWEST_LEVEL,
            /*010b = Regeneration needed - moderate level*/
            NEEDED_MODERATE_LEVEL,
            /*011b = Regeneration needed - highest level*/
            NEEDED_HIGHEST_LEVEL,
            /*100b = reserved for SAE assignment*/
            RESERVED_1,
            /*101b = reserved for SAE assignment*/
            RESERVED_2,
            /*110b = reserved for SAE assignment*/
            RESERVED_3,
            /*111b = not available*/
            NOT_AVAILABLE
        };

        using SPN_3701 = SPN_STATUS<SPN_3701_STATES, 2, 5, 3>;

        enum class SPN_3702_STATES {
            /*00b = not inhibited*/
            NOT_INHIBITED,
            /*01b = inhibited*/
            INHIBITED,
            /*10b = reserved for SAE assignment*/
            RESERVED,
            /*11b = not available*/
            NOT_AVAILABLE
        };

        using SPN_3702 = SPN_STATUS<SPN_3702_STATES, 3, 1, 2>;
        
        using SPN_3703_STATES = SPN_3702_STATES;
        using SPN_3703 = SPN_STATUS<SPN_3703_STATES, 3, 3, 2>;

        using SPN_3704_STATES = SPN_3702_STATES;
        using SPN_3704 = SPN_STATUS<SPN_3704_STATES, 3, 5, 2>;

        using SPN_3705_STATES = SPN_3702_STATES;
        using SPN_3705 = SPN_STATUS<SPN_3705_STATES, 3, 7, 2>;

        using SPN_3706_STATES = SPN_3702_STATES;
        using SPN_3706 = SPN_STATUS<SPN_3706_STATES, 4, 1, 2>;

        using SPN_3707_STATES = SPN_3702_STATES;
        using SPN_3707 = SPN_STATUS<SPN_3707_STATES, 4, 3, 2>;

        using SPN_3708_STATES = SPN_3702_STATES;
        using SPN_3708 = SPN_STATUS<SPN_3708_STATES, 4, 5, 2>;

        using SPN_3709_STATES = SPN_3702_STATES;
        using SPN_3709 = SPN_STATUS<SPN_3709_STATES, 4, 7, 2>;

        using SPN_3710_STATES = SPN_3702_STATES;
        using SPN_3710 = SPN_STATUS<SPN_3710_STATES, 5, 1, 2>;

        using SPN_3711_STATES = SPN_3702_STATES;
        using SPN_3711 = SPN_STATUS<SPN_3711_STATES, 5, 3, 2>;

        using SPN_3712_STATES = SPN_3702_STATES;
        using SPN_3712 = SPN_STATUS<SPN_3712_STATES, 5, 5, 2>;

        using SPN_3713_STATES = SPN_3702_STATES;
        using SPN_3713 = SPN_STATUS<SPN_3713_STATES, 5, 7, 2>;

        using SPN_3714_STATES = SPN_3702_STATES;
        using SPN_3714 = SPN_STATUS<SPN_3714_STATES, 6, 1, 2>;

        using SPN_3715_STATES = SPN_3702_STATES;
        using SPN_3715 = SPN_STATUS<SPN_3715_STATES, 6, 3, 2>;

        using SPN_3716_STATES = SPN_3702_STATES;
        using SPN_3716 = SPN_STATUS<SPN_3716_STATES, 6, 5, 2>;

        using SPN_3717_STATES = SPN_3702_STATES;
        using SPN_3717 = SPN_STATUS<SPN_3717_STATES, 6, 7, 2>;

        using SPN_3718_STATES = SPN_3702_STATES;
        using SPN_3718 = SPN_STATUS<SPN_3718_STATES, 7, 1, 2>;

        enum class SPN_3750_STATES {
            /*00b = active DPF regeneration not inhibited*/
            NOT_INHIBITED,
            /*01b = active DPF regeneration inhibited*/
            INHIBITED,
            /*10b = reserved for SAE assignment*/
            RESERVED,
            /*11b = not available*/
            NOT_AVAILABLE
        };
        using SPN_3750 = SPN_STATUS<SPN_3750_STATES, 8, 5, 2>;
    } // namespace SPN
}     // namespace J1939

#endif /* __SPN_3xxx_H__ */
