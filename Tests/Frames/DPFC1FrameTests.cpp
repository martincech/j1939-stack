#include <gtest/gtest.h>

#include "Frames/DPFC1Frame.h"
#include "Frames/PGN.h"

using namespace J1939;
namespace DPFC1FrameTests {
    using namespace DPFC1;
    struct DPFC1TestFrame : testing::Test {
        DPFC1::Frame *frame;
        const u8 src = 0x25;
        const u8 priority = 0x04;
        const u32 expectedId = (static_cast<u32>(PGN::DPFC1) << ID::PGN_OFFSET) | (src << ID::SRC_ADDR_OFFSET) | (priority << ID::PRIORITY_OFFSET);
        std::vector<u8> data;

        DPFC1TestFrame() {
            frame = new DPFC1::Frame();
            frame->setSrcAddr(src);
            data = std::vector<u8> {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
        }

        ~DPFC1TestFrame() {
            delete frame;
        }


        void assertInhibition(int &b, int &bit, int &i)
        {
            data = std::vector<u8>{0,0,0,0,0,0,0,0};
            data[2] = static_cast<u8>(SPN_3702_STATES::INHIBITED);
            data[b] |= (static_cast<u8>(SPN_3702_STATES::INHIBITED)) << (bit%8);
            ASSERT_TRUE(frame->decode(expectedId, data.data(), data.size()));
            ASSERT_EQ(frame->getActiveInhibitReason(), static_cast<ActiveRegenerationInhibitedReason>(i));
            ASSERT_TRUE(frame->activeRegenerationIsInhibited());
            bit+=2;
            if(bit % 8 ==0 )
            {
                b++;
            }
        }
    };

    TEST_F(DPFC1TestFrame, DecodeCorrect) {
        ASSERT_TRUE(frame->decode(expectedId, data.data(), data.size()));
        ASSERT_EQ(frame->getPriority(), priority);
        ASSERT_EQ(frame->getDataLength(), DPFC1::PACKET_SIZE);
    }

    TEST_F(DPFC1TestFrame, LampStatus)
    {
        for(auto i = static_cast<int>(LampCommand::OFF); i <= static_cast<int>(LampCommand::NOT_AVAILABLE); i++)
        {
            data[0] = ((~0b111) << 0)| static_cast<u8>(static_cast<LampCommand>(i));
            ASSERT_TRUE(frame->decode(expectedId, data.data(), data.size()));
            ASSERT_EQ(frame->getLamp(), static_cast<LampCommand>(i));
        }                
    }

    TEST_F(DPFC1TestFrame, ActiveRegenerationAvailability)
    {
        for(auto i = static_cast<int>(ActiveRegenerationAvailability::NOT_READY); i <= static_cast<int>(ActiveRegenerationAvailability::NOT_AVAILABLE); i++)
        {
            data[0] = (~0b11 << 3) | (static_cast<u8>(static_cast<ActiveRegenerationAvailability>(i)) << 3);
            ASSERT_TRUE(frame->decode(expectedId, data.data(), data.size()));
            ASSERT_EQ(frame->getActiveAvailable(), static_cast<ActiveRegenerationAvailability>(i));
        }                
    }
    TEST_F(DPFC1TestFrame, PassiveStatus)
    {
        for(auto i = static_cast<int>(PassiveRegenerationStatus::NOT_ACTIVE); i <= static_cast<int>(PassiveRegenerationStatus::NOT_AVAILABLE); i++)
        {
            data[1] = (~0b11 << 0) | (static_cast<u8>(static_cast<PassiveRegenerationStatus>(i)) << 0);
            ASSERT_TRUE(frame->decode(expectedId, data.data(), data.size()));
            ASSERT_EQ(frame->getPasiveStatus(), static_cast<PassiveRegenerationStatus>(i));
        }                
    }

    TEST_F(DPFC1TestFrame, ActiveStatus)
    {
        for(auto i = static_cast<int>(ActiveRegenerationStatus::NOT_ACTIVE); i <= static_cast<int>(ActiveRegenerationStatus::NOT_AVAILABLE); i++)
        {
            data[1] = (~0b11 << 2) | (static_cast<u8>(static_cast<ActiveRegenerationStatus>(i)) << 2);
            ASSERT_TRUE(frame->decode(expectedId, data.data(), data.size()));
            ASSERT_EQ(frame->getActiveStatus(), static_cast<ActiveRegenerationStatus>(i));
        }                
    }

    TEST_F(DPFC1TestFrame, ActiveLevel)
    {
        for(auto i = static_cast<int>(ActiveRegenerationNeedLevel::NOT_NEEDED); i <= static_cast<int>(ActiveRegenerationNeedLevel::NOT_AVAILABLE); i++)
        {
            data[1] = (~0b111 << 4) | (static_cast<u8>(static_cast<ActiveRegenerationNeedLevel>(i)) << 4);
            ASSERT_TRUE(frame->decode(expectedId, data.data(), data.size()));
            ASSERT_EQ(frame->getActiveLevel(), static_cast<ActiveRegenerationNeedLevel>(i));
        }                
    }


    TEST_F(DPFC1TestFrame, ActiveInhibitionReason)
    {
        data = std::vector<u8>{0,0,0,0,0,0,0,0};
        auto b = 2;
        auto bit = 2;
        
        ASSERT_TRUE(frame->decode(expectedId, data.data(), data.size()));
        ASSERT_EQ(frame->getActiveInhibitReason(),ActiveRegenerationInhibitedReason::NOT_INHIBITED);
        ASSERT_FALSE(frame->activeRegenerationIsInhibited());
        
        data[b] = static_cast<u8>(SPN_3702_STATES::INHIBITED);
        ASSERT_TRUE(frame->decode(expectedId, data.data(), data.size()));
        ASSERT_EQ(frame->getActiveInhibitReason(),ActiveRegenerationInhibitedReason::INHIBITED_DUE_UNKNOWN_REASON);
        ASSERT_TRUE(frame->activeRegenerationIsInhibited());


        for(auto i = static_cast<int>(ActiveRegenerationInhibitedReason::INHIBITED_DUE_INHIBIT_SWITCH); i <= static_cast<int>(ActiveRegenerationInhibitedReason::INHIBITED_DUE_VEHICLE_SPEED_BELOW_ALLOWED_SPEED); i++)
        {
            assertInhibition(b,bit,i);            
        }
        auto i =  static_cast<int>(ActiveRegenerationInhibitedReason::INHIBITED_DUE_LOW_EXHAUST_GAS_PRESSURE);
        b = 7;
        bit = 2;
        assertInhibition(b,bit,i);
        i =  static_cast<int>(ActiveRegenerationInhibitedReason::INHIBITED_DUE_TO_TRESHER);
        bit = 6;
        assertInhibition(b,bit,i);
    }
}