#ifndef __SPN_H__
#define __SPN_H__
#include "J1939Common.h"
#include <cstdint>

namespace J1939 {
    namespace SPN {
        /**
         * BYTE - in which byte this SPN starts (1 based)
         * BIT - in which bit of this byte this SPN starts (1 based)
         * WIDTH - width of SPN in bits
         */
        template <uint32_t BYTE, uint32_t BIT, uint32_t WIDTH>
        struct SPN {
            static constexpr uint8_t MaskOfWidth(const uint8_t width) {
                auto mask = 0;
                for (auto m = 0; m < width; m++) {
                    mask = mask | 1 << m;
                }
                return mask;
            }

            static u8 *ByteInBuffer(const u8 *buffer)
            {
                return const_cast<u8*>(buffer + BYTE - 1);
            }


            static uint64_t MaskValue(const u8 *buffer) {
                return (*ByteInBuffer(buffer) >> (BIT - 1)) & MaskOfWidth(WIDTH);
            }
        };

        /**
         * E - enum type representing this SPN status 
         * BYTE - in which byte this SPN starts (1 based)
         * BIT - in which bit of this byte this SPN starts (1 based)
         * WIDTH - width of SPN in bits
         */
        template <typename E, uint32_t BYTE, uint32_t BIT = 1, uint32_t WIDTH = 8>
        struct SPN_STATUS : SPN<BYTE, BIT, WIDTH> {
            static E decode(const u8 *buffer) {
                return static_cast<E>(SPN<BYTE, BIT, WIDTH>::MaskValue(buffer));
            }

            static void encode(u8 *buffer, E value)
            {
                *SPN<BYTE, BIT, WIDTH>::ByteInBuffer(buffer) |= (static_cast<uint64_t>(value) & SPN<BYTE, BIT, WIDTH>::MaskOfWidth(WIDTH)) << (BIT -1);
            }
        };
    }
} /* namespace J1939 */

#endif /* __SPN_H__ */
