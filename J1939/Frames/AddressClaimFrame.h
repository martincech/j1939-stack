#ifndef __ADDRESS_CLAIM_FRAME_H__
#define __ADDRESS_CLAIM_FRAME_H__

#include "J1939Frame.h"

namespace J1939 {
    namespace AddressClaim {
        constexpr auto PACKET_SIZE = 8;
        constexpr auto DEFAULT_PRIORITY = 6;
        namespace EcuName {
            constexpr auto ARBITRARY_ADDR_CAPABLE_MASK = 0x1;
            constexpr auto ARBITRARY_ADDR_CAPABLE_OFFSET = 63;
            constexpr auto INDUSTRY_GROUP_MASK = 0x7;
            constexpr auto INDUSTRY_GROUP_OFFSET = 60;
            constexpr auto VEHICLE_SYSTEM_INTERFACE_MASK = 0xF;
            constexpr auto VEHICLE_SYSTEM_INTERFACE_OFFSET = 56;
            constexpr auto VEHICLE_SYSTEM_MASK = 0x7F;
            constexpr auto VEHICLE_SYSTEM_OFFSET = 49;
            constexpr auto FUNCTION_MASK = 0xFF;
            constexpr auto FUNCTION_OFFSET = 40;
            constexpr auto FUNCTION_INSTANCE_MASK = 0x1F;
            constexpr auto FUNCTION_INSTANCE_OFFSET = 35;
            constexpr auto ECU_INSTANCE_MASK = 0x7;
            constexpr auto ECU_INSTANCE_OFFSET = 32;
            constexpr auto MANUFACTURER_CODE_MASK = 0x7FF;
            constexpr auto MANUFACTURER_CODE_OFFSET = 21;
            constexpr auto IDENTITY_NUMBER_MASK = 0x1FFFFF;
            constexpr auto IDENTITY_NUMBER_OFFSET = 0;

            class EcuName {
                u32 mIdNumber = 0;
                u16 mManufacturerCode = 0;
                u8 mEcuInstance = 0;
                u8 mFunctionInstance = 0;
                u8 mFunction = 0;
                u8 mVehicleSystem = 0;
                u8 mVehicleSystemInstance = 0;
                u8 mIndustryGroup = 0;
                bool mArbitraryAddressCapable = false;

            public:
                EcuName();

                EcuName(u32 idNumber,
                        u16 manufacturerCode,
                        u8 ecuInstance,
                        u8 functionInstance,
                        u8 function,
                        u8 vehicleSystem,
                        u8 vehicSystemInstance,
                        u8 industryGroup,
                        bool arbitraryAddressCapable);
                virtual ~EcuName();

                u64 getValue() const;

                bool operator<(const EcuName &other) const;

                bool operator>(const EcuName &other) const;

                bool isArbitraryAddressCapable() const;

                void setArbitraryAddressCapable(bool arbitraryAddressCapable);

                u8 getEcuInstance() const;

                void setEcuInstance(u8 ecuInstance);

                u8 getFunction() const;

                void setFunction(u8 function);

                u8 getFunctionInstance() const;

                void setFunctionInstance(u8 functionInstance);

                u32 getIdNumber() const;

                void setIdNumber(u32 idNumber);

                u8 getIndustryGroup() const;

                void setIndustryGroup(u8 industryGroup);

                u16 getManufacturerCode() const;

                void setManufacturerCode(u16 manufacturerCode);

                u8 getVehicleSystem() const;

                void setVehicleSystem(u8 vehicleSystem);

                u8 getVehicleSystemInstance() const;

                void setVehicleSystemInstance(u8 vehicleSystemInstance);
            };
        } // namespace EcuName

        /**
         * Parameter Group Name             Request
         * Parameter Group Number           60928 (00EE00hex)
         * Definition                       Address Claimed Message
         * Transmission                     Rate As required.
         * Data Length                      8 bytes (CAN DLC = 8)
         * Extended Data Page (R)           0
         * Data Page                        0
         * PDU Format                       238
         * PDU Specific                     255 = Global Destination Address
         * Default Priority                 6
         * Data Description                 NAME of Controller Application
         * Byte 1                           Bits 8-1: LSB of Identity Field
         * Byte 2                           Bits 8-1: 2nd byte of Identity Field
         * Byte 3                           Bits 8-6: LSB of Manufacturer Code
         *                                  Bits 5-1: MSB of Identity Field
         * Byte 4                           Bits 8-1: MSB of Manufacturer Code
         * Byte 5                           Bits 8-4: Function Instance
         *                                  Bits 3-1: ECU Instance
         * Byte 6                           Bits 8-1: Function
         * Byte 7                           Bits 8-2: Vehicle System
         *                                  Bit 1: Reserved
         * Byte 8                           Bit 8: Arbitrary Address Capable
         *                                  Bits 7-5: Industry Group
         *                                  Bits 4-1: Vehicle System Instance
         */
        class Frame : public J1939::Frame {
            EcuName::EcuName mEcuName;

        protected:
            bool decodeData(const u8 *buffer, const size_t &length) override;
            bool encodeData(u8 *buffer) const override;

        public:
            Frame();
            Frame(const EcuName::EcuName &name);
            virtual ~Frame();

            size_t getDataLength() const override;
            const EcuName::EcuName &getEcuName() const;
            void setEcuName(EcuName::EcuName &ecuName);

#if defined(DEBUG) && defined(J1939_STRINGS)
            std::string toString() override;
#endif
        };
    } // namespace AddressClaim
}     /* namespace J1939 */

#endif /* __ADDRESS_CLAIM_FRAME_H__ */
