#include "RequestFrame.h"
#include "J1939Common.h"
#include "PGN.h"
#if defined(DEBUG) && defined(J1939_STRINGS)
#include <ostream>
#include <sstream>
#include <string>
constexpr auto REQUEST_FRAME_NAME = "Request";
#endif

namespace J1939 {
    namespace Request {
        Frame::Frame()
            : J1939::Frame(PGN::REQUEST_PGN,
                           DEFAULT_PRIORITY
#if defined(DEBUG) && defined(J1939_STRINGS)
                         ,
                         REQUEST_FRAME_NAME
#endif
                        ) {
        }

        Frame::Frame(u32 requestPGN)
            : Frame() {
            mRequestPGN = requestPGN;
        }

        Frame::~Frame() = default;

        size_t Frame::getDataLength() const { return PACKET_SIZE; }

        u32 Frame::getRequestPGN() const { return mRequestPGN; }

        void Frame::setRequestPGN(u32 requestPGN) { mRequestPGN = requestPGN; }

        bool Frame::decodeData(const u8 *buffer, const size_t &length) {
            if (length < getDataLength()) {
                return false;
            }
            mRequestPGN = buffer[0];
            mRequestPGN |= buffer[1] << 8;
            mRequestPGN |= buffer[2] << 16;
            mRequestPGN &= ID::PGN_MASK_PDUF2;
            return true;
        }

        bool Frame::encodeData(u8 *buffer) const {
            buffer[0] = mRequestPGN & 0xFF;
            buffer[1] = (mRequestPGN >> 8) & 0xFF;
            buffer[2] = (mRequestPGN >> 16) & (ID::PGN_MASK_PDUF2 >> 16);
            return true;
        }

#if defined(DEBUG) && defined(J1939_STRINGS)
        std::string Frame::toString() {
            auto retVal = Frame::toString();
            std::stringstream sstr;
            sstr << "Request PGN: " << std::hex << mRequestPGN << std::endl;
            return retVal + sstr.str();
        }
#endif
    } // namespace Request
}     /* namespace J1939 */
