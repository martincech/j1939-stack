#include "TestFrame.h"
#include <cstring>

namespace J1939 {

    bool TestFrame::decodeData(const u8 *buffer, const size_t &length) {
        mRaw.clear();
        mRaw.append(buffer, length);
        return true;
    }

    bool TestFrame::encodeData(u8 *buffer) const {
        memcpy(buffer, mRaw.c_str(), getDataLength());
        return true;
    }

    TestFrame::TestFrame(u32 pgn)
        : Frame(pgn, "TestFrame") {
    }

    TestFrame::TestFrame(u32 pgn, u8 prio)
        : Frame(pgn, prio, "TestFrame") {
    }

    TestFrame::TestFrame(u32 pgn, const char *name)
        : Frame(pgn, name) {
    }

    TestFrame::TestFrame(u32 pgn, u8 prio, const char *name)
        : Frame(pgn, prio, name) {
    }

    TestFrame::~TestFrame() = default;

    std::basic_string<u8> TestFrame::getRaw() const { return mRaw; }

    void TestFrame::setRaw(const std::basic_string<u8>& raw) {
        decodeData(raw.data(), raw.length());
    }

    size_t TestFrame::getDataLength() const { return mRaw.size(); }

} /* namespace J1939 */
