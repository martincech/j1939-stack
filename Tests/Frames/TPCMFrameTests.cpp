#include <gtest/gtest.h>

#include "Frames/PGN.h"
#include "Frames/TPCMFrame.h"

using namespace J1939;
using namespace Transport;
namespace TPCMFrameTests {
    struct TPCMTestFrame : testing::Test {
        TPCM::Frame *frame;
        const u8 src = 0x25;
        const u8 dst = 0x55;
        const u8 priority = 0x04;
        const u32 dataPgn = 0xEAABCD;
        const u32 expectedId = (static_cast<u32>(PGN::TPCM) << ID::PGN_OFFSET) | (src << ID::SRC_ADDR_OFFSET) | (dst << ID::DST_ADDR_OFFSET) | (priority << ID::PRIORITY_OFFSET);

        TPCMTestFrame() {
            frame = new TPCM::Frame();
            frame->setSrcAddr(src);
            frame->setDstAddr(dst);
            frame->setPriority(priority);
            frame->setDataPgn(dataPgn);
        }

        ~TPCMTestFrame() {
            delete frame;
        }
    };

    struct TPMCMCommon : TPCMTestFrame, testing::WithParamInterface<TPCM::ControlType> {
    };

    using TPMCMCommonDecode = TPMCMCommon;
    TEST_P(TPMCMCommonDecode, decode) {
        const size_t buflen = 8;
        u16 bytesToSend = 1253;
        const u8 packetsToSend = bytesToSend / 7 + (bytesToSend % 7 ? 1 : 0);
        u8 expectedData[] = {static_cast<u8>(GetParam()), reinterpret_cast<u8 *>(&bytesToSend)[0], reinterpret_cast<u8 *>(&bytesToSend)[1], packetsToSend, 0xFF, ((u8 *)&dataPgn)[0], ((u8 *)&dataPgn)[1], ((u8 *)&dataPgn)[2]};

        ASSERT_TRUE(frame->decode(expectedId, expectedData, buflen));
        ASSERT_EQ(frame->getCtrlType(), GetParam());
        ASSERT_EQ(frame->getAbortReason(), TPCM::AbortReason::NONE);
        ASSERT_EQ(frame->getDataPgn(), dataPgn);
        ASSERT_EQ(frame->getRTSMaxPackets(), 0xFF);
        ASSERT_EQ(frame->getTotalMsgSize(), bytesToSend);
        ASSERT_EQ(frame->getTotalPackets(), packetsToSend);
        ASSERT_EQ(frame->getCTSNextPacket(), 0);
        ASSERT_EQ(frame->getCTSPacketsToTx(), 0);

        expectedData[3] = 2;
        ASSERT_FALSE(frame->decode(expectedId, expectedData, buflen)); // total packets is not equal to bytestosend
    }

    INSTANTIATE_TEST_CASE_P(Decode, TPMCMCommonDecode,
        testing::Values(
            TPCM::ControlType::RTS,
            TPCM::ControlType::EOM_ACK,
            TPCM::ControlType::BAM)
    );

    using TPMCMCommonEncode = TPMCMCommon;
    TEST_P(TPMCMCommonEncode, encode) {
        u32 id;
        u8 buf[8];
        size_t buflen = 8;
        u16 bytesToSend = 1253;
        const u8 packetsToSend = bytesToSend / 7 + (bytesToSend % 7 ? 1 : 0);
        u8 expectedData[] = {static_cast<u8>(GetParam()), ((u8 *)&bytesToSend)[0], ((u8 *)&bytesToSend)[1], packetsToSend, 0xFF, ((u8 *)&dataPgn)[0], ((u8 *)&dataPgn)[1], ((u8 *)&dataPgn)[2]};

        frame->setCtrlType(GetParam());

        ASSERT_FALSE(frame->setAbortReason(TPCM::AbortReason::MULTI_CTS));
        ASSERT_FALSE(frame->setCTSNextPacket(1));
        ASSERT_FALSE(frame->setCTSPacketsToTx(1));

        ASSERT_TRUE(frame->setTotalMsgSize(bytesToSend));
        ASSERT_TRUE(frame->encode(id, buf, buflen));
        ASSERT_EQ(buflen, 8);
        ASSERT_EQ(id, expectedId);
        ASSERT_EQ(memcmp(buf, expectedData, buflen), 0);
    }

    INSTANTIATE_TEST_CASE_P(Encode, TPMCMCommonEncode,
        testing::Values(
            TPCM::ControlType::RTS,
            TPCM::ControlType::EOM_ACK,
            TPCM::ControlType::BAM)
    );

    TEST_F(TPCMTestFrame, RTS_decodeMaxTxPackets) {
        const size_t buflen = 8;
        u16 bytesToSend = 1253;
        const u8 packetsToSend = bytesToSend / 7 + (bytesToSend % 7 ? 1 : 0);
        u8 expectedData[] = {static_cast<u8>(TPCM::ControlType::RTS), ((u8 *)&bytesToSend)[0], ((u8 *)&bytesToSend)[1], packetsToSend, 2, ((u8 *)&dataPgn)[0], ((u8 *)&dataPgn)[1], ((u8 *)&dataPgn)[2]};

        ASSERT_TRUE(frame->decode(expectedId, expectedData, buflen));
    }

    TEST_F(TPCMTestFrame, RTS_encodeMaxTxPackets) {
        u32 id;
        u8 buf[8];
        size_t buflen = 8;
        u16 bytesToSend = 1253;
        const u8 packetsToSend = bytesToSend / 7 + (bytesToSend % 7 ? 1 : 0);
        const u8 maxTxPackets = 2;
        u8 expectedData[] = {static_cast<u8>(TPCM::ControlType::RTS), ((u8 *)&bytesToSend)[0], ((u8 *)&bytesToSend)[1], packetsToSend, maxTxPackets, ((u8 *)&dataPgn)[0], ((u8 *)&dataPgn)[1], ((u8 *)&dataPgn)[2]};

        frame->setCtrlType(TPCM::ControlType::RTS);
        ASSERT_TRUE(frame->setTotalMsgSize(bytesToSend));
        ASSERT_TRUE(frame->setRTSMaxPackets(maxTxPackets));
        ASSERT_TRUE(frame->encode(id, buf, buflen));
        ASSERT_EQ(memcmp(buf, expectedData, buflen), 0);
    }

    TEST_F(TPCMTestFrame, RTSFrame_encodeZeroSize) {
        u8 buf[8];
        size_t buflen = 8;
        u32 id;
        ASSERT_FALSE(frame->encode(id, buf, buflen));
    }

    TEST_F(TPCMTestFrame, CTSFrame_decode) {
        const size_t buflen = 8;
        const u16 bytesToSend = 1253;
        const u8 packetsToSend = bytesToSend / 7 + (false ? 1 : 0);
        const u8 nextPacket = 10;
        u8 expectedData[] = {static_cast<u8>(TPCM::ControlType::CTS), packetsToSend, nextPacket, 0xFF, 0xFF, ((u8 *)&dataPgn)[0], ((u8 *)&dataPgn)[1], ((u8 *)&dataPgn)[2]};

        ASSERT_TRUE(frame->decode(expectedId, expectedData, buflen));
        ASSERT_EQ(frame->getCtrlType(), TPCM::ControlType::CTS);
        ASSERT_EQ(frame->getAbortReason(), TPCM::AbortReason::NONE);
        ASSERT_EQ(frame->getDataPgn(), dataPgn);
        ASSERT_EQ(frame->getRTSMaxPackets(), 0xFF);
        ASSERT_EQ(frame->getTotalMsgSize(), 0);
        ASSERT_EQ(frame->getTotalPackets(), 0);
        ASSERT_EQ(frame->getCTSNextPacket(), nextPacket);
        ASSERT_EQ(frame->getCTSPacketsToTx(), packetsToSend);

        expectedData[1] = 0;
        expectedData[2] = 0;
        ASSERT_TRUE(frame->decode(expectedId, expectedData, buflen));
        ASSERT_EQ(frame->getCTSPacketsToTx(), 0);
        ASSERT_EQ(frame->getCTSNextPacket(), 0);
    }

    TEST_F(TPCMTestFrame, CTSFrame_encode) {
        u32 id;
        u8 buf[8];
        size_t buflen = 8;
        const u16 bytesToSend = 1253;
        const u8 packetsToSend = bytesToSend / 7 + (false ? 1 : 0);
        const u8 nextPacket = 10;
        u8 expectedData[] = {static_cast<u8>(TPCM::ControlType::CTS), packetsToSend, nextPacket, 0xFF, 0xFF, ((u8 *)&dataPgn)[0], ((u8 *)&dataPgn)[1], ((u8 *)&dataPgn)[2]};

        frame->setCtrlType(TPCM::ControlType::CTS);

        ASSERT_FALSE(frame->setAbortReason(TPCM::AbortReason::MULTI_CTS));
        ASSERT_FALSE(frame->setTotalMsgSize(bytesToSend));

        ASSERT_TRUE(frame->setCTSNextPacket(nextPacket));
        ASSERT_TRUE(frame->setCTSPacketsToTx(packetsToSend));

        ASSERT_TRUE(frame->encode(id, buf, buflen));
        ASSERT_EQ(buflen, 8);
        ASSERT_EQ(id, expectedId);
        ASSERT_EQ(memcmp(buf, expectedData, buflen), 0);

        expectedData[1] = 1;
        expectedData[2] = 1;
        ASSERT_TRUE(frame->setCTSNextPacket(1));
        ASSERT_TRUE(frame->setCTSPacketsToTx(1));

        ASSERT_TRUE(frame->encode(id, buf, buflen));
        ASSERT_EQ(memcmp(buf, expectedData, buflen), 0);
    }

    TEST_F(TPCMTestFrame, AbortFrame_decode)
    {
        const size_t buflen = 8;

        int abortReason;
        for (abortReason = static_cast<u32>(TPCM::AbortReason::NOT_SUPPORTED); abortReason < static_cast<u32>(TPCM::AbortReason::_LAST); abortReason++) {
            u8 expectedData[] = {static_cast<u8>(TPCM::ControlType::ABORT), abortReason, 0xFF, 0xFF, 0xFF, ((u8 *)&dataPgn)[0], ((u8 *)&dataPgn)[1], ((u8 *)&dataPgn)[2]};
            ASSERT_TRUE(frame->decode(expectedId, expectedData, buflen));
            ASSERT_EQ(frame->getCtrlType(), TPCM::ControlType::ABORT);
            ASSERT_EQ(frame->getAbortReason(), static_cast<TPCM::AbortReason>(abortReason));
            ASSERT_EQ(frame->getDataPgn(), dataPgn);
            ASSERT_EQ(frame->getRTSMaxPackets(), 0xFF);
            ASSERT_EQ(frame->getTotalMsgSize(), 0);
            ASSERT_EQ(frame->getTotalPackets(), 0);
            ASSERT_EQ(frame->getCTSNextPacket(), 0);
            ASSERT_EQ(frame->getCTSPacketsToTx(), 0);   
        }
        // invalid abort reason
        abortReason =0;
        u8 expectedData[] = {static_cast<u8>(TPCM::ControlType::ABORT), abortReason, 0xFF, 0xFF, 0xFF, ((u8 *)&dataPgn)[0], ((u8 *)&dataPgn)[1], ((u8 *)&dataPgn)[2]};
        ASSERT_FALSE(frame->decode(expectedId, expectedData, buflen));
        expectedData[1] = static_cast<u8>(TPCM::AbortReason::_LAST);
        ASSERT_FALSE(frame->decode(expectedId, expectedData, buflen));
    }

    TEST_F(TPCMTestFrame, AbortFrame_encode)
    {
        u32 id;
        u8 buf[8];
        size_t buflen = 8;
        u8 expectedData[] = {static_cast<u8>(TPCM::ControlType::ABORT), 0, 0xFF, 0xFF, 0xFF, ((u8 *)&dataPgn)[0], ((u8 *)&dataPgn)[1], ((u8 *)&dataPgn)[2]};

        frame->setCtrlType(TPCM::ControlType::ABORT);
        ASSERT_FALSE(frame->setCTSNextPacket(1));
        ASSERT_FALSE(frame->setCTSPacketsToTx(1));
        ASSERT_FALSE(frame->setRTSMaxPackets(2));
        ASSERT_FALSE(frame->setTotalMsgSize(2));
        
        for (auto abortReason = static_cast<u32>(TPCM::AbortReason::NOT_SUPPORTED); abortReason < static_cast<u32>(TPCM::AbortReason::_LAST); abortReason++) {
            ASSERT_TRUE(frame->setAbortReason(static_cast<TPCM::AbortReason>(abortReason)));
            ASSERT_TRUE(frame->encode(id, buf, buflen));
            ASSERT_EQ(buflen, 8);
            ASSERT_EQ(id, expectedId);
            expectedData[1] = abortReason;
            ASSERT_EQ(memcmp(buf, expectedData, buflen), 0);   
        }
        
    }
} // namespace TPCMFrameTests
