#include <gtest/gtest.h>

#include "Frames/TPDTFrame.h"
#include "Frames/PGN.h"

using namespace J1939;
using namespace Transport;
namespace TPDTFrameTests {
    TEST(SequenceNumber, SetByConstructor) {
        auto sq = 5;
        u8 raw[] = {1, 2, 3, 4, 5, 6, 7, 8};
        TPDT::Frame frame(sq, raw, sizeof raw);

        ASSERT_EQ(frame.getSq(), sq);
    }

    TEST(SequenceNumber, SetByMethod) {
        const auto sq = 5;
        TPDT::Frame frame;
        frame.setSq(sq);

        ASSERT_EQ(frame.getSq(), sq);
    }

    struct TPDTTestFrame : testing::Test {
        TPDT::Frame *frame;
        const u8 src = 0x25;
        const u8 dst = 0x55;
        const u8 seq = 0x12;
        const u8 priority = 0x04;
        const u32 expectedId = (static_cast<u32>(PGN::TPDT) << ID::PGN_OFFSET) | (src << ID::SRC_ADDR_OFFSET) | (dst << ID::DST_ADDR_OFFSET) | (priority << ID::PRIORITY_OFFSET);
        std::vector<u8> *validRaw;
        std::vector<u8> *shortRaw;
        std::vector<u8> *longRaw;

        TPDTTestFrame() {
            frame = new TPDT::Frame();
            validRaw = new std::vector<u8> {seq, 2, 3, 4, 5, 6, 7, 8};
            shortRaw = new std::vector<u8>{seq, 2};
            longRaw = new std::vector<u8>{seq, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        }

        ~TPDTTestFrame() {
            delete frame;
            delete validRaw;
            delete shortRaw;
            delete longRaw;
        }
    };

    TEST_F(TPDTTestFrame, DecodeSqAndDataCorrect) {
        ASSERT_TRUE(frame->decode(expectedId, validRaw->data(), validRaw->size()));
        ASSERT_EQ(frame->getSq(), validRaw->front());
        ASSERT_EQ(frame->getPriority(), priority);
        ASSERT_EQ(memcmp(frame->getData(), validRaw->data()+1, validRaw->size() - 1), 0);
    }

    TEST_F(TPDTTestFrame, DecodeShortDataLen) {
        ASSERT_FALSE(frame->decode(expectedId, shortRaw->data(),shortRaw->size()));
        ASSERT_NE(frame->getSq(), shortRaw->front());
        ASSERT_EQ(frame->getPriority(), priority);
        ASSERT_NE(memcmp(frame->getData(), shortRaw->data()+1, shortRaw->size() - 1), 0);
    }

    TEST_F(TPDTTestFrame, DecodeLongDataLen) {
        ASSERT_TRUE(frame->decode(expectedId, longRaw->data(), longRaw->size()));
        ASSERT_EQ(frame->getSq(), longRaw->front());
        ASSERT_EQ(frame->getPriority(), priority);
        ASSERT_EQ(memcmp(frame->getData(), longRaw->data()+1, frame->getDataLength()- 1), 0);
    }

    TEST_F(TPDTTestFrame, EncodeSqAndDataCorrect) {

        u32 id;        
        u8 buf[8];
        frame->setSrcAddr(src);
        frame->setDstAddr(dst);
        frame->setPriority(priority);
        frame->setSq(seq);
        frame->setData(validRaw->data()+1, validRaw->size() - 1);
        size_t buflen = frame->getDataLength();

        ASSERT_TRUE(frame->encode(id, buf, buflen));
        ASSERT_EQ(id, expectedId);
        ASSERT_EQ(buflen, 8);
        ASSERT_EQ(memcmp(buf, validRaw->data(), validRaw->size()), 0);
    }

    TEST_F(TPDTTestFrame, EncodeShortDataLen) {
        u32 id;
        u8 origBuf[8];
        memset(origBuf, 0xFF, 8);
        u8 buf[8];
        frame->setSq(seq);
        frame->setData(shortRaw->data()+1, shortRaw->size()-1);
        const auto origBufLen = frame->getDataLength()-1;
        auto buflen = origBufLen;
        
        //short buffer
        ASSERT_FALSE(frame->encode(id, buf, buflen));
        ASSERT_EQ(buflen, origBufLen);
        
        //long enough buffer
        buflen = frame->getDataLength();
        ASSERT_TRUE(frame->encode(id, buf, buflen));        
        ASSERT_EQ(memcmp(buf, shortRaw->data(), shortRaw->size()), 0); //first part is correct
        ASSERT_EQ(memcmp(buf+shortRaw->size(), origBuf, buflen-shortRaw->size()), 0); // the rest is filled with FF
    }

    TEST_F(TPDTTestFrame, EncodeLongDataLen) {
        u32 id;
        u8 buf[8];
        frame->setSq(seq);
        frame->setData(longRaw->data()+1, longRaw->size());
        auto buflen = frame->getDataLength()+1;

        ASSERT_TRUE(frame->encode(id, buf, buflen));
        ASSERT_EQ(buflen, frame->getDataLength());
        ASSERT_EQ(memcmp(buf, longRaw->data(), buflen), 0);
    }
   
}