#ifndef __SPN_8xxx_H__
#define __SPN_8xxx_H__
#include "SPN.h"

namespace J1939 {
    namespace SPN {
        enum class SPN_8857_STATES {
            /*00b = Not ready for initiation*/
            NOT_READY,
            /*01b = Ready for initiation*/
            READY,
            /*10b = reserved for SAE assignment*/
            RESERVED,
            /*11b = not available*/
            NOT_AVAILABLE
        };

        using SPN_8857 = SPN_STATUS<SPN_8857_STATES, 1, 4, 2>;
    }

} /* namespace J1939 */

#endif /* __SPN_H__ */
