#include "Addressing/AddressClaimer.h"
#include "Transport/MultipacketFrameHandler.h"

#include "Factory.h"
#include "MockCallback.h"
#include "MockFrameHandler.h"
#include "MockTimer.h"
#include "TestFrame.h"
#include <gmock/gmock.h>
#include <ostream>
#include "Frames/GenericMultiFrame.h"

using namespace J1939;
using namespace Tests;
using namespace Transport;
using testing::_;
using testing::Eq;
using testing::Invoke;
using testing::InvokeArgument;
using testing::InvokeWithoutArgs;
using testing::Matcher;
using testing::MatcherCast;
using testing::NiceMock;
using testing::Return;
using testing::SafeMatcherCast;
using testing::SaveArg;

namespace MultipacketFrameHandlerTests {

    static u8 claimedAddress = 5;

    struct MockAddressClaimer : AddressClaim::IAddressClaimer {
        MOCK_METHOD0(currentAddress, u8());
        MOCK_METHOD1(registerOnAddressClaimed, void(const AddressClaim::AddressClaimerCallbackFn &));
        MOCK_METHOD1(registerOnAddressConflict, void(const AddressClaim::AddressClaimerCallbackFn &));
    };

    struct MultipacketFrameHandlerTest : testing::Test {
        NiceMock<MockFrameHandler> mockHandler;
        NiceMock<MockTimer> mockTimer;
        MockCallback callbacks;
        NiceMock<MockAddressClaimer> mockAddressClaimer;
        MultipacketFrameHandler *uutHandler;


        MultipacketFrameHandlerTest() {
            Factory::registerStackFrames();
            uutHandler = new MultipacketFrameHandler(&mockHandler, &mockTimer, &mockAddressClaimer);
            uutHandler->registerReceiveHandler(MockCallback::frameReceivedMockFn);

            ON_CALL(mockAddressClaimer, currentAddress())
                .WillByDefault(Return(claimedAddress));

        }

        ~MultipacketFrameHandlerTest() {
            delete uutHandler;
        }
    };

    struct Params {
        int srcAddr;
        int dstAddr;
        bool received;
        int pgn;

        friend std::ostream &operator<<(std::ostream &os, const Params &obj) {
            return os
                   << "srcAddr: " << obj.srcAddr
                   << " dstAddr: " << obj.dstAddr
                   << " received: " << obj.received
                   << " pgn: " << obj.pgn;
        }
    };

    struct MultipacketFrameHandlerParametrizedTest : MultipacketFrameHandlerTest, testing::WithParamInterface<Params> {
    };

    TEST_P(MultipacketFrameHandlerParametrizedTest, FrameReceived) {
        const auto param = GetParam();
        EXPECT_CALL(callbacks, FrameReceivedCallback(_))
            .Times(param.received ? 1 : 0);
        TestFrame frame(static_cast<u32>(param.pgn));
        frame.setSrcAddr(static_cast<u8>(param.srcAddr));
        frame.setDstAddr(static_cast<u8>(param.dstAddr));
        uutHandler->receive(&frame);
    }

    INSTANTIATE_TEST_CASE_P(ReceivedCalled, MultipacketFrameHandlerParametrizedTest,
        testing::Values(
            /* Peer to Peer  - for me*/
            Params{
                12,             // src
                claimedAddress, // dst
                true,           //received
                0x00AA00        //PGN
            },
            Params{
                0,              // src
                claimedAddress, // dst
                true,           //received
                0x00AA00        //PGN
            },
            Params{
                0xFF,           // src
                claimedAddress, // dst
                true,           //received
                0x00AA00        //PGN
            },

            /* Peer to Peer  - not for me*/
            Params{
                12,               // src
                claimedAddress+1, // dst
                false,            //received
                0x00AA00          //PGN
            },

            /* Broadcast */
            Params{
                12,                // src
                BROADCAST_ADDRESS, // dst
                true,              //received
                0x00AA00           //PGN
            },
            Params{
                0,                 // src
                BROADCAST_ADDRESS, // dst
                true,              //received
                0x00AA00           //PGN
            },
            Params{
                0xFF,              // src
                BROADCAST_ADDRESS, // dst
                true,              //received
                0x00AA00           //PGN
            },

            /* TPCM packet start of transport*/
            Params{
                12,                         // src
                claimedAddress,             // dst
                false,                      //received - not yet, started to process
                static_cast<int>(PGN::TPCM) //PGN
            },
            Params{
                12,                         // src
                BROADCAST_ADDRESS,          // dst
                false,                      //received - not yet, started to process
                static_cast<int>(PGN::TPCM) //PGN
            },
            /* TPDT packet */
            Params{
                12,                         // src
                claimedAddress,             // dst
                false,                      //received - not yet, started to process
                static_cast<int>(PGN::TPDT) //PGN
            },
            Params{
                12,                         // src
                BROADCAST_ADDRESS,          // dst
                false,                      //received - not yet, started to process
                static_cast<int>(PGN::TPDT) //PGN
            }
        )
    );

    struct BAMParams {
        int srcAddr;
        int dstAddr;
        int msgSize;
        int pgn;
        bool sessionStarted;

        friend std::ostream &operator<<(std::ostream &os, const BAMParams &obj) {
            return os
                   << "srcAddr: " << obj.srcAddr
                   << " dstAddr: " << obj.dstAddr
                   << " msgSize: " << obj.msgSize
                   << " pgn: " << obj.pgn
                   << " sessionStarted: " << obj.sessionStarted;
        }
    };

    struct BAMParametrizedTest : MultipacketFrameHandlerTest, testing::WithParamInterface<BAMParams> {};

    TEST_P(BAMParametrizedTest, BamSessionStarted) {
        auto param = GetParam();
        /* Nothing received yet */
        EXPECT_CALL(callbacks, FrameReceivedCallback(_))
            .Times(0);
        /* session registers timer to wait for DT packet */
        EXPECT_CALL(mockTimer, setTimer(TimeoutControl::T1,_))
            .Times(param.sessionStarted ? 1 : 0)
            .WillRepeatedly(Return(5));

        /* No response to BAM frame */
        EXPECT_CALL(mockHandler, send(_))
            .Times(0);

        /* BAM frame */
        TPCM::Frame frame;
        frame.setCtrlType(TPCM::ControlType::BAM);
        frame.setSrcAddr(static_cast<u8>(param.srcAddr));
        frame.setDstAddr(static_cast<u8>(param.dstAddr));
        frame.setTotalMsgSize(static_cast<u16>(param.msgSize));
        frame.setDataPgn(param.pgn);
        uutHandler->receive(&frame);
    }

    INSTANTIATE_TEST_CASE_P(SessionStarted, BAMParametrizedTest,
        testing::Values(
            /* Valid frames */
            BAMParams{
                12,                // src
                BROADCAST_ADDRESS, // dst
                250,               // message size
                0x00AA00,          //PGN
                true               // session started
            },
            BAMParams{
                12,                // src
                BROADCAST_ADDRESS, // dst
                MAX_PACKET_SIZE+1, // message size
                0x00AA00,          //PGN
                true               // session started
            },
            BAMParams{
                12,                              // src
                BROADCAST_ADDRESS,               // dst
                MAX_TRANSPORT_PACKETS_DATA_SIZE, // message size
                0x00AA00,                        //PGN
                true                             // session started
            },
            /* Invalid frames*/
            BAMParams{
                12,             // src
                claimedAddress, // dst - INVALID
                250,            // message size
                0x00AA00,       //PGN
                false           // session started
            },
            BAMParams{
                12,                                // src
                BROADCAST_ADDRESS+1,               // dst
                MAX_TRANSPORT_PACKETS_DATA_SIZE+1, // message size - INVALID
                0x00AA00,                          //PGN
                false                              // session started
            },
            BAMParams{
                12,                // src
                BROADCAST_ADDRESS, // dst
                MAX_PACKET_SIZE,   // message size - INVALID
                0x00AA00,          //PGN
                false              // session started
            },
            BAMParams{
                12,                // src
                BROADCAST_ADDRESS, // dst
                0,                 // message size - INVALID
                0x00AA00,          //PGN
                false              // session started
            },
            BAMParams{
                12,             // src
                claimedAddress, // dst - INVALID
                0,              // message size
                0x00AA00,       //PGN
                false           // session started
            }
        )
    );

    ACTION_P3(TestFrame, rcvPgn, msgSize, expectedData) {

        ASSERT_EQ(arg0->getPGN(), rcvPgn);
        ASSERT_EQ(arg0->getDataLength(), msgSize);
        ASSERT_EQ(memcmp(((const GenericMultiFrame *)arg0)->getRawData(), expectedData, arg0->getDataLength()), 0);
    }

    TEST_F(MultipacketFrameHandlerTest, BamSessionContinuesUntilComplete) {
        constexpr auto msgSize = 20;
        constexpr auto packets = msgSize / TPDT::DATA_SIZE + ((msgSize % TPDT::DATA_SIZE) == 0 ? 0 : 1);
        u8 expectedData[msgSize];
        for (auto i = 0; i < msgSize; i++) {
            expectedData[i] = i + 1;
        }
        const auto rcvPgn = 0x00AA00;
        /* Nothing received yet */
        EXPECT_CALL(callbacks, FrameReceivedCallback(_))
            .Times(1)
            .WillOnce(TestFrame(rcvPgn, msgSize, expectedData));
        /* session registers timer to wait for DT packet */
        EXPECT_CALL(mockTimer, setTimer(TimeoutControl::T1, _))
            .Times(packets)
            .WillRepeatedly(Return(5));
        /* timer cancelled when packet received */
        EXPECT_CALL(mockTimer, cancelTimer(5))
            .Times(packets)
            .WillRepeatedly(Return());

        /* No response to BAM frame */
        EXPECT_CALL(mockHandler, send(_))
            .Times(0);

        TPCM::Frame frame;
        frame.setCtrlType(TPCM::ControlType::BAM);
        frame.setSrcAddr(12);
        frame.setDstAddr(BROADCAST_ADDRESS);
        frame.setTotalMsgSize(msgSize);
        frame.setDataPgn(rcvPgn);
        uutHandler->receive(&frame);

        auto restSize = msgSize;
        for (int i = 0; i < packets; i++) {
            TPDT::Frame dtFrame;
            dtFrame.setSrcAddr(12);
            dtFrame.setDstAddr(BROADCAST_ADDRESS);
            dtFrame.setSq(i + 1);
            dtFrame.setData(&expectedData[i * TPDT::DATA_SIZE], restSize > TPDT::DATA_SIZE ? TPDT::DATA_SIZE : restSize);
            uutHandler->receive(&dtFrame);
            restSize -= TPDT::DATA_SIZE;
        }
    }
} // namespace MultipacketFrameHandlerTests
