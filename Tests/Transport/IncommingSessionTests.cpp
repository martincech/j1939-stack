#include "Addressing/AddressClaimer.h"
#include "Transport/MultipacketSession.h"

#include "Factory.h"
#include "MockCallback.h"
#include <gmock/gmock.h>
#include <ostream>
#include <complex.h>

using namespace J1939;
using namespace Tests;
using namespace Transport;
using testing::_;
using testing::Eq;
using testing::Invoke;
using testing::InvokeArgument;
using testing::InvokeWithoutArgs;
using testing::Matcher;
using testing::MatcherCast;
using testing::NiceMock;
using testing::Return;
using testing::SafeMatcherCast;
using testing::SaveArg;

namespace IncommingSessionTests {
    constexpr u8 claimedAddress = 5;
    constexpr u32 srcAddress = 12;

    class MockSessionOwner : public ISessionOwner {
    public:
        MOCK_METHOD2(setTimer, u32(std::chrono::milliseconds, TimerHandlerFnPtr));
        MOCK_METHOD1(cancelTimer, void(u32));
        MOCK_METHOD1(send, void(const Frame *));
        MOCK_METHOD1(receive, void(const Frame *));
        MOCK_METHOD0(currentAddress, u8());
    };

    struct IncommingSessionTest : testing::Test {
        NiceMock<MockSessionOwner> mockSessionOwner;
        TPCM::Frame bamSessionFrame;
        u32 pgn = 0x00AA00;
        std::vector<u8> expectedData;
        u32 msgSize;
        u32 dataPackets;
        TPCM::Frame p2pFrame;

        IncommingSession *uutSession;

        IncommingSessionTest()
            : IncommingSessionTest(125) {
        }

        IncommingSessionTest(u32 msgSize)
            : msgSize(msgSize)
            , dataPackets(msgSize / TPDT::DATA_SIZE + ((msgSize % TPDT::DATA_SIZE) == 0 ? 0 : 1)) {
            Factory::registerStackFrames();
            ON_CALL(mockSessionOwner, currentAddress())
                .WillByDefault(Return(claimedAddress));

            bamSessionFrame.setCtrlType(TPCM::ControlType::BAM);
            bamSessionFrame.setSrcAddr(srcAddress);
            bamSessionFrame.setDstAddr(BROADCAST_ADDRESS);
            bamSessionFrame.setTotalMsgSize(msgSize);
            bamSessionFrame.setDataPgn(pgn);
            uutSession = nullptr;

            p2pFrame.setCtrlType(TPCM::ControlType::RTS);
            p2pFrame.setSrcAddr(srcAddress);
            p2pFrame.setDstAddr(claimedAddress);
            p2pFrame.setTotalMsgSize(msgSize);
            p2pFrame.setDataPgn(pgn);

            for (u32 i = 0; i < msgSize; i++) {
                expectedData.push_back(i + 1);
            }
        }

        ~IncommingSessionTest() {
            delete uutSession;
        }

        void AssertAckFrame(const Frame *sendFrame) {
            const auto ackFrame = *static_cast<const TPCM::Frame *>(sendFrame);
            ASSERT_EQ(ackFrame.getCtrlType(), TPCM::ControlType::EOM_ACK);
            ASSERT_EQ(ackFrame.getTotalMsgSize(), msgSize);
            ASSERT_EQ(ackFrame.getTotalPackets(), dataPackets);
            ASSERT_EQ(ackFrame.getDstAddr(), srcAddress);
            ASSERT_EQ(ackFrame.getSrcAddr(), claimedAddress);
        }

        void AssertCtsFrame(const Frame *sendFrame, u8 nextPacket, u8 packetToTx) {
            const auto ctsFrame = *static_cast<const TPCM::Frame *>(sendFrame);
            ASSERT_EQ(ctsFrame.getCtrlType(), TPCM::ControlType::CTS);
            ASSERT_EQ(ctsFrame.getCTSPacketsToTx(), packetToTx);
            ASSERT_EQ(ctsFrame.getCTSNextPacket(), nextPacket);
            ASSERT_EQ(ctsFrame.getDstAddr(), srcAddress);
            ASSERT_EQ(ctsFrame.getSrcAddr(), claimedAddress);
        }

        void AssertAbortFrame(const Frame *sendFrame, TPCM::AbortReason reason) {
            const auto abortFrame = *static_cast<const TPCM::Frame *>(sendFrame);
            ASSERT_EQ(abortFrame.getCtrlType(), TPCM::ControlType::ABORT);
            ASSERT_EQ(abortFrame.getAbortReason(), reason);
            ASSERT_EQ(abortFrame.getDstAddr(), srcAddress);
            ASSERT_EQ(abortFrame.getSrcAddr(), claimedAddress);
        }

        void newDataFrameToSession(const u32 packetNum, bool bam = true) {
            const auto restSize = (bam ? bamSessionFrame.getTotalMsgSize() : p2pFrame.getTotalMsgSize()) - (packetNum * TPDT::DATA_SIZE);

            TPDT::Frame dtFrame;
            dtFrame.setSrcAddr(bam ? bamSessionFrame.getSrcAddr() : p2pFrame.getSrcAddr());
            dtFrame.setDstAddr(bam ? bamSessionFrame.getDstAddr() : p2pFrame.getDstAddr());
            dtFrame.setSq(packetNum + 1);
            dtFrame.setData(&expectedData[packetNum * TPDT::DATA_SIZE], restSize > TPDT::DATA_SIZE ? TPDT::DATA_SIZE : restSize);

            ASSERT_FALSE(uutSession->isTimedOut());
            ASSERT_FALSE(uutSession->isAborted());
            ASSERT_FALSE(uutSession->isComplete());
            ASSERT_TRUE(uutSession->isActive());

            uutSession->newDataFrame(dtFrame);

            if (packetNum == dataPackets - 1) {
                //last packet
                ASSERT_TRUE(uutSession->isComplete());
                ASSERT_FALSE(uutSession->isActive());
            }
        }

        void newDataFrameToBamSession(const int packetNum) {
            newDataFrameToSession(packetNum, true);
        }

        void newDataFrameToP2PSession(const int packetNum) {
            newDataFrameToSession(packetNum, false);
        }
    };

    TEST_F(IncommingSessionTest, BamSessionStarted) {

        EXPECT_CALL(mockSessionOwner, setTimer(TimeoutControl::T1, _))
            .Times(1)
            .WillOnce(Return(5));
        uutSession = new IncommingBAMSession(&mockSessionOwner, bamSessionFrame);

        ASSERT_FALSE(uutSession->isTimedOut());
        ASSERT_FALSE(uutSession->isComplete());
        ASSERT_FALSE(uutSession->isAborted());
        ASSERT_TRUE(uutSession->isActive());
    }

    TEST_F(IncommingSessionTest, BamSessionTimedOut) {

        EXPECT_CALL(mockSessionOwner, setTimer(TimeoutControl::T1, _))
            .Times(1)
            .WillOnce(DoAll(InvokeArgument<1>(), Return(5)));
        uutSession = new IncommingBAMSession(&mockSessionOwner, bamSessionFrame);

        ASSERT_TRUE(uutSession->isTimedOut());
        ASSERT_FALSE(uutSession->isComplete());
        ASSERT_FALSE(uutSession->isAborted());
        ASSERT_FALSE(uutSession->isActive());
    }

    TEST_F(IncommingSessionTest, BamSessionContinuesUntilComplete) {
        EXPECT_CALL(mockSessionOwner, setTimer(TimeoutControl::T1, _))
            .WillRepeatedly(Return(5));
        uutSession = new IncommingBAMSession(&mockSessionOwner, bamSessionFrame);

        for (u32 i = 0; i < dataPackets; ++i) {
            newDataFrameToBamSession(i);
        }
    }

    TEST_F(IncommingSessionTest, BamSessionAbortedWithAbortFrame) {
        EXPECT_CALL(mockSessionOwner, setTimer(TimeoutControl::T1, _))
            .WillRepeatedly(Return(5));
        EXPECT_CALL(mockSessionOwner, send(_))
            .Times(0);
        uutSession = new IncommingBAMSession(&mockSessionOwner, bamSessionFrame);

        TPCM::Frame abortFrame;
        abortFrame.setCtrlType(TPCM::ControlType::ABORT);
        abortFrame.setAbortReason(TPCM::AbortReason::TERMINATED);
        abortFrame.setSrcAddr(srcAddress);
        abortFrame.setDstAddr(BROADCAST_ADDRESS);
        abortFrame.setDataPgn(pgn);

        newDataFrameToBamSession(0); // 1 frame to session, then abort

        uutSession->newConnectionFrame(abortFrame);
        ASSERT_FALSE(uutSession->isTimedOut());
        ASSERT_FALSE(uutSession->isComplete());
        ASSERT_TRUE(uutSession->isAborted());
        ASSERT_FALSE(uutSession->isActive());
    }

    TEST_F(IncommingSessionTest, P2PSessionStarted) {
        EXPECT_CALL(mockSessionOwner, setTimer(TimeoutControl::T2, _))
            .Times(testing::AtLeast(1))
            .WillRepeatedly(Return(5));

        const Frame *sendFrame = nullptr;
        EXPECT_CALL(mockSessionOwner, send(_))
            .Times(1)
            .WillOnce(SaveArg<0>(&sendFrame));

        const auto maxPacketsToTx = dataPackets - 1;
        p2pFrame.setRTSMaxPackets(maxPacketsToTx); // one packet less than expected
        uutSession = new IncommingP2PSession(&mockSessionOwner, p2pFrame);

        ASSERT_FALSE(uutSession->isTimedOut());
        ASSERT_FALSE(uutSession->isComplete());
        ASSERT_FALSE(uutSession->isAborted());
        ASSERT_TRUE(uutSession->isActive());
        AssertCtsFrame(sendFrame, 1, maxPacketsToTx);
    }

    ACTION_P3(CheckSendFrame, dtSendCount, maxPacketsToTx, t) {
        if (*dtSendCount == t->dataPackets - 1) {
            /* at the end ack frame */
            t->AssertAckFrame(arg0);
        } else {
            auto restPackets = t->dataPackets - *dtSendCount - 1;
            auto packetsToTx = restPackets < *maxPacketsToTx ? restPackets : *maxPacketsToTx;
            t->AssertCtsFrame(arg0, *dtSendCount + 2, packetsToTx);
        }
    }

    struct P2PSessionParams {
        u32 msgSize;
        u32 maxPacketsToTx;
        u32 expectedCtsPackets;

        friend std::ostream &operator<<(std::ostream &os, const P2PSessionParams &obj) {
            return os
                   << "msgSize: " << obj.msgSize
                   << " maxPacketsToTx: " << obj.maxPacketsToTx
                   << " expectedCtsPackets: " << obj.expectedCtsPackets;
        }
    };

    struct IncommingSessionParametrizedTest : IncommingSessionTest, testing::WithParamInterface<P2PSessionParams> {

        explicit IncommingSessionParametrizedTest()
            : IncommingSessionTest(GetParam().msgSize) {
        }
    };

    using P2PSessionContinuesUntilComplete = IncommingSessionParametrizedTest;
    TEST_P(P2PSessionContinuesUntilComplete, P2PSessionContinuesUntilComplete) {
        const auto param = GetParam();
        const auto maxPacketsToTx = param.maxPacketsToTx;
        const auto ctsPacketsToBeSend = (dataPackets / maxPacketsToTx) + (dataPackets % maxPacketsToTx == 0 ? 0 : 1);
        ASSERT_EQ(ctsPacketsToBeSend, param.expectedCtsPackets);
        u32 i = -1;
        EXPECT_CALL(mockSessionOwner, setTimer(TimeoutControl::T2, _))
            .Times(ctsPacketsToBeSend)
            .WillRepeatedly(Return(5));
        EXPECT_CALL(mockSessionOwner, setTimer(TimeoutControl::T1, _))
            .Times(dataPackets - ctsPacketsToBeSend) // FOR each data packet (except when CTS has been send
            .WillRepeatedly(Return(5));
        const Frame *sendFrame;
        EXPECT_CALL(mockSessionOwner, send(_))
            .Times(ctsPacketsToBeSend + 1) // one CTS and ACK allways present
            .WillOnce(DoAll(SaveArg<0>(&sendFrame), CheckSendFrame(&i, &maxPacketsToTx, this)))
            .WillRepeatedly(CheckSendFrame(&i, &maxPacketsToTx, this));

        ASSERT_TRUE(p2pFrame.setRTSMaxPackets(maxPacketsToTx));
        uutSession = new IncommingP2PSession(&mockSessionOwner, p2pFrame);

        ASSERT_FALSE(uutSession->isTimedOut());
        ASSERT_FALSE(uutSession->isComplete());
        ASSERT_FALSE(uutSession->isAborted());
        ASSERT_TRUE(uutSession->isActive());

        /* First frame send right after session start - at least one CTS send allways */
        AssertCtsFrame(sendFrame, 1, dataPackets < maxPacketsToTx ? dataPackets : maxPacketsToTx);
        ASSERT_FALSE(uutSession->isTimedOut());
        ASSERT_FALSE(uutSession->isComplete());
        ASSERT_FALSE(uutSession->isAborted());
        ASSERT_TRUE(uutSession->isActive());

        for (i = 0; i < dataPackets; ++i) {
            newDataFrameToP2PSession(i);
        }
        ASSERT_FALSE(uutSession->isTimedOut());
        ASSERT_TRUE(uutSession->isComplete());
        ASSERT_FALSE(uutSession->isAborted());
        ASSERT_FALSE(uutSession->isActive());
    }

    INSTANTIATE_TEST_CASE_P(VariousLength, P2PSessionContinuesUntilComplete, testing::Values(
        /* Max size in single cts request*/
        P2PSessionParams{
            MAX_TRANSPORT_PACKETS_DATA_SIZE, //msgSize
            0xFF,                            //maxPacketsToTx
            1                                //expectedCtsPackets
        },
        P2PSessionParams{
            MAX_TRANSPORT_PACKETS_DATA_SIZE, //msgSize
            2,                               //maxPacketsToTx
            128                              //expectedCtsPackets
        },
        P2PSessionParams{
            10, //msgSize
            2,  //maxPacketsToTx
            1   //expectedCtsPackets
        },
        P2PSessionParams{
            125, //msgSize
            17,  //maxPacketsToTx
            2    //expectedCtsPackets
        },
        P2PSessionParams{
            255, //msgSize
            17,  //maxPacketsToTx
            3    //expectedCtsPackets
        },
        P2PSessionParams{
            12,   //msgSize
            0xFF, //maxPacketsToTx
            1     //expectedCtsPackets
        },
        P2PSessionParams{
            16, //msgSize
            2,  //maxPacketsToTx
            2   //expectedCtsPackets
        }));

    
    TEST_F(IncommingSessionTest, P2PSessionAbortedWithAbortFrame) {
        EXPECT_CALL(mockSessionOwner, setTimer(_, _))
            .WillRepeatedly(Return(5));
        
        const Frame *sendFrame = nullptr;
        EXPECT_CALL(mockSessionOwner, send(_))
            .Times(1)
            .WillOnce(SaveArg<0>(&sendFrame));
        uutSession = new IncommingP2PSession(&mockSessionOwner, p2pFrame);

        AssertCtsFrame(sendFrame, 1, dataPackets);
        TPCM::Frame abortFrame;
        abortFrame.setCtrlType(TPCM::ControlType::ABORT);
        abortFrame.setAbortReason(TPCM::AbortReason::MULTI_CTS);
        abortFrame.setSrcAddr(srcAddress);
        abortFrame.setDstAddr(claimedAddress);
        abortFrame.setDataPgn(pgn);

        newDataFrameToP2PSession(0); // 1 frame to session, then abort

        uutSession->newConnectionFrame(abortFrame);
        ASSERT_FALSE(uutSession->isTimedOut());
        ASSERT_FALSE(uutSession->isComplete());
        ASSERT_TRUE(uutSession->isAborted());
        ASSERT_FALSE(uutSession->isActive());        
    }

    TEST_F(IncommingSessionTest, P2PSessionOutOfOrderDataRequestResend) {
        const auto maxPacketsToTx = 0xFF;
        EXPECT_CALL(mockSessionOwner, setTimer(TimeoutControl::T2, _))
            .Times(2)
            .WillRepeatedly(Return(5));
        EXPECT_CALL(mockSessionOwner, setTimer(TimeoutControl::T1, _))
            .Times(1);
        
        const Frame *sendFrame;
        EXPECT_CALL(mockSessionOwner, send(_))
            .Times(2) // one CTS and ACK allways present
            .WillRepeatedly(SaveArg<0>(&sendFrame));

        ASSERT_TRUE(p2pFrame.setRTSMaxPackets(maxPacketsToTx));
        uutSession = new IncommingP2PSession(&mockSessionOwner, p2pFrame);

        ASSERT_FALSE(uutSession->isTimedOut());
        ASSERT_FALSE(uutSession->isComplete());
        ASSERT_FALSE(uutSession->isAborted());
        ASSERT_TRUE(uutSession->isActive());

        /* First frame send right after session start - at least one CTS send allways */
        AssertCtsFrame(sendFrame, 1, dataPackets < maxPacketsToTx ? dataPackets : maxPacketsToTx);
        ASSERT_FALSE(uutSession->isTimedOut());
        ASSERT_FALSE(uutSession->isComplete());
        ASSERT_FALSE(uutSession->isAborted());
        ASSERT_TRUE(uutSession->isActive());

        newDataFrameToP2PSession(0); // first packet, OK
        newDataFrameToP2PSession(2); // packet 1 skyped
        AssertCtsFrame(sendFrame, 2, dataPackets-1);
        ASSERT_FALSE(uutSession->isTimedOut());
        ASSERT_FALSE(uutSession->isComplete());
        ASSERT_FALSE(uutSession->isAborted());
        ASSERT_TRUE(uutSession->isActive());
    }
} // namespace IncommingSessionTests
