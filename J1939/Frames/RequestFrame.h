#ifndef __REQUEST_FRAME_H__
#define __REQUEST_FRAME_H__

#include "J1939Frame.h"

namespace J1939 {
    namespace Request {
        constexpr auto PACKET_SIZE = 3;
        constexpr auto DEFAULT_PRIORITY = 6;
        /**
         * Parameter Group Name             Request
         * Parameter Group Number           59904 (00EA00hex)
         * Definition                       Requests a Parameter Group from a single device or all devices in
         *                                  the network.
         * Transmission                     Rate User defined (no more 2 to 3 times a second is recommended)
         * Data Length                      3 bytes (CAN DLC = 3)
         * Extended Data Page (R)           0
         * Data Page                        0
         * PDU Format                       234
         * PDU Specific                     Destination Address (Global or Peer-to-Peer)
         * Default Priority                 6
         * Data Description                 Byte 1, 2, 3 = Requested Parameter Group Number
         *                                  PGN 60928 (00EE00hex) - Address Claim request
         */
        class Frame : public J1939::Frame {
            u32 mRequestPGN = 0;

        protected:
            bool decodeData(const u8 *buffer, const size_t &length) override;
            bool encodeData(u8 *buffer) const override;

        public:
            Frame();
            Frame(u32 requestPGN);
            virtual ~Frame();

            size_t getDataLength() const override;
            u32 getRequestPGN() const;
            void setRequestPGN(u32 requestPGN);

#if defined(DEBUG) && defined(J1939_STRINGS)
            std::string toString() override;
#endif
        };
    } // namespace Request
}     /* namespace J1939 */

#endif /* __REQUEST_FRAME_H__ */
